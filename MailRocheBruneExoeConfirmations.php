<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

require_once('localise/localise.php');
require_once('functions/db_functions.php');
require_once('data/data_capture.php');

if (NEOCAPTURE_DEBUG_ECHO) echo 'MailRocheBruneExoeConfirmations.php, Start' . PHP_EOL;



// Get trade confirmations from pending directory

$pendingDirectoryName = '/mnt/exoe';
$processedDirectoryName = '/mnt/exoe/processed';
$filePattern = '#Executions_ROCHE_BRUNE_[0-9]+\.csv$#';
$fileSeparator = ';';

$orderLine = array();

// intl Formatters.

$Formatter_Decimal = numfmt_create('fr_FR', NumberFormatter::DECIMAL );
$Formatter_Date = datefmt_create ('fr_FR', IntlDateFormatter::SHORT , IntlDateFormatter::NONE);

$thisFile = '';

if (NEOCAPTURE_DEBUG_ECHO) echo '  Starting ' . $pendingDirectoryName . PHP_EOL;

try
  {
  $directoryFiles = scandir($pendingDirectoryName);
  }
catch (Exception $e)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
  }

try
  {
  if (strlen(date_default_timezone_get()) == 0)
    {
    date_default_timezone_set('UTC');
    }
  }
catch (Exception $e)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
  }

try
  {
  if ($directoryFiles !== false)
    {
    foreach ($directoryFiles as $thisFile)
      {
        // is this file is a directory, especially . and ..
        if (is_dir($pendingDirectoryName . '/' . $thisFile))
        {
        continue;
        }
        // is this file already exists
        if (is_file($pendingDirectoryName . '/' . $file) or is_file($processedDirectoryName . '/' . $file) )
        {
          echo "File already received - skip" . $file . PHP_EOL;
          continue;
        }

      if (preg_match($filePattern, $thisFile))
        {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    Checking : ' . $thisFile . PHP_EOL;


        /* Process this file*/
        $filecontents = file_get_contents($pendingDirectoryName . '/' . $thisFile);
        $DoMoveFile = false;
        if ($filecontents === false)
          {
          if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    Failed to read file ' . $thisFile) . PHP_EOL;

          $DoMoveFile = false;
          }
        else
          {
          $lines = explode("\n", $filecontents);
          $orders_array = array();

          if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    File has %d lines.', count($lines)) . PHP_EOL;
          $idx = 1;
          if (count($lines) > 0)
            {

              foreach ($lines as $line)
              {

                    $orderFields = explode($fileSeparator, $line);
                    $orderLine = array();

                    //Skip the 1st line - columns name - and process only if line is not empty and
                    if ($idx > 1 and $orderFields[0])
                    {

                        //$orderLine['Text'] = $line;
                        $orderLine['FundName'] = trim($orderFields[0]);
                        $orderLine['BrokerName'] = trim($orderFields[1]);
                        $orderLine['BrokerBicCode'] = trim($orderFields[2]);
                        $orderLine['TradeDate'] = trim($orderFields[3]);
                        $orderLine['LastTradeTimestamp'] = trim($orderFields[4]);
                        $orderLine['BuyOrSell'] = trim($orderFields[5]);
                        $orderLine['Instrument'] = trim($orderFields[6]);
                        $orderLine['Isin'] = trim($orderFields[7]);
                        $orderLine['TradedQuantity'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $orderFields[8]));
                        $orderLine['AveragePrice'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $orderFields[9]));
                        $orderLine['Currency'] = trim($orderFields[10]);
                        $orderLine['Clearer'] = trim($orderFields[11]);
                        $orderLine['Exchange'] = trim($orderFields[12]);
                        $orderLine['MicCode'] = trim($orderFields[13]);
                        $orderLine['BrokerFees'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $orderFields[14]));
                        $orderLine['BrokerTradeCode'] = trim($orderFields[15]);
                        $orderLine['SettlementDate'] = trim($orderFields[16]);
                        $orderLine['VeniceID'] = trim($orderFields[17]);
                        $orderLine['VeniceID'] = preg_replace("#ROCHEBRUNE#s","",$orderLine['VeniceID']);


                        $bkr = get_brokerFromBic($orderLine['BrokerBicCode']);
                        $bkrAccount = get_fundFromAccount($orderLine['FundName'],$bkr['BrokerID']);
                        $instrument = get_instrumentFromIsin($orderLine['Isin']);

                        $calculatedFees = round(($orderLine['TradedQuantity'] * $orderLine['AveragePrice'] * $bkrAccount['CommissionRate']), 4);
                        $difference = round($orderLine['BrokerFees'] - $calculatedFees,4);

                        $orderLine['calculatedFundID'] = $bkrAccount['FundID'];
                        $orderLine['calculatedBrokerID'] = $bkr['BrokerID'];
                        $orderLine['calculatedFees'] = $calculatedFees;
                        $orderLine['calculatedFeesDiff'] = $difference;
                        $orderLine['calculatedInstrumentID'] = $instrument['InstrumentID'];
                        $orderLine['calculatedRate'] = $bkrAccount['CommissionRate'];


                        if($orderLine['calculatedFundID'] and $orderLine['calculatedBrokerID'] and $orderLine['calculatedInstrumentID'] and !$orderLine['VeniceID'] )
                        {
                            $result = insert_transaction_sqlsrv($orderLine);
                        }
                        if($orderLine['calculatedFundID'] and $orderLine['calculatedBrokerID'] and $orderLine['calculatedInstrumentID'] and $orderLine['VeniceID'] )
                        {

                            $currDetails = get_existingDetails($orderLine['VeniceID']);
                            $newQuantity = $currDetails['OrderAmount'] - $currDetails['FilledAmount'] ;
                            if(!$newQuantity and $currDetails['OrderStatus'] = '80' and $currDetails['Cancelled'] != '1')
                            {
                                $orderLine['parID'] = '1'; //update existing
                                $result = insert_transaction_sqlsrv($orderLine);

                            }
                            if($newQuantity and $currDetails['OrderStatus'] = '60' and $currDetails['Cancelled'] != '1')
                            {
                                // New line
                                insert_transaction_sqlsrv($orderLine);
                                // Update existing and changing the quantity to working quantity
                                $orderLine['parID'] = '1';
                                $orderLine['calculatedNewUnits'] = $newQuantity;
                                insert_transaction_sqlsrv($orderLine);
                            }


                        }

                        $orders_array[] = $orderLine;
                    }

                    $idx++;
              }

            } // If Count($lines)
          else
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' contained no lines. Empty or no permissions ?';
                $DoMoveFile = true;
            }


          if (count($orders_array) > 0)
          {

              $details = print_r($orders_array,true);

              $subject = "EXOE -  Executions ConfirmationFile Received - " . $thisFile;
              $message = $details;
              $headers = "From: ITSUPPORTAM@primonial.fr";
              $mailto = "mo_pam@primonial.fr,rodolphe.ruzindana@primonial.fr,lotfi.ouederni@primonial.fr";
              // SendMail
              mail($mailto, $subject, $message, $headers);

              $DoMoveFile = true;

           }


          }

        // Move Confirmation file to Processed directory.

        if ($DoMoveFile)
          {
          if (NEOCAPTURE_DEBUG_ECHO) echo '    moving file ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . PHP_EOL;

          try
            {
            $nowDate = New DateTime();
            }
          catch (Exception $e)
            {
            // In the case of an exception, probably because the timezone is not set in php.ini, default to Paris.
            date_default_timezone_set('Europe/Paris');
            $nowDate = New DateTime();
            }

          $newFile = $nowDate->format('YmdHis') . '_' . $thisFile;


          //$copyResult = copy(($pendingDirectoryName . '/' . $thisFile), ($processedDirectoryName . '/' . $newFile));
          $copyResult = copy(($pendingDirectoryName . '/' . $thisFile), ($processedDirectoryName . '/' . $thisFile));
          if ($copyResult)
            {
            $capture = array();
            $capture['setID'] = 44;
            $capture['result'] = 'success';
            $capture['filename'] = $thisFile;
            $capture['priority'] = 0;
            $sql_DateEntered = get_DateNow_sqlsvr();
            $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);

            unlink(($pendingDirectoryName . '/' . $thisFile));
            }
          else
            {
            	$capture = array();
            	$capture['setID'] = 44;
            	$capture['result'] = 'failed to copy';
            	$capture['filename'] = $thisFile;
            	$capture['priority'] = 0;
            	$sql_DateEntered = get_DateNow_sqlsvr();
            	$sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);
            if (NEOCAPTURE_DEBUG_ECHO) echo '    Error, failed to copy ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . '/' . $newFile . PHP_EOL;
            }

          }
        else
          {
          if (NEOCAPTURE_DEBUG_ECHO) echo '    not moving file ' . $pendingDirectoryName . '/' . $thisFile . ' - Update did not work.' . PHP_EOL;
          }

        } // If preg_match()
      else
        {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' did not match the given pattern (' . $filePattern . ')';
        }

      } // For Each $directoryFiles as $thisFile

    } // If $directoryFiles !== false
  else
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '    directory is empty.';
    }

  if (NEOCAPTURE_DEBUG_ECHO) echo '  Ending ' . $pendingDirectoryName . PHP_EOL;

  }
catch (Exception $e)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
  capturefailed('MailRocheBruneExoeConfirmations, failed to process file : ' . $e->getMessage(), $thisFile);
  }


function get_fundFromAccount($account,$broker) {

    renaissance_RB_connect();

    $query = mssql_query("select * from fn_tblBrokerAccounts_SelectKD(null) where AccountNumber = '$account' and BrokerID = '$broker'");

    if (!mssql_num_rows($query)) {
    echo 'No BrokerAccount line found' . PHP_EOL;
    } else {

    $results_array = array();
    while ($row = mssql_fetch_array($query)) {

        echo $row . PHP_EOL;
        $results_array = $row;

    }
    return $results_array;
    }

}

function get_brokerFromBic($BicCode) {

    renaissance_RB_connect();

    $query = mssql_query("select * from fn_tblBroker_SelectKD(null) where BIC_Code = '$BicCode'");

    if (!mssql_num_rows($query)) {
        echo 'No Broker line found' . PHP_EOL;
    } else {

        $results_array = array();
        while ($row = mssql_fetch_array($query)) {

            echo $row . PHP_EOL;
            $results_array = $row;

        }
        return $results_array;
    }

}

function get_instrumentFromIsin($IsinCode) {

    renaissance_RB_connect();

    $query = mssql_query("select * from fn_tblInstrument_SelectKD(null) where InstrumentISIN = '$IsinCode'");

    if (!mssql_num_rows($query)) {
        echo 'No Instrument line found' . PHP_EOL;
    } else {

        $results_array = array();
        while ($row = mssql_fetch_array($query)) {

            echo $row . PHP_EOL;
            $results_array = $row;

        }
        return $results_array;
    }

}


function insert_transaction_sqlsrv($params)
{
    try
    {
        $sqlsvr_conn = renaissance_RB_connect();
        //ini_set('display_errors', 1);

        // Create a new stored prodecure
        $stmt = mssql_init('web_addBuySellTransaction', $sqlsvr_conn);

        /*
         * PROCEDURE web_addBuySellTransaction]
            (
            @TransactionFund [int],
	        @TransactionInstrument [int],
	        @TradeDate [nvarchar](20),
	        @BuyOrSell [nvarchar](50),
	        @TransactionUnits [float],
	        @TransactionPrice [float],
	        @TransactionBroker [int],
	        @TransactionCosts [float],
	        @Settlement [int] = 0,
	        @TradeStatus [int] = 21,
	        @UpdateAutoUpdate [int] = 0
            )
        */
        $trdStatus = '1'; // Status changed from pending MO to Validated MO
        if($params['VeniceID'] and $params['parID'])
        {
            $idToUse = $params['VeniceID'];
        }else{
            $idToUse = '0';
        }
        if($params['VeniceID'] and $params['calculatedNewUnits'])
        {
            $units = $params['calculatedNewUnits'];
            $trdStatus = 4   ; //Keep  "Electronic Market"Status - OrderWorking on bloomberg side
            $params['calculatedBrokerID'] = '0';
            $params['calculatedFees'] = '0';


        }else {
            $units = $params['TradedQuantity'];
        }

        mssql_bind($stmt, '@TransactionFund', &$params['calculatedFundID'], SQLINT4, false, false);
        mssql_bind($stmt, '@TransactionInstrument', &$params['calculatedInstrumentID'], SQLINT4, false, false);
        mssql_bind($stmt, '@TradeDate', &$params['TradeDate'], SQLVARCHAR, false, false);
        mssql_bind($stmt, '@BuyOrSell', &$params['BuyOrSell'], SQLVARCHAR, false, false);
        mssql_bind($stmt, '@TransactionUnits', &$units, SQLFLT8, false, false);
        mssql_bind($stmt, '@TransactionPrice', &$params['AveragePrice'], SQLFLT8, false, false);
        mssql_bind($stmt, '@TransactionBroker', &$params['calculatedBrokerID'], SQLINT4, false, false);
        mssql_bind($stmt, '@TransactionCosts', &$params['calculatedFees'], SQLFLT8, false, false);
        mssql_bind($stmt, '@Settlement', &$params['SettlementDate'], SQLINT4, false, false);
        mssql_bind($stmt, '@TradeStatus', &$trdStatus, SQLINT4, false, false);
        mssql_bind($stmt, '@TransactionParentID', &$idToUse, SQLINT4, false, false);


        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if ($proc_result === false)
            {
                $CaptureID = false;
            }
            else if ($proc_result !== true)
            {

                $row = mssql_fetch_assoc($proc_result);
                $CaptureID = $row['RN'];

            }
            else
            { // ($proc_result===true)
                $CaptureID = true;
            }

            // Free statement
            mssql_free_statement($stmt);
        }
        else
        {
            $CaptureID = (-1);
        }
    } catch (Exception $e)
    {
        $CaptureID = -1;
    }

    return $CaptureID;
}

function get_existingDetails($parentID) {

    renaissance_RB_connect();

    $query = mssql_query("select txn.TransactionParentID, txn.TransactionUnits AS currentUnits, emsx.OrderAmount, emsx.FilledAmount, emsx.OrderStatus, emsx.Cancelled from
    fn_tblTradeFileEmsxStatus_SelectKD(null) as emsx,fn_tblTransaction_FirstLeg_SelectKD(null) as txn
    where emsx.TransactionParentID = txn.TransactionParentID
    and emsx.TransactionParentID = '$parentID'");

    if (!mssql_num_rows($query)) {
        echo 'No Emsx line found' . PHP_EOL;
    } else {

        $results_array = array();
        while ($row = mssql_fetch_array($query)) {

            echo $row . PHP_EOL;
            $results_array = $row;

        }
        return $results_array;
    }

}

?>

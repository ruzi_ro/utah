<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

require_once('localise/localise.php');

require_once(NEOCAPTURE_ROOT . '/functions/curl_neolink.php');
require_once(NEOCAPTURE_ROOT . '/functions/tidy_functions.php');
require_once(NEOCAPTURE_ROOT . '/functions/dom_functions.php');
require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');
require_once(NEOCAPTURE_ROOT . '/data/data_capture.php');

require_once(NEOCAPTURE_ROOT . '/functions/neolink_calls.php');


//collect the post variables for the login

$post = array();
$post['accessCode'] = NEOLINK_BNP_USERNAME;
$post['accessPass'] = NEOLINK_BNP_PASSWORD;
$post['appId'] = '-1';
$post['appURL'] = '';
$post['authLevel'] = '1';
$post['locale'] = 'en';

//$search=$_GET['search'];

//UCITS orders GET
$get_ucits_orders="https://securities-link.bnpparibas.com/web/ConnectActionEx.do?bdujpo]4E]3GMpbeGjmufsMjtu/ep]4Gqbhfobnf]4EGET%60QBHF%60MJTUF%60PSEFS]37nfov]4Eusvf]37bqqmz]4Eusvf&bcu]4EB3192&lang=en";

$day_from = date('d', mktime(0, 0, 0, date("m"), date("d") - 90, date("Y")));
$month_from = date('m', mktime(0, 0, 0, date("m"), date("d") - 90, date("Y")));
$year_from = date('Y', mktime(0, 0, 0, date("m"), date("d") - 90, date("Y")));

$params_add_criteria_date = "pagename=FDS_PAGE_LISTE_ORDER&libelleFilter=&codeFilter=368215&criteriaCodeToDelete=&selectedCriteriaCode=fds.order.date&selectedOperator=filter-operator.greater_or_equal&currentValue=".$day_from."%2F".$month_from."%2F".$year_from."&htxtMode=&SHOW_ALL=&fldParam_FDS_PAGE_LISTE_ORDER=&indexSort_FDS_PAGE_LISTE_ORDER=&sort_FDS_PAGE_LISTE_ORDER=&TABLE_ID=FDS_PAGE_LISTE_ORDER&InputHeaderColumnValue_FDS_PAGE_LISTE_ORDER=false&TRindex=";
$post_add_criteria_date = "https://securities-link.bnpparibas.com/web/addCriteria.do";

$params_apply_filter = "pagename=FDS_PAGE_LISTE_ORDER&libelleFilter=&codeFilter=368215&criteriaCodeToDelete=&fds.order.date_operator=filter-operator.greater_or_equal&fds.order.date_value=".$day_from."%2F".$month_from."%2F".$year_from."&selectedCriteriaCode=fds.order.amount&selectedOperator=filter-operator.greater_or_equal&currentValue=&htxtMode=&SHOW_ALL=&fldParam_FDS_PAGE_LISTE_ORDER=&indexSort_FDS_PAGE_LISTE_ORDER=&sort_FDS_PAGE_LISTE_ORDER=&TABLE_ID=FDS_PAGE_LISTE_ORDER&InputHeaderColumnValue_FDS_PAGE_LISTE_ORDER=true";
$post_apply_filter = "https://securities-link.bnpparibas.com/web/applyFilter.do";

$params_filter_builder = "pagename=FDS_PAGE_LISTE_ORDER&libelleFilter=&codeFilter=368215&criteriaCodeToDelete=&fds.order.date_operator=filter-operator.greater_or_equal&fds.order.date_value=".$day_from."%2F".$month_from."%2F".$year_from."&selectedCriteriaCode=fds.order.amount&selectedOperator=filter-operator.greater_or_equal&currentValue=&htxtMode=all&SHOW_ALL=&fldParam_FDS_PAGE_LISTE_ORDER=&indexSort_FDS_PAGE_LISTE_ORDER=&sort_FDS_PAGE_LISTE_ORDER=&TABLE_ID=FDS_PAGE_LISTE_ORDER&InputHeaderColumnValue_FDS_PAGE_LISTE_ORDER=false&TRindex=";
$post_filter_builder = "https://securities-link.bnpparibas.com/web/FilterBuilder.do?C_MODE=LAST";

//create a post string to pass to the login curl script

$post_params_2 = "";
$post_params_2 .= "pagename=CAR_PAGE_LISTE_SECURITIES";
$post_params_2 .= "&libelleFilter=";
$post_params_2 .= "&codeFilter=281338";
$post_params_2 .= "&criteriaCodeToDelete=";
$post_params_2 .= "&criteriaCodeChangedValue=";
$post_params_2 .= "&newCriteriaValue=";
$post_params_2 .= "&codeIsinValeur_operator=filter-operator.equal";
$post_params_2 .= "&codeIsinValeur_value=";
$post_params_2 .= "&statut_operator=filter-operator.equal";
$post_params_2 .= "&statut_value=";
$post_params_2 .= "&numeroCompte_operator=filter-operator.contain";
$post_params_2 .= "&numeroCompte_value=";
$post_params_2 .= "&car.conf.referenceClientLong_operator=filter-operator.contain";
$post_params_2 .= "&car.conf.referenceClientLong_value=";
$post_params_2 .= "&categorie_operator=filter-operator.equal";
$post_params_2 .= "&categorie_value=";
$post_params_2 .= "&swiftStatut_operator=filter-operator.startswith";
$post_params_2 .= "&swiftStatut_value=";
$post_params_2 .= "&selectedCriteriaCode=libelleCompte";
$post_params_2 .= "&selectedOperator=filter-operator.equal";
$post_params_2 .= "&currentValue=";
$post_params_2 .= "&htxtMode=all";
$post_params_2 .= "&SHOW_ALL=";
$post_params_2 .= "&fldParam_CAR_PAGE_LISTE_SECURITIES=";
$post_params_2 .= "&indexSort_CAR_PAGE_LISTE_SECURITIES=";
$post_params_2 .= "&sort_CAR_PAGE_LISTE_SECURITIES=";
$post_params_2 .= "&TABLE_ID=CAR_PAGE_LISTE_SECURITIES";
$post_params_2 .= "&InputHeaderColumnValue_CAR_PAGE_LISTE_SECURITIES=false";
$post_params_2 .= "&TRindex=";


$ckfile = NEOCAPTURE_ROOT . '/tmp/UTAH_CURLCOOKIE_PARIS.txt';
$path_parts = pathinfo($ckfile);
if (!is_dir($path_parts['dirname']))
  {
  echo 'The directory `'.$path_parts['dirname'].'` does not exist, it is not possible to create the curl cookie file.';
  }

$url_content = "";
$token = "";
$post_params_1 = "";

// try to get all holdings with existing cookies and session variables




function capturefailed($message)
  {
  $capture = array();
  $capture['setID'] = 6;
  $capture['dateandtime'] = convertToSQLDate(time());
  $capture['result'] = $message;
  
  $sql_DateEntered = get_DateNow_sqlsvr();
  add_capture_sqlserver($capture, $sql_DateEntered);
  }

//first assume that session is still valid and try and go straight to the confirmations table

if (NEOCAPTURE_DEBUG_ECHO) echo 'Ucits Orders, Start' . "<br>";

$success = true;
$result = "failed";

if (NEOCAPTURE_DEBUG_ECHO) echo '  first Result : '.$result . "<br>";

//if first try is not successful then walk through logon procedure

if ($result == "failed")
  {
  $complete = false;
  $trycount = 0;
  $loginCount = 0;

  do
    {
    switch ($result)
    {
      case "failed":
        if ($loginCount >= 6)
          {
          $loginCount = 0;
          unlink($ckfile);
          }

        $result = getLoginScreen();
        $loginCount++;
        break;

      case "login screen":
        if ($loginCount >= 6)
          {
          $loginCount = 0;
          unlink($ckfile);
          $result = 'failed';
          break; // so it does getLoginScreen() again.
          }

        $result = postLoginScreen();
        $loginCount+=2;
        break;

      case "loginContinuation":
        $result = loginScreenContinue();
        break;

      case "timeout login screen":
        $result = postTimeoutLoginScreen();
        //neolink returns a form with a 'post' of this.  This is a get. Have not tested.
        $result = getUcitsOrders();
        break;

      case "menu page":
        $result = getPortalPage();
        break;
        
       case "home post":
        $result = homePost();
        break;

      case "portal page":
        $result = getUcitsOrders();
        break;

      case "paged orders page":
        $result = postAddCriteriaDate();
        break;

      case "date criteria added":
        $result = postApplyFilter();
        break;
        
      case "filtered results":
        $result = postAllFilter();
        break;
        
      case "success":
        $complete = true;
        $success = true;
        break;
    }
    $trycount++;
    if ($trycount > 17)
      {
      $complete = true;
      $success = false;
      }

    if (NEOCAPTURE_DEBUG_ECHO) echo '    Next Result : '.$result . "<br>";

    } while (!$complete);
  }

if ($success)
  {

//clean up content by removing non blank spaces

  $url_clean = str_replace('&nbsp;', "", $url_content);
  $url_content = $url_clean;

//extract the table containing the holdings

  if (NEOCAPTURE_DEBUG_ECHO) echo '  URLContent (a): '.strlen($url_content) . "<br>";

  $select = "#ContentTable_FDS_PAGE_LISTE_ORDER";
  try
    {
    $url_content = tidy_clip(phpq_extract($url_content, $select));

    if (NEOCAPTURE_DEBUG_ECHO) echo '  URLContent (b): '.strlen($url_content) . "<br>";

    $url_content = dom_remove_elements_attributes_links($url_content);  // Combined function

    if (NEOCAPTURE_DEBUG_ECHO) echo '  URLContent (c): '.strlen($url_content) . "<br>".$url_content;

    } catch (Exception $e)
    {
    capturefailed('extract failed');
    exit;
    }

// write to database

  try
    {
    	//$sql_DateEntered = date('Y-m-d H:i:s.u'); // /* ODBC canonical (with milliseconds) yyyy-mm-dd hh:mi:ss.mmm(24h)  */

        $sql_DateEntered = get_DateNow_sqlsvr();

        $capture = array();
        $capture['setID'] = 6;
        $capture['dateandtime'] = convertToSQLDate(time());
        $capture['result'] = 'success';

        // Event Log.
        $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);
        $id = 0;

      if (NEOCAPTURE_DEBUG_ECHO) echo '  $id        : '.$id . PHP_EOL;
    	if (NEOCAPTURE_DEBUG_ECHO) echo '  $sqlsvr_id : '.$sqlsvr_id . PHP_EOL;

      // Save Confirmations (UCITS) data.
    	echo dom_ucits_orders_table($url_content, $id, $sqlsvr_id, $sql_DateEntered);

    } catch (Exception $e)
    {
    	capturefailed('write failed ' . $e->getMessage());
    	if (NEOCAPTURE_DEBUG_ECHO) echo 'Failed.' . $e->getMessage() . PHP_EOL;
    	exit;
    }

  }
else
  {
  capturefailed('failed after max attempts');
  if (NEOCAPTURE_DEBUG_ECHO) echo 'Failed.' . PHP_EOL;
  }

@chmod ($ckfile, 0777);
@chgrp ($ckfile, 'primonial');
@chown ($ckfile, 'www-data');

if (NEOCAPTURE_DEBUG_ECHO) echo 'Done.' . PHP_EOL;

?>
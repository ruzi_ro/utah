<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

require_once('localise/localise.php');

require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');
require_once(NEOCAPTURE_ROOT . '/functions/curl_neolink.php');
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');
require_once(NEOCAPTURE_ROOT . '/data/data_capture.php');
require_once(NEOCAPTURE_ROOT . '/data/data_morningstar_prices.php');

$public_dir = '/mnt/public';
$date = date('dmYHis');
$filename=$public_dir."/instrumentbenchmarks.csv";
$fileContents="";
$filenamereturns=$public_dir."/benchmarksreturns.csv";
$returnsContents="";
$filenamebenchmarks=$public_dir."/benchmarks.csv";
$benchmarksContents="";

//debug info
function LiveOutput($tag, $message)
{
	$channel = "d994fTXc";
	file_get_contents("http://liveoutput.com/log.php?channel=" . URLEncode($channel) . "&tag=" . URLEncode($tag) . "&message=" . URLEncode($message));
}

function getMorningstarPrices($msCodes)
{
	global $filename, $filenamereturns, $filenamebenchmarks, $fileContents, $returnContents;
	
	try
	{

		$results = array();
		$resultline = array();
		$codecount = 0;
		$chunkcount = 0;
		$codestring = "";
		$currencystring = "";

		foreach ($msCodes AS $instrumentLine)
		{
			// $msCodes[1] as $msCurrency){
			$msCurrency = trim($instrumentLine['Currency']);
			$msISIN = trim($instrumentLine['ISIN']);
			LiveOutput("curr,isin",$msCurrency." ".$msISIN);

			$url="https://lt.morningstar.com/api/rest.svc/security_details/jrhe6v87w5?id=".$msISIN."&idtype=ISIN&currencyid=".$msCurrency;

			//$url = "http://lt.morningstar.com/api/rest.svc/security_list/jrhe6v87w5/?pagesize=500&identifierList=" . $codestring . "&identifierCurrencyList=" . $currencystring;

			
			$xmlstring = getGeneric($url);
			
			if (strlen($xmlstring)>1000){
			$xml = simplexml_load_string($xmlstring);
			$name=(string)$xml->Fund->FundBasics->Name;
			$msCode=(string)$xml->attributes()->_Id;
			LiveOutput("name",$name);
			
			foreach ($xml->Fund->FundBasics->PrimaryBenchmark->HoldingDetail as $Benchmark)
			{
				
				$benchmarkName=(string)$Benchmark->SecurityName;
				LiveOutput("benchmark",$benchmarkName);
				
				$benchmarkid=(string)$Benchmark->attributes()->_Id;
				$returnContents="";
				
				if (strlen($benchmarkid)>6){
					
					$url="http://lt.morningstar.com/api/rest.svc/timeseries_cumulativereturn/jrhe6v87w5?type=MSID&timeperiod=120&frequency=daily&outputType=XML&id=".$benchmarkid.']8]0]CAALL$$ALL';
					
					$benchmarksContents=$benchmarkid.",".$benchmarkName."\r\n";
					if (!(file_put_contents($filenamebenchmarks, $benchmarksContents, FILE_APPEND)===false)){
						//echo $filenamereturns." sucess"."<br>";
					} else {
						//echo $filename." FAILED - UNABLE TO CREATE FILE"."<br>";
					}
					
					
					try{
						$xmlretstring=(getGeneric($url));
						//echo $fund."<br>";
						$xmlret=simplexml_load_string($xmlretstring);
						foreach ($xmlret->Security->CumulativeReturnSeries->HistoryDetail as $History){
							$hdate=$History->EndDate;
							$hvalue=$History->Value;
							$hdateparts=explode("-",$hdate);
							$outvalue=(float)$hvalue+100.00;
							$outdate=$hdateparts[2]."/".$hdateparts[1]."/".$hdateparts[0];
							$returnContents.=$benchmarkid.",".$outvalue.",".$outdate."\r\n";
						}
					
					}
					catch (Exception $e)
					{
						capturefailed('capture failed ' . $e->getMessage());
						return "failed";
					}
					
					if (!(file_put_contents($filenamereturns, $returnContents, FILE_APPEND)===false)){
						echo $filenamereturns." sucess"."<br>";
					} else {
						echo $filename." FAILED - UNABLE TO CREATE FILE"."<br>";
					}
				}
				
				$fileContents.=$msISIN.",".$msCurrency.",".$msCode.",".$name.",".(string)$Benchmark->attributes()->_Id.",".(string)$Benchmark->attributes()->_DetailHoldingTypeId.",".$benchmarkName.",".$Benchmark->Weighting."\r\n";
			
			}

			
			$codecount++;
		}
		}
		
		if (!(file_put_contents($filename, $fileContents)===false)){
			echo $filename." sucess"."<br>";
			return "sucess";
		} else {
			echo $filename." FAILED - UNABLE TO CREATE FILE"."<br>";
			return "write failed";
		}

	} catch (Exception $e)
	{
		//capturefailed('capture failed ' . $e->getMessage());
		return "failed";
	}
	return $results;
}

// MAIN

if (NEOCAPTURE_DEBUG_ECHO) echo 'Morningstar Prices, Start' . PHP_EOL;

if (NEOCAPTURE_DEBUG_ECHO) echo '  Get Instrument IDs from Venice' . PHP_EOL;

$msCodes = getMorningstarTest();

if (NEOCAPTURE_DEBUG_ECHO) echo '  ' . count($msCodes[0]) . ' Instruments returned.' . PHP_EOL;

$results = GetMorningstarPrices($msCodes);

if (count($results) > 1)
{
	$success = true;
} else
{
	$success = false;
}


if ($success)
{

	try
	{

		/*
		$capture = array();
		$capture['setID'] = 7;
		$capture['dateandtime'] = convertToSQLDate(time());
		$capture['result'] = 'success';
		$sql_DateEntered = get_DateNow_sqlsvr();

		$sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);

		if (NEOCAPTURE_DEBUG_ECHO) echo '  Capture ID : ' . $sqlsvr_id . PHP_EOL;

		$results_array = add_morningstar_prices_sqlsrv($msCodes, $results, $sql_DateEntered);
		*/

	}
	catch (Exception $e)
	{
		if (NEOCAPTURE_DEBUG_ECHO) echo 'write failed ' . $e->getMessage() . PHP_EOL;

		//capturefailed('write failed ' . $e->getMessage());
		exit;
	}

}
else
{
	if (NEOCAPTURE_DEBUG_ECHO) echo 'capture failed';
	//capturefailed('failed');
	exit;
}

if (NEOCAPTURE_DEBUG_ECHO) echo 'Done.' . PHP_EOL;

?>
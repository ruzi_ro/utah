<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);


require_once('localise/localise.php');
require_once(LOGIN_PASSWORD_FILE);
require_once 'includes/PHPExcel/Classes/PHPExcel/IOFactory.php';
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');
require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');
require_once(NEOCAPTURE_ROOT . '/data/data_capture.php');
require_once(NEOCAPTURE_ROOT . '/data/data_reconcilliation_dashboard.php');

//The following list must be updated when a new fund is created for RocheBrune
$rocheBruneFunds = "1310501;1310502;1310503;1310504;1310505";

if (NEOCAPTURE_DEBUG_ECHO) echo 'check_FeesAmount_RocheBrune.php, Start' . PHP_EOL;

ini_set('display_errors', 1);


$pendingDirectoryName = VLFILES;
$processedDirectoryName = VLFILES_PROCESSED;
//$filePattern = INVENTORY_PATTERN;
$filePattern = '/Hist_frais_gestion/';
$directoryFiles = false;

$thisFile = '';

if (NEOCAPTURE_DEBUG_ECHO) echo '  Starting ' . $pendingDirectoryName . PHP_EOL;


try
{
    $directoryFiles = scandir($pendingDirectoryName);
}
catch (Exception $e)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
}

try
{
    $sql_DateEntered = get_DateNow_sqlsvr();
    $capture = array();
    $capture['setID'] = 19;
    $capture['dateandtime'] = convertToSQLDate(time());
    $capture['result'] = 'success';
    $id = add_capture($capture);
    $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);

    if ($directoryFiles !== false)
    {
        foreach ($directoryFiles as $thisFile)
        {
            // is this file is a directory, especially . and .


            if (is_dir($pendingDirectoryName . '/' . $thisFile))
            {
                continue;
            }

            if (preg_match($filePattern, $thisFile))
            {
                echo 'Filename : ' . $thisFile . PHP_EOL;
                if (NEOCAPTURE_DEBUG_ECHO) echo '    Checking : ' . $thisFile . PHP_EOL;

                $objPHPExcel = PHPExcel_IOFactory::load($pendingDirectoryName . '/' . $thisFile);

                if ($objPHPExcel === false)
                {
                    if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    Failed to load file ' . $thisFile) . PHP_EOL;

                    $DoMoveFile = false;
                }
                else
                {
                    $sheet_nbr = $objPHPExcel->getSheetCount();
                    $sheet = $objPHPExcel->getSheet(0);
                    $chkColumn = "Code Fonds";
                    $firstFundCode = "1111111"; //fake pattern
                    //echo $sheet_nbr . " sheets in the xls file." . PHP_EOL;
                    if ($sheet_nbr > 1)
                    {
                        echo $sheet_nbr . " wrong sheets number in the xls file." . PHP_EOL;

                    }else
                    {
                        $columnNameChk = $sheet->getCell('C3')->getValue();
                        $firstFundCode = $sheet->getCell('C4')->getValue();

                        $lastRow = $sheet->getHighestRow();
                        echo '---------------------------------' . PHP_EOL;
                        echo 'First FundCode : ' . $firstFundCode . PHP_EOL;
                    }

                    $params = array();

                    if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    File has lines.', count($lastRow)) . PHP_EOL;

                    $columnPattern = '/'. $chkColumn . '/';
                    $fundPattern = '/'. $firstFundCode . '/';

                    //if (strpos($rocheBruneFunds,$fundCode) >= 0)
                    if (preg_match($fundPattern,$rocheBruneFunds) and preg_match($columnPattern,$columnNameChk))
                    {

                        echo 'RocheBrune valid fees file --- processing ' . PHP_EOL;
                        echo '----------------------------------' . PHP_EOL;



                        if (NEOCAPTURE_DEBUG_ECHO) echo '      Exchange Rate '. PHP_EOL;
                        xls_feesamount_table($sheet, $sqlsvr_id);


                        // Save events file entries.
                        $DoMoveFile = true;



                    } // If Count($lines)
                    else
                    {
                        echo 'not interesting or not for RocheBrune ---- skip the file ' . PHP_EOL;
                        echo '----------------------------------' . PHP_EOL;
                        if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' contained no lines. Empty or no permissions ?';

                        $DoMoveFile = false;

                    }


                }

                if ($DoMoveFile)
                {
                    if (NEOCAPTURE_DEBUG_ECHO) echo '    moving file ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . PHP_EOL;

                    try
                    {
                        $nowDate = New DateTime();
                    }
                    catch (Exception $e)
                    {
                        // In the case of an exception, probable because the timezone is not set in php.ini, default to Paris.
                        date_default_timezone_set('Europe/Paris');
                        $nowDate = New DateTime();
                    }

                    $newFile = $nowDate->format('YmdHis') . '_FeesAmount_RB_' . $thisFile;

                    $copyResult = copy(($pendingDirectoryName . '/' . $thisFile), ($processedDirectoryName . '/' . $newFile));
                    if ($copyResult)
                    {
                        unlink(($pendingDirectoryName . '/' . $thisFile));
                    }
                    else
                    {
                        if (NEOCAPTURE_DEBUG_ECHO) echo '    Error, failed to copy ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . '/' . $newFile . PHP_EOL;
                    }

                }
                else
                {
                    if (NEOCAPTURE_DEBUG_ECHO) echo '    not moving file ' . $pendingDirectoryName . '/' . $thisFile . ' - Update did not work.' . PHP_EOL;
                }

            } // If preg_match()
            else
            {
                if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' did not match the given pattern (' . $filePattern . ')';
            }

        } // For Each $directoryFiles as $thisFile

    } // If $directoryFiles !== false
    else
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    directory is empty.';
    }

    if (NEOCAPTURE_DEBUG_ECHO) echo '  Ending ' . $pendingDirectoryName . PHP_EOL;

}
catch (Exception $e)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
    capturefailed('check_FeesAmount_RocheBrune : ' . $e->getMessage(), $thisFile);
}




function xls_feesamount_table($currentSheet, $sqlsvr_id)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '  In `feesamount sheet`' . PHP_EOL;

    $classArray = array (
        "C01"   => "CLASSIQUE         ",
        "C24"	=> "P                 ",
        "C02"	=> "B1                ",
        "C03"	=> "INSTITUTIONNELLE  ",
        "C04"	=> "N                 ",
        "C05"	=> "X                 ",
        "C06"	=> "PRIVILEGE         ",
        "C07"	=> "PRIVILEGE Y       ",
        "C08"	=> "I Y               ",
        "C09"	=> "B4                ",
        "C10"	=> "A                 ",
        "C11"	=> "B                 ",
        "C12"	=> "C                 ",
        "C13"	=> "I                 ",
        "C14"	=> "INSTITUTIONS 2    ",
        "C15"	=> "B5                ",
        "C16"	=> "D                 ",
        "C17"	=> "E                 ",
        "C18"	=> "F                 ",
        "C19"	=> "B6                ",
        "C20"	=> "B7                ",
        "C21"	=> "B8                ",
        "C22"	=> "AC2               ",
        "C23"	=> "AC3               ",
        "C25"	=> "CLASSIC H         ",
        "C26"	=> "I DISTRIBUTION    ",
        "C27"	=> "S                 ",
        "C28"	=> "ES                ",
        "C29"	=> "JOUR              ",
        "C30"	=> "TERME             ",
        "C31"	=> "A1 HEDGE          ",
        "C32"	=> "A2 HEDGE          ",
        "C33"	=> "B1 HEDGE          ",
        "C34"	=> "B2 HEDGE          ",
        "C35"	=> "H                 ",
        "C36"	=> "K                 ",
        "C37"	=> "I                 ",
        "C38"	=> "P                 ",
        "C39"	=> "I 100             ",
        "C40"	=> "P 100             ",
        "C41"	=> "I HORIZON         ",
        "C42"	=> "P HORIZON         ",
        "C43"	=> "O                 ",
        "C44"	=> "R                 ",
        "C45"	=> "U                 ",
        "C46"	=> "LFP DIF. ZONE EURO",
        "C47"	=> "GE                ",
        "C48"	=> "A                 ",
        "C49"	=> "X                 ",
        "C50"	=> "GESTION PRIVEE    ",
        "C51"	=> "N                 ",
        "C52"	=> "G                 ",
        "C53"	=> "P1                ",
        "C54"	=> "P2                ",
        "C55"	=> "I1                ",
        "C56"	=> "I2                ",
        "C57"	=> "I3                ",
        "C58"	=> "C HEDGED          ",
        "C59"	=> "D HEDGED          ",
        "C60"	=> "A HEDGED          ",
        "C61"	=> "B HEDGED          ",
        "C62"	=> "IA EUR            ",
        "C63"	=> "IA USD            ",
        "C64"	=> "IB EUR            ",
        "C65"	=> "IB USD            ",
        "C66"	=> "RA EUR            ",
        "C67"	=> "E HEDGED          ",
        "C68"	=> "RA SGD            ",
        "C69"	=> "RA HKD            ",
        "C70"	=> "RB EUR            ",
        "C71"	=> "CLASSIC T1        ",
        "C72"	=> "M                 ",
        "C73"	=> "Z                 ",
        "C74"	=> "L                 ",
        "C75"	=> "S HEDGE           ",
        "C76"	=> "MANDAT            ",
        "C77"	=> "F HEDGE           ",
        "C80"	=> "IT1               ",
        "C81"	=> "K DISTRIBUTION    ",
        "C82"	=> "I PLUS            ",
        "C83"	=> "CLASSIC NEW DISTRI",
        "C84"	=> "I HEDGE           ",
        "C85"	=> "P DISTRIBUTION    ",
        "C86"	=> "Q                 "
    ) ;

    try
    {

        $rowNbr = 1;
        foreach($currentSheet->getRowIterator() as $row) {


            if  ($rowNbr > 3)
            {
                $feesAmountLine = array();
                $feesAmountLine['captureID'] = $sqlsvr_id;
                $feesAmountLine['fundCode'] = $currentSheet->getCell('C' . (string)$rowNbr)->getValue();
                $feesAmountLine['NAVDate'] = $currentSheet->getCell('B' . (string)$rowNbr)->getValue();
                $ndt = PHPExcel_Shared_Date::ExcelToPHP($feesAmountLine['NAVDate']);
                $ndt = date('d/m/Y', $ndt);
                $feesAmountLine['NAVDate'] = $ndt;
                $classCode = $currentSheet->getCell('E' . (string)$rowNbr)->getValue();
                if ($classCode)
                {
                  $feesAmountLine['class'] = $classArray["$classCode"];
                }

                $feesAmountLine['feetype'] = $currentSheet->getCell('H' . (string)$rowNbr)->getValue();
                $feesAmountLine['feesamount'] = $currentSheet->getCell('I' . (string)$rowNbr)->getValue();
                $feesAmountLine['feescumulative'] = $currentSheet->getCell('J' . (string)$rowNbr)->getValue();
                $feesAmountLine['partisin'] = $currentSheet->getCell('D' . (string)$rowNbr)->getValue();
                $feesAmountLine['parttype'] = $currentSheet->getCell('F' . (string)$rowNbr)->getValue();
                $feesAmountLine['classcode'] = $currentSheet->getCell('E' . (string)$rowNbr)->getValue();
                $feesAmountLine['feescode'] = $currentSheet->getCell('G' . (string)$rowNbr)->getValue();


                $feesAmountLine['class'] = trim($feesAmountLine['class']);

                //echo 'feesamount : ' . $feesAmountLine['feesamount'] . PHP_EOL;
                //echo 'feesamountcumu : ' . $feesAmountLine['feescumulative'] . PHP_EOL;

                if ($feesAmountLine['partisin'] && $classCode )
                {

                    print_r ($feesAmountLine)  . PHP_EOL;
                    insert_feesamount_sqlsrv($feesAmountLine);
                }

            }


            $rowNbr++;

        }



    }
    catch (Exception $e)
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

}

function insert_feesamount_sqlsrv($params)
{
    try
    {
        $sqlsvr_conn = sqlserver_neocapture_connect();
        //ini_set('display_errors', 1);

        // Create a new stored prodecure
        $stmt = mssql_init('adp_rec_FeesAmount_InsertCommand', $sqlsvr_conn);

        // Bind the field names

        /*
              (
              PROCEDURE [dbo].[adp_rec_FeesAmount_InsertCommand]
                    (
                    @IDENTITY int OUTPUT,
                    @captureID int,
                    @NAVDate [nvarchar](50),
                    @FundCode [nvarchar](50),
                    @neo_class [nvarchar](50),
                    @neo_feetype [nvarchar](50),
                    @neo_feesamount [nvarchar](50),
                    @neo_feescumulative[nvarchar](50),
                    @neo_DateEntered nvarchar(50)
                    @neo_partisin [nvarchar](50),
                    @neo_parttype [nvarchar](50),
                    @neo_classcode [nvarchar](50),
                    @neo_feescode [nvarchar](50)

              )

         * */

        $Identity = 0;
        mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
        mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
        mssql_bind($stmt, '@NAVDate', &$params['NAVDate'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@FundCode', &$params['fundCode'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_class', &$params['class'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_feetype', &$params['feetype'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_feesamount', &$params['feesamount'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_feescumulative', &$params['feescumulative'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_partisin', &$params['partisin'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_parttype', &$params['parttype'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_classcode', &$params['classcode'], SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@neo_feescode', &$params['feescode'], SQLVARCHAR, false, false, 50);


        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if ($proc_result === false)
            {
                $CaptureID = false;
            }
            else if ($proc_result !== true)
            {

                $row = mssql_fetch_assoc($proc_result);
                $CaptureID = $row['ID'];

            }
            else
            { // ($proc_result===true)
                $CaptureID = true;
            }

            // Free statement
            mssql_free_statement($stmt);
        }
        else
        {
            $CaptureID = (-1);
        }
    } catch (Exception $e)
    {
        $CaptureID = -1;
    }

    return $CaptureID;
}

function capturefailed($message)
{
    $capture = array();
    $capture['setID'] = 19;
    $capture['dateandtime'] = convertToSQLDate(time());
    $capture['result'] = $message;
    $sql_DateEntered = get_DateNow_sqlsvr();
    add_capture_sqlserver($capture, $sql_DateEntered);
}

?>

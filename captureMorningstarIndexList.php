<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

require_once('localise/localise.php');

require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');
require_once(NEOCAPTURE_ROOT . '/functions/curl_neolink.php');
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');
require_once(NEOCAPTURE_ROOT . '/data/data_capture.php');
require_once(NEOCAPTURE_ROOT . '/data/data_morningstar_prices.php');

$public_dir = '/mnt/public';
$date = date('dmYHis');
$filename=$public_dir."/instrumentbenchmarks.csv";
$fileContents="";
$filenamereturns=$public_dir."/benchmarksreturns2.csv";
$returnsContents="";
$filenamebenchmarks=$public_dir."/benchmarks.csv";
$benchmarksContents="";

//debug info
function LiveOutput($tag, $message)
{
	$channel = "d994fTXc";
	file_get_contents("http://liveoutput.com/log.php?channel=" . URLEncode($channel) . "&tag=" . URLEncode($tag) . "&message=" . URLEncode($message));
}

function getMorningstarPrices($msCodes)
{
	global $filename, $filenamereturns, $filenamebenchmarks, $fileContents, $returnContents;

	$codestring="FOUSA06RP3
FOUSA06EGK
XIUSA00108
FOUSA06EK0
FOUSA06DWM
FOUSA06EH9
FOUSA06FMD
FOUSA05Y3Q
F00000MTUG
F00000OLAF
FOUSA08RNJ
FOUSA05Y3M
FOUSA06E26
F00000JR49
FOUSA06VFS
FOUSA08AF9
FOUSA06K3V
XIUSA000CU
XIUSA00109
XIUSA04CX5
FOUSA06RX7
FOUSA05YDY
XIUSA0010B
XIUSA000MC
FOUSA06MFO
XIUSA04GTZ
XIUSA000C3
FOUSA08NBR
FOUSA08NBN
XIUSA04GR8
XIUSA04DM9
FOUSA06W37
F00000MPND
XIUSA04EVI
XIUSA04GTA
F00000LY1T
F00000JRSZ
FOUSA06JCL
FOUSA05U41
FOUSA05U3Z
XIUSA04GTB
F00000J4UK
XIUSA04GT8
F00000J4UM
FOUSA08YRX
F00000J4UN
F00000J4UJ
F00000MQ8O
FOUSA06JCJ
F00000J4UO
F00000J79F
XIUSA04GTG
F00000J4UL
FOUSA08QSW
XIUSA04GTE
XIUSA04GTC
FOUSA05U44
FOUSA06JCG
FOUSA06JDC
FOUSA05U42
FOUSA05U40
F00000JRT0
F00000MQ8M
F00000JRT1
FOUSA05UI2
XIUSA04AGF
XIUSA04AMJ
XIUSA04AMN
XIUSA04AE3
XIUSA04B61
XIUSA04B66
XIUSA04B6B
XIUSA04GVB
XIUSA04BIG
XIUSA04BIK
XIUSA04BL2
XIUSA04BL5
XIUSA04BLE
XIUSA04GIH
XIUSA04AEE
XIUSA04BTS
XIUSA04BVV
XIUSA04C1V
FOUSA05WAQ
XIUSA04GTX
XIUSA04C3A
XIUSA04C3C
XIUSA04C4W
XIUSA04C4V
XIUSA04C5S
XIUSA04C6A
XIUSA04C72
XIUSA000TY
XIUSA000UC
FOUSA05UMV
FOUSA08T53
FOUSA06WSY
FOUSA06EA0
FOUSA05LNO
FOUSA05LSZ
FOUSA06CGQ
FOUSA06CGU
XIUSA04GB7
XIUSA04GB5
XIUSA04GBB
XIUSA04GB4
XIUSA04GB8
XIUSA04GB9
XIUSA04GBN
XIUSA04GBM
XIUSA04GBZ
XIUSA04GBP
XIUSA04GBQ
XIUSA04GBR
XIUSA04GBO
XIUSA0001M
XIUSA04CG4
XIUSA0001H
XIUSA04CG6
XIUSA04CGI
XIUSA04CGU
FOUSA06TUS
FOUSA05PYV
FOUSA08O06
FOUSA088OX
FOUSA088OY
FOUSA088PE
FOUSA09F9N
XIUSA04CTD
FOUSA05O5X
FOUSA08X90
FOUSA08X91
FOUSA06G9X
FOUSA06XRO
FOUSA08N4W
FOUSA06CYX
F00000JRHY
FOUSA06EE1
XIUSA04GWF
XIUSA04EVS
XIUSA04EW2
XIUSA04EW7
XIUSA04GAX
XIUSA000PO
XIUSA000PX
XIUSA04F13
FOUSA09K50
XIUSA000Q6
XIUSA04F1J
XIUSA000QM
XIUSA000PK
XIUSA04F36
FOUSA06FBM
XIUSA04F3N
XIUSA04F2T
F00000HE7C
XIUSA04F6F
XIUSA04F6I
XIUSA000RF
XIUSA04FB0
XIUSA04FB6
XIUSA000PL
XIUSA04FB9
XIUSA04FBC
FOUSA06V57
FOUSA06V59
XIUSA04FET
XIUSA04FEY
XIUSA000Q9
XIUSA04FFB
XIUSA04FFD
XIUSA000PR
XIUSA000PS
XIUSA04FFU
XIUSA04FG2
XIUSA000Q0
XIUSA04FIA
XIUSA04FIF
XIUSA04FIQ
XIUSA000PT
XIUSA000PU
XIUSA000Q5
XIUSA04FJC
XIUSA000PV
XIUSA04FJL
XIUSA04FJQ
XIUSA04FJS
XIUSA04FK9
XIUSA04FKC
XIUSA04FKI
XIUSA04ABV
XIUSA04FKL
XIUSA000RN
XIUSA04FKO
XIUSA04FKR
XIUSA04FJV
XIUSA000RA
XIUSA04FJY
XIUSA000PM
XIUSA04FK4
XIUSA04FL1
XIUSA04FLF
XIUSA04FLH
XIUSA04FLP
XIUSA04FLX
XIUSA04FM5
XIUSA04FM9
XIUSA04FMH
XIUSA04FML
FOUSA06WLF
FOUSA06WLE
FOUSA05SKY
FOUSA05SP9
FOUSA05SPD
FOUSA05PRG
FOUSA05SIV
FOUSA05SIW
XIUSA04GQS
XIUSA04GQY
XIUSA04GR0
XIUSA04GR1
FOUSA06N01
XIUSA04G92
F00000LZRS
FOUSA088KU
FOUSA06SQ9
FOUSA088L0
FOUSA088RM
FOUSA060JF
FOUSA07XUG
FOUSA08X95
FOUSA08X98
FOUSA06CXK
FOUSA05LU0
F00000ITY7
F00000ITY6
F00000ITY5
F00000J7UY
FOUSA05TLG
F00000ITYA
FOUSA08ON4
FOUSA06GAN
FOUSA06GAM
F00000JYLX
FOUSA08OUA
FOUSA06GGF
FOUSA08640";

	
	$codes=explode("\n", $codestring);

	foreach ($codes as $benchmarkid)
	{
		$benchmarkid=trim($benchmarkid);
		LiveOutput("benchmark",$benchmarkid);

		if (strlen($benchmarkid)>6){
				
			$url="http://lt.morningstar.com/api/rest.svc/timeseries_cumulativereturn/jrhe6v87w5?type=MSID&timeperiod=120&frequency=daily&outputType=XML&id=".$benchmarkid.']8]0]CAALL$$ALL';
						
			try{
				$xmlretstring=(getGeneric($url));
				//echo $fund."<br>";
				$returnContents="";
				$xmlret=simplexml_load_string($xmlretstring);
				foreach ($xmlret->Security->CumulativeReturnSeries->HistoryDetail as $History){
					$hdate=$History->EndDate;
					$hvalue=$History->Value;
					$hdateparts=explode("-",$hdate);
					$outvalue=(float)$hvalue+100.00;
					$outdate=$hdateparts[2]."/".$hdateparts[1]."/".$hdateparts[0];
					$returnContents.=$benchmarkid.",".$outvalue.",".$outdate."\r\n";
				}
					
			}
			catch (Exception $e)
			{
				capturefailed('capture failed ' . $e->getMessage());
				return "failed";
			}
				
			if (!(file_put_contents($filenamereturns, $returnContents, FILE_APPEND)===false)){
				echo $filenamereturns." sucess"."<br>";
			} else {
				echo $filename." FAILED - UNABLE TO CREATE FILE"."<br>";
			}
		}
			
	}

return $results;
}

// MAIN

if (NEOCAPTURE_DEBUG_ECHO) echo 'Morningstar Prices, Start' . PHP_EOL;

if (NEOCAPTURE_DEBUG_ECHO) echo '  Get Instrument IDs from Venice' . PHP_EOL;

$msCodes = getMorningstarTest();

if (NEOCAPTURE_DEBUG_ECHO) echo '  ' . count($msCodes[0]) . ' Instruments returned.' . PHP_EOL;

$results = GetMorningstarPrices($msCodes);

if (count($results) > 1)
{
	$success = true;
} else
{
	$success = false;
}


if ($success)
{

	try
	{

		/*
		 $capture = array();
		$capture['setID'] = 7;
		$capture['dateandtime'] = convertToSQLDate(time());
		$capture['result'] = 'success';
		$sql_DateEntered = get_DateNow_sqlsvr();

		$sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);

		if (NEOCAPTURE_DEBUG_ECHO) echo '  Capture ID : ' . $sqlsvr_id . PHP_EOL;

		$results_array = add_morningstar_prices_sqlsrv($msCodes, $results, $sql_DateEntered);
		*/

	}
	catch (Exception $e)
	{
		if (NEOCAPTURE_DEBUG_ECHO) echo 'write failed ' . $e->getMessage() . PHP_EOL;

		//capturefailed('write failed ' . $e->getMessage());
		exit;
	}

}
else
{
	if (NEOCAPTURE_DEBUG_ECHO) echo 'capture failed';
	//capturefailed('failed');
	exit;
}

if (NEOCAPTURE_DEBUG_ECHO) echo 'Done.' . PHP_EOL;

?>
<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

require_once('localise/localise.php');

if (NEOCAPTURE_DEBUG_ECHO) echo 'CaptureTradefiles, Start' . PHP_EOL;

//ini_set('display_errors', 1);

require_once(NEOCAPTURE_ROOT . '/data/data_tradefiles.php');
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');

// this is where portfolios are extracted from the database and returned in html
$break = array(0, 6, 9, 11, 19, 27, 35, 56, 57, 61, 73, 103, 105, 125, 181, 197, 200);

// Existing file information

if (NEOCAPTURE_DEBUG_ECHO) echo '  Get get_ExistingTradefiles_sqlsvr()' . PHP_EOL;

$PreviousCheckedFiles = get_ExistingTradefiles_sqlsvr();
$previousFilenames = $PreviousCheckedFiles['filename'];
$previousPriorities = $PreviousCheckedFiles['priority'];

$datenow = new DateTime('now');

if (NEOCAPTURE_DEBUG_ECHO) echo '  ' . sprintf('%d', count($previousFilenames)) . ' files previously checked.' . PHP_EOL;

/* $trades_dir :  array(<Sub Directory>, <use tmp>, <priority>, <CheckFileNamePrevious>)  */
/* Do not check file names for Lux & Paris directories as these file names are always the same and the files do not sit there very long. */

$trades_dir = array(
  array('Paris', 1, 1, 0),
  array('ParisRB', 1, 1, 0),
  array('Lux', 1, 1, 0),
  array('SENT', 0, 10, 1)
);

$Paris_Formatter_Decimal = numfmt_create( PARIS_TRADEFILES_LOCALE, NumberFormatter::DECIMAL );
$Lux_Formatter_Decimal = numfmt_create( LUX_TRADEFILES_LOCALE, NumberFormatter::DECIMAL );

$tmp_dir = NEOCAPTURE_ROOT . '/tmp';
$sql_DateEntered = get_DateNow_sqlsvr();

foreach ($trades_dir as $thisDirectory)
  {
  $location = $thisDirectory[0];
  $directoryName = NEOCAPTURE_TRADEFILES_ROOT . '/' . $location;
  $useTmp = $thisDirectory[1];
  $thisPriority = $thisDirectory[2];

  if (NEOCAPTURE_DEBUG_ECHO) echo '  Starting ' . $directoryName . PHP_EOL;

  $directoryFiles = scandir($directoryName);
  $filecount=0;

  try
    {
    if ($directoryFiles!==false)
    foreach ($directoryFiles as $thisFile)
      {
      // is this file is a directory, especially . and ..

      if (is_dir($directoryName . '/' . $thisFile))
        {
        continue;
        }

      if (NEOCAPTURE_DEBUG_ECHO) echo '    Checking : ' . $thisFile . PHP_EOL;

      // Don't check files that are too old, say 30 days or more.

      $fileDate = new DateTime('@' . filemtime($directoryName . '/' . $thisFile));

      $fileage = date_diff($datenow, $fileDate);

      if ($fileage->days > 30)
        {
        if (NEOCAPTURE_DEBUG_ECHO) echo '      Too old. (' . (string)(date_diff($datenow, $fileDate)->days) . ' days.)' . PHP_EOL;
        continue;
        }

      // OK, continue.

      $keyVal = array_search($thisFile, $previousFilenames);

      $filenameTest = substr($thisFile, 0, 1);

      if (($keyVal !== false) && ($thisDirectory[3] > 0))
        {
        // Previously checked this file, but what directory ?

        if ($thisPriority <= $previousPriorities[$keyVal])
          {
          // skip this file, it has been done before, at this or higher priority directory.
          if (NEOCAPTURE_DEBUG_ECHO) echo '    Skipping file, done already.' . PHP_EOL;

          continue;
          }
        }

        $tst = strpos("P V",$filenameTest);
        if ( $tst === false )
        {
          // skip this file, it is not a valid opcvm trade file-
          // Luxembourg tradefiles = PXXXXXX
          // Paris tradefile = VXXXXXXXX
          if (NEOCAPTURE_DEBUG_ECHO) echo '    Skipping file, not valid tradefile.' . PHP_EOL;

          continue;
        }




      // process file

      // if this is not in 'SENT' then I want to copy the file to the tmp directory before looking in it.
      // This is in order to minimise the chances of file conflicts.

      if ($useTmp)
        {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    Copying to tmp directory : ' . (NEOCAPTURE_ROOT . '/tmp/' . $thisFile) . PHP_EOL;

        copy(($directoryName . '/' . $thisFile), (NEOCAPTURE_ROOT . '/tmp/' . $thisFile));
        $filecontents = file_get_contents(NEOCAPTURE_ROOT . '/tmp/' . $thisFile);
        }
      else
        {
        $filecontents = file_get_contents($directoryName . '/' . $thisFile);
        }

      $lines = explode("\n", $filecontents);

      if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    File has $d lines.', count($lines)) . PHP_EOL;

      $capture = array();
      $capture['setID'] = 5;
      $capture['priority'] = $thisPriority;
      $capture['filename'] = $thisFile;
      $capture['result'] = 'success';

      $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);

      // Process lines :

      foreach ($lines as $line)
        {
        if (strlen($line) > 20)
          {

          try
            {

            if (substr($thisFile, 0, 1) == "P")
              {
              // Luxembourg Trade Files

              $cells = explode(";", $line);

              //foreach ($cells as $cell)
              //  {
              //  //echo "<td>".$cell."</td>";
              //  }

              $tradefile = array();
              $tradefile['tradefilename'] = $thisFile;
              $tradefile['tradefiledate'] = substr($fileDate->format('Y-m-d H:i:s.u'), 0, 23);
              $tradefile['status'] = (($thisPriority == 1) ? "PENDING" : "SENT");
              $tradefile['office'] = "LUX";
              $tradefile['sophis_refcon'] = trim($cells[1]);
              $tradefile['instrumentcodetype'] = trim($cells[4]);
              $tradefile['instrumentcode'] = trim($cells[5]);
              $tradefile['transactiontype'] = trim($cells[3]);
              $tradefile['units'] = numfmt_parse($Lux_Formatter_Decimal, str_replace(' ','', $cells[12])); // trim($cells[12])
              $tradefile['value'] = numfmt_parse($Lux_Formatter_Decimal, str_replace(' ','', $cells[9])); // trim($cells[9]); //
              $tradefile['currency'] = trim($cells[10]);
              $tradefile['priority'] = $thisPriority;
              $tradefile['directory'] = $location;
              $tradefile['createoramend'] = '';

              $tradefile['lux_account'] = trim($cells[2]);
              $tradefile['paris_securitiesaccount'] = '';

              //add_tradefiles($tradefile, $sql_DateEntered);
              $tradefile['captureID'] = $sqlsvr_id;
              add_tradefiles_sqlsrv($tradefile, $sql_DateEntered);

              }
            else
              {
              // Paris Trade Files

              $cells = array();

              for ($j = 1; $j < count($break); $j++)
                {
                $cell = substr($line, $break[$j - 1], $break[$j] - $break[$j - 1]);
                //echo "<td>".$cell."</td>";
                $cells[] = $cell;
                }

              //pick up the last part of the line
              $cell = substr($line, $break[count($break) - 1], strlen($line) - $break[count($break) - 1] - 1);
              //echo "<td>".$cell."</td>";
              $cells[] = $cell;

              // PARIS_TRADEFILES_LOCALLE (Paris trade files are written using a fr-FR locale, assume space and commas in formatted numbers...

              $tradefile = array();
              $tradefile['tradefilename'] = $thisFile;
              $tradefile['tradefiledate'] = substr($fileDate->format('Y-m-d H:i:s.u'), 0, 23);
              $tradefile['status'] = (($thisPriority == 1) ? "PENDING" : "SENT");
              $tradefile['office'] = "PARIS";
              $tradefile['sophis_refcon'] = trim($cells[4]);
              $tradefile['instrumentcodetype'] = trim($cells[8]);
              $tradefile['instrumentcode'] = trim($cells[9]);
              $tradefile['transactiontype'] = trim($cells[11]);
              $tradefile['units'] = numfmt_parse($Paris_Formatter_Decimal, str_replace(' ','', $cells[12])); // trim($cells[12]);
              $tradefile['value'] = numfmt_parse($Paris_Formatter_Decimal, str_replace(' ','', $cells[14])); // trim($cells[14]);
              $tradefile['currency'] = trim($cells[15]);
              $tradefile['createoramend'] = trim($cells[7]);
              $tradefile['priority'] = $thisPriority;
              $tradefile['directory'] = $location;
              $tradefile['createoramend'] = '';

              $tradefile['lux_account'] = '';
              $tradefile['paris_securitiesaccount'] = trim($cells[6]);

              //$tradefile['lux_account']=NULL;

              //add_tradefiles($tradefile, $sql_DateEntered);
              $tradefile['captureID'] = $sqlsvr_id;
              add_tradefiles_sqlsrv($tradefile, $sql_DateEntered);

              }

            } catch (Exception $e)
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
            capturefailed('Tradefile, failed to save trade : ' . $e->getMessage(), $thisFile);
            }
          }
        }

      if ($useTmp)
        {
        @unlink((NEOCAPTURE_ROOT . '/tmp/' . $thisFile));
        }

      } // foreach ($directoryFiles as $thisFile)
    } catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
    capturefailed('Tradefile, failed to process file : ' . $e->getMessage(), $thisFile);
    }

  } // foreach ($trades_dir as $thisDirectory)

?>
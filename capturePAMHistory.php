<?php
/**
 * Collect price history from Morningstar and save into csv file to be submitted to PAM website
 *
 * This routine is run on-demand and collects a history of prices for each of the PAM funds putting
 * the results in a csv file which is formatted specifically for upload to the PAM website. Normally 
 * run once per day manually
 *
 * @package utah
 *
 */
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

/**
 * Application constants
 */
require_once('localise/localise.php');
/**
 * General functions for database connection and usage
 */
require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');
/**
 * Curl routines
 */
require_once(NEOCAPTURE_ROOT . '/functions/curl_neolink.php');

$public_dir = '/mnt/public';
$date = date('dmYHis');
$filename=$public_dir."/Performance.csv";
$fileContents="";


/**
 * Live debug information
 * @param unknown_type $tag
 * @param unknown_type $message
 */
function LiveOutput($tag, $message)
  {
  $channel = "d994fTXc";
  file_get_contents("http://liveoutput.com/log.php?channel=" . URLEncode($channel) . "&tag=" . URLEncode($tag) . "&message=" . URLEncode($message));
  }
/**
 * Move the comma in a string representation of a number two figures to the left
 * @param unknown_type $performString
 * @return string
 */
function moveComma($performString){
	$posString="";
	//check if negative
	$posString=ereg_replace("-", "", $performString);
	if (strlen($posString)!=strlen($performString)){
		$negative=true;
	}else{
		$negative=false;
	}
	//find the comma
	$posComma=strpos($posString,",");
	if ($posComma==2){$posString="0,".ereg_replace(",", "", $posString);}
	if ($posComma==1){$posString="0,0".ereg_replace(",", "", $posString);}
	if ($negative){
		$result="-".$posString;	
	} else {
		$result=$posString;
	}
	return $result;
}  
/**
 * Extract price histories for PAM funds from Morningstar and write to file
 * 
 * For each PAM fund extract the historic price series from Morningstar; create a single csv file
 * containing the date and price for all dates for all funds
 * @return string ("failed" if error)
 */  
function getPAMHistory(){
	
	global $filename, $fileContents;
	
	$isins="FR0000443954;FR0000443996;FR0000444002;FR0000981193;FR0010564245;FR0010606251;FR0010894634;FR0010937847;FR0011060300;FR0011144195;LU0581204301;LU0581205290";
	 
	$funds="F0GBR04G59,F0GBR04G5B,F0GBR04G5D,F0GBR04QZJ,F000001UGS,F0000024R2,F00000J4FQ,F00000JV3B,F00000MO1X,F00000NOLI,F00000MJ7D,F00000MJ7E";
	
	$header="ISIN;VL;Date";
	
	$urlbase="https://lt.morningstar.com/api/rest.svc/timeseries_price/jrhe6v87w5?idtype=ISIN&timeperiod=120&frequency=daily&id=";
	
	$fundArray=explode(";",$isins);
	
	$fileContents=$header."\r\n";
	
	foreach ($fundArray as $fund){

		$capture=array();
		$capture['ISIN']="";
		$capture['VL']="";
		$capture['DATE']="";

		$url=$urlbase.$fund;

		try{
			$xmlstring=(getGeneric($url));
			echo $fund."<br>";
			$xml=simplexml_load_string($xmlstring);
			foreach ($xml->Security->HistoryDetail as $History){
				$hdate=$History->EndDate;
				$hvalue=$History->Value;
				$hdateparts=explode("-",$hdate);
				$outvalue=str_replace(".", ",", $hvalue);
				$outdate=$hdateparts[2]."/".$hdateparts[1]."/".$hdateparts[0];
				$fileContents.=$fund.";".$outvalue.";".$outdate."\r\n";
			}
			
		}
		catch (Exception $e)
		{
			capturefailed('capture failed ' . $e->getMessage());
			return "failed";
		}
		
		

	}
	if (!(file_put_contents($filename, $fileContents)===false)){
		echo $filename." sucess"."<br>";
		return "sucess";
	} else {
		echo $filename." FAILED - UNABLE TO CREATE FILE"."<br>";
		return "write failed";
	}
}
  
//collect the post variables for the login
  

//first assume that session is still valid and try and go straight to the confirmations table

if (NEOCAPTURE_DEBUG_ECHO) echo 'PAM Prices, Start' . PHP_EOL;

$result = "failed";

//if first try is not successful then walk through logon procedure

if ($result == "failed")
  {
  $complete = false;
  $trycount = 0;
  $loginCount = 0;
  do
    {
    switch ($result)
    {
      case "failed":
       if ($loginCount >= 6){
          $loginCount = 0;
          unlink($ckfile);
        }

        $result = getPAMHistory();
        $loginCount++;
        break;

      case "sucess":
        $complete = true;
        $success = true;
        break;
        
      case "write failed":
		$complete = true;
		$success = false;
		break;  
    }
    $trycount++;
    if ($trycount > 15)
      {
      $complete = true;
      $success = false;
      }

    if (NEOCAPTURE_DEBUG_ECHO) echo '    Next Result : '.$result . PHP_EOL;

    } while (!$complete);
  }

if ($success)
  {

  //extract data from xml


  //extract the table containing the holdings

  if (NEOCAPTURE_DEBUG_ECHO) echo 'Data Extracted : '. PHP_EOL;

  // write to database

  try
    {
	    
	
	    
    } catch (Exception $e)
    {
	    
	    exit;
    }

  }
else
  {
  
  exit;
  }
/*
@chmod ($ckfile, 0777);
@chgrp ($ckfile, 'primonial');
@chown ($ckfile, 'www-data');
*/
if (NEOCAPTURE_DEBUG_ECHO) echo 'Done.' . PHP_EOL;



?>
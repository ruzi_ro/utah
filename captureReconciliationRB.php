<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

require_once('localise/localise.php');

require_once(NEOCAPTURE_ROOT . '/functions/curl_neolink.php');
require_once(NEOCAPTURE_ROOT . '/functions/tidy_functions.php');
require_once(NEOCAPTURE_ROOT . '/functions/dom_functions.php');
require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');
require_once(NEOCAPTURE_ROOT . '/data/data_capture.php');
require_once(NEOCAPTURE_ROOT . '/data/data_reconciliation_viewbyfund.php');
require_once(NEOCAPTURE_ROOT . '/data/data_reconciliation_inventory.php');
require_once(NEOCAPTURE_ROOT . '/data/data_reconciliation_availabilities.php');
require_once(NEOCAPTURE_ROOT . '/data/data_reconciliation_movement.php');
require_once(NEOCAPTURE_ROOT . '/data/data_reconciliation_order.php');
require_once(NEOCAPTURE_ROOT . '/data/data_reconcilliation_dashboard.php');

require_once(NEOCAPTURE_ROOT . '/functions/neolink_calls.php');

//debug info
function LiveOutput($tag, $message)
  {
  $channel = "d994fTXc";
  file_get_contents("http://liveoutput.com/log.php?channel=" . URLEncode($channel) . "&tag=" . URLEncode($tag) . "&message=" . URLEncode($message));
  }

ini_set('display_errors', 1);

//collect the post variables for the login

$post = array();
$post['accessCode'] = NEOLINK_BNP_USERNAME;
$post['accessPass'] = NEOLINK_BNP_PASSWORD;
$post['appId'] = '-1';
$post['appURL'] = '';
$post['authLevel'] = '1';
$post['locale'] = 'en';
//$search=$_GET['search'];

//create a post string to pass to the login curl script

$ckfile = NEOCAPTURE_ROOT . '/tmp/UTAH_CURLCOOKIE_REC.txt';
$path_parts = pathinfo($ckfile);
if (!is_dir($path_parts['dirname']))
  {
  echo 'The directory `'.$path_parts['dirname'].'` does not exist, it is not possible to create the curl cookie file.';
  }

$url_content = "";
$token = "";
$post_params_1 = "";


function capturefailed($message)
  {
  $capture = array();
  $capture['setID'] = 4;
  $capture['dateandtime'] = convertToSQLDate(time());
  $capture['result'] = $message;
  $sql_DateEntered = get_DateNow_sqlsvr();
  add_capture_sqlserver($capture, $sql_DateEntered);
  }

//first assume that session is still valid and try and go straight to the confirmations table

if (NEOCAPTURE_DEBUG_ECHO) echo 'Reconciliation information, Start' . PHP_EOL;

/* Always log in from scratch
 $success = true;
 $result = FirstTrypostConfirmationsAll();
 */

$result = "failed";

//if first try is not successful then walk through logon procedure

if ($result == "failed")
  {
  $complete = false;
  $trycount = 0;
  $loginCount = 0;
  do
    {
    LiveOutput("result:", $result);
    switch ($result)
    {
      case "failed":
        if ($loginCount >= 6)
          {
          $loginCount = 0;
          unlink($ckfile);
          }

        $result = getLoginScreen();
        $loginCount++;
        break;

      case "login screen":
        if ($loginCount >= 6)
          {
          $loginCount = 0;
          unlink($ckfile);
          $result = 'failed';
          break; // so it does getLoginScreen() again.
          }

        $result = postLoginScreen();
        $loginCount+=2;
        break;

      case "loginContinuation":
        $result = loginScreenContinue();
        break;
        
      case "home post":
         $result = homePost();
         break;

      case "timeout login screen":
        $result = postTimeoutLoginScreen();
        //neolink returns a form with a 'post' of this.  This is a get. Have not tested.
        $result = getAdministrationByFund();
        break;
      case "menu page":
        $result = getPortalPage();
        break;
      case "portal page":
        //$result = postRocheBrune();
        $result = getAdministrationByFund();
        break;
      case "rochebrune":
        $result = getAdministrationByFund();
        break;
      case "waiting page":
        $result = getAdministrationByFundFollow();
        break;
      case "view by fund":
        $complete = true;
        $success = true;
        break;
    }
    $trycount++;
    if ($trycount > 30)
      {
      $complete = true;
      $success = false;
      }

    if (NEOCAPTURE_DEBUG_ECHO) echo '    Next Result : '.$result . PHP_EOL;

    } while (!$complete);
  }

if ($success)
  {

  //clean up content by removing non blank spaces

  $url_clean = str_replace('&nbsp;', "", $url_content);
  $url_content = $url_clean;

  //extract the table containing the holdings

  if (NEOCAPTURE_DEBUG_ECHO) echo '  URLContent (a): '.strlen($url_content) . PHP_EOL;

  $select = "#ContentTable_ADF_PAGE_LISTE_NAV";
  try
    {
    $url_content = tidy_clip(phpq_extract($url_content, $select));
    //$url_content = dom_remove_elements($url_content);
    ////$url_content = phpq_remove_attributes($url_content);
    //$url_content = phpq_remove_links($url_content);

    if (NEOCAPTURE_DEBUG_ECHO) echo '  URLContent (b): '.strlen($url_content) . PHP_EOL;

    $url_content = dom_remove_elements_attributes_links($url_content);  // Combined function

    if (NEOCAPTURE_DEBUG_ECHO) echo '  URLContent (c): '.strlen($url_content) . PHP_EOL;

    } catch (Exception $e)
    {
    capturefailed('extract failed');
    exit;
    }

  // write to database

  try
    {
    //$sql_DateEntered = date('Y-m-d H:i:s.u'); // /* ODBC canonical (with milliseconds) yyyy-mm-dd hh:mi:ss.mmm(24h)  */
    $sql_DateEntered = get_DateNow_sqlsvr();

    $capture = array();
    $capture['setID'] = 4;
    $capture['dateandtime'] = convertToSQLDate(time());
    $capture['result'] = 'success';
    $id = add_capture($capture);
    $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);

    if (NEOCAPTURE_DEBUG_ECHO) echo '  $id        : '.$id . PHP_EOL;
    if (NEOCAPTURE_DEBUG_ECHO) echo '  $sqlsvr_id : '.$sqlsvr_id . PHP_EOL;

    if (NEOCAPTURE_DEBUG_ECHO) echo '  Save `ViewByFund` ' . PHP_EOL;

    $funds = dom_viewbyfund_table($url_content, $id, $sqlsvr_id, $sql_DateEntered);

    if (NEOCAPTURE_DEBUG_ECHO) echo '    ' . sprintf('%4d', count($funds)) . ' funds to save.' . PHP_EOL;

    //cycle through dashboards and tabs
    //$funds=get_viewbyfund_by_capture($id, $sqlsvr_id);

    $count = 0;
    $lastcode = '';
    $lastdate = '';

    foreach ($funds as $fund)
      {
      $fundcode = $fund['neo_fundcode'];
      $funddate = $fund['neo_navdate'];
      $fundname = $fund['neo_funddescription'];
      
      LiveOutput("fund", $fundname);

      if (substr($fundname, 0, 3) == 'XXX')
        {
        if (NEOCAPTURE_DEBUG_ECHO) echo '  Skipping fund (' . sprintf(($count+1), '%4d') . ' of ' . sprintf(count($funds), '%4d') . '): '.$fundcode . ', ' . $fund['neo_funddescription'] . PHP_EOL;
        }
      else
        {

        if (NEOCAPTURE_DEBUG_ECHO) echo '  Saving fund (' . sprintf(($count+1), '%4d') . ' of ' . sprintf(count($funds), '%4d') . '): '.$fundcode . ', ' . $fund['neo_funddescription'] . PHP_EOL;

        //if no fundcode use the previous one
        if (strlen($fundcode) == 0)
          {
          $fundcode = $lastcode;
          $funddate = $lastdate;
          }
        $lastcode = $fundcode;
        $lastdate = $funddate;

        $result = getDashboard($count, $fundcode);

        //topdiv

        $select="#bodyDetail";
        $url_extract = tidy_clip(phpq_extract($url_content, $select));
        $url_extract = dom_remove_elements_attributes_links($url_extract);  // Combined function
        if (NEOCAPTURE_DEBUG_ECHO) echo '      Top Div Table ('.strlen($url_extract).')'. PHP_EOL;
        $names = dom_topdiv_table($url_extract, $id, $sqlsvr_id, $fundcode, $sql_DateEntered);

        if (array_key_exists('neo_navdate', $names))
          {
          $thisFundNavDate = $names['neo_navdate'];
          }
        else
          {
          $thisFundNavDate = $funddate;
          }

        //ContentTable_ADF_VALO_LISTE
        
        $select="#ContentTable_ADF_VALO_LISTE";
        $url_extract = tidy_clip(phpq_extract($url_content, $select));
        $url_extract = dom_remove_elements_attributes_links($url_extract);  // Combined function
        if (NEOCAPTURE_DEBUG_ECHO) echo '      Valuation ('.strlen($url_extract).')'. PHP_EOL;
        dom_valuation_table($url_extract, $id, $sqlsvr_id, $thisFundNavDate, $fundcode, $sql_DateEntered);
        
        //ContentTable_ADF_EXCHANGE_RATE_LISTE
        
        $select="#ContentTable_ADF_EXCHANGE_RATE_LISTE";
        $url_extract = tidy_clip(phpq_extract($url_content, $select));
        $url_extract = dom_remove_elements_attributes_links($url_extract);  // Combined function
        if (NEOCAPTURE_DEBUG_ECHO) echo '      Exchange Rate ('.strlen($url_extract).')'. PHP_EOL;
        dom_exchangerate_table($url_extract, $id, $sqlsvr_id, $thisFundNavDate, $fundcode, $sql_DateEntered);
        
        //ContentTable_ADF_FEES_LISTE
        
        $select="#ContentTable_ADF_FEES_LISTE";
        $url_extract = tidy_clip(phpq_extract($url_content, $select));
        $url_extract = dom_remove_elements_attributes_links($url_extract);  // Combined function
        if (NEOCAPTURE_DEBUG_ECHO) echo '      Fees List ('.strlen($url_extract).')'. PHP_EOL;
        dom_feeslist_table($url_extract, $id, $sqlsvr_id, $thisFundNavDate, $fundcode, $sql_DateEntered);
        
        //ContentTable_ADF_FEES_AMOUNT_LISTE
        
        $select="#ContentTable_ADF_FEES_AMOUNT_LISTE";
        $url_extract = tidy_clip(phpq_extract($url_content, $select));
        $url_extract = dom_remove_elements_attributes_links($url_extract);  // Combined function
        if (NEOCAPTURE_DEBUG_ECHO) echo '      Fees Amount ('.strlen($url_extract).')'. PHP_EOL;
        dom_feesamount_table($url_extract, $id, $sqlsvr_id, $thisFundNavDate, $fundcode, $sql_DateEntered);
        
        //result_1 (div)
        
        $select="#result_1";
        $url_extract = tidy_clip(phpq_extract($url_content, $select));
        $url_extract = dom_remove_elements_attributes_links($url_extract);  // Combined function
        if (NEOCAPTURE_DEBUG_ECHO) echo '      Result Table ('.strlen($url_extract).')'. PHP_EOL;
        dom_result_table($url_extract, $id, $sqlsvr_id, $thisFundNavDate, $fundcode, $sql_DateEntered);

        // Inventory Tab :->

        $result = postInventoriesTab($funddate, $fundcode);

        //extract the inventory tab data
        $result = postInventoriesTabAll($fundcode);
        $select = "#ContentTable_ADF_PAGE_LISTE_INVENTORY_TABSHEET";
        try
          {
          $url_content = tidy_clip(phpq_extract($url_content, $select));
          //$url_content = dom_remove_elements($url_content);
          //$url_content = phpq_remove_links($url_content);
          $url_content = dom_remove_elements_attributes_links($url_content);  // Combined function

          if (NEOCAPTURE_DEBUG_ECHO) echo '      Saving Inventory' . PHP_EOL;

          dom_inventory_table($url_content, $fundcode, $id, $sqlsvr_id, $funddate, $sql_DateEntered);
          } catch (Exception $e)
          {
          $result = "inventory extract failed";
          }

        $message = $result . " " . $fundcode . "<br>";
        //LiveOutput("debug", $message);

         //extract the availabilities tab data
        $result = postAvailabilitiesTab($funddate,$fundcode);

        if ($result=='retry')
          {
          $result = postAvailabilitiesTab($funddate,$fundcode);
          }

        $select = "#ContentTable_ADF_PAGE_LISTE_AVAILABILITY_TABSHEET";
        try
          {
          $url_content = tidy_clip(phpq_extract($url_content, $select));
          //$url_content = dom_remove_elements($url_content);
          //$url_content = phpq_remove_links($url_content);
          $url_content = dom_remove_elements_attributes_links($url_content);  // Combined function

          if (NEOCAPTURE_DEBUG_ECHO) echo '      Saving Availabilities' . PHP_EOL;

          dom_availabilities_table($url_content, $fundcode, $id, $sqlsvr_id, $funddate, $sql_DateEntered);
          } catch (Exception $e)
          {
          $result = "inventory extract failed";
          }

        $message = $result . " " . $fundcode . "<br>";
        //LiveOutput("debug", $message);
        
        
        //extract the movement tab data
        $result = postMovementTab($funddate, $fundcode);
        $select = "#ContentTable_ADF_PAGE_LISTE_MOVEMENT_TABSHEET";
        try
          {
          $url_content = tidy_clip(phpq_extract($url_content, $select));
          //$url_content = dom_remove_elements($url_content);
          //$url_content = phpq_remove_links($url_content);
          $url_content = dom_remove_elements_attributes_links($url_content);  // Combined function

          if (NEOCAPTURE_DEBUG_ECHO) echo '      Saving Movements' . PHP_EOL;

          dom_movement_table($url_content, $fundcode, $id, $sqlsvr_id, $funddate, $sql_DateEntered);
          } catch (Exception $e)
          {
          $result = "movement extract failed";
          }

        $message = $result . " " . $funddate . " " . $fundcode . "<br>";
        //LiveOutput("debug", $message);

        //extract the order (subs and redemptions) tab data
        $result = postOrderTab($funddate, $fundcode);
        $select = "#ContentTable_ADF_PAGE_LISTE_ORDER_TABSHEET";
        try
          {
          $url_content = tidy_clip(phpq_extract($url_content, $select));
          //$url_content = dom_remove_elements($url_content);
          //$url_content = phpq_remove_links($url_content);
          $url_content = dom_remove_elements_attributes_links($url_content);  // Combined function

          if (NEOCAPTURE_DEBUG_ECHO) echo '      Saving Orders (Subscriptions / Redemptions)' . PHP_EOL;

          dom_order_table($url_content, $fundcode, $id, $sqlsvr_id, $funddate, $sql_DateEntered);
          } catch (Exception $e)
          {
          $result = "order extract failed";
          }

        $message = $result . " " . $funddate . " " . $fundcode . "<br>";
        //LiveOutput("debug", $message);

        }

      $count++;
	  
      }
    } catch (Exception $e)
    {
    capturefailed('write failed ' . $e->getMessage());
    exit;
    }

  }
else
  {
  capturefailed('failed after max attempts');
  exit;
  }

@chmod ($ckfile, 0777);
@chgrp ($ckfile, 'primonial');
@chown ($ckfile, 'www-data');

if (NEOCAPTURE_DEBUG_ECHO) echo 'Done.' . PHP_EOL;

?>
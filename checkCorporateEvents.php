<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

require_once('localise/localise.php');
require_once(LOGIN_PASSWORD_FILE);

if (NEOCAPTURE_DEBUG_ECHO) echo 'checkCorporateEvents.php, Start' . PHP_EOL;

//ini_set('display_errors', 1);

require_once(NEOCAPTURE_ROOT . '/data/data_corporateEvents.php');
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');


// Get corporate events from pending directory

$pendingDirectoryName = EVENTSFILES;
$processedDirectoryName = EVENTSFILES_PROCESSED;
//$filePattern = EVENTSFILES_PATTERN;
//$fileSeparator = EVENTSFILES_SEPARATOR;
//$pendingDirectoryName = '/mnt/events';
//$processedDirectoryName = 'processed';
$filePattern = '/EVENT/';
$fileSeparator = ',';
$eventLine = array();
$directoryFiles = false;

// intl Formatters.

$Formatter_Decimal = numfmt_create( EVENTSFILES_LOCALE, NumberFormatter::DECIMAL );
$Formatter_Date = datefmt_create ( EVENTSFILES_LOCALE, IntlDateFormatter::SHORT , IntlDateFormatter::NONE);

$thisFile = '';

if (NEOCAPTURE_DEBUG_ECHO) echo '  Starting ' . $pendingDirectoryName . PHP_EOL;


try
{
    $directoryFiles = scandir($pendingDirectoryName);
}
catch (Exception $e)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
}

try
{
    if (strlen(date_default_timezone_get()) == 0)
    {
        date_default_timezone_set('UTC');
    }
}
catch (Exception $e)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;

}

try
{
    if ($directoryFiles !== false)
    {
        foreach ($directoryFiles as $thisFile)
        {
            // is this file is a directory, especially . and .


            if (is_dir($pendingDirectoryName . '/' . $thisFile))
            {
                continue;
            }

            if (preg_match($filePattern, $thisFile))
            {
                if (NEOCAPTURE_DEBUG_ECHO) echo '    Checking : ' . $thisFile . PHP_EOL;

                /* Log this file */

                /*
                $capture = array();
                $capture['setID'] = 9;
                $capture['result'] = 'Subscriptions, Processing ' . $pendingDirectoryName . '/' . $thisFile;
                $capture['filename'] = $thisFile;
                $capture['priority'] = 0;
                $sql_DateEntered = get_DateNow_sqlsvr();
                $sqlsvr_id = add_capture_sqlserver($capture, $sql_DateEntered);
                */

                /* Process this file*/
                $filecontents = utf8_encode(file_get_contents($pendingDirectoryName . '/' . $thisFile));

                if ($filecontents === false)
                {
                    if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    Failed to read file ' . $thisFile) . PHP_EOL;

                    $DoMoveFile = false;
                }
                else
                {
                    $lines = explode("\n", $filecontents);
                    $params = array();

                    if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    File has $d lines.', count($lines)) . PHP_EOL;

                    if (count($lines) > 0)
                    {

                        foreach ($lines as $line)
                        {
                            $eventFields = explode($fileSeparator, $line);
                            $eventLine = array();


                            $nbr = (string)count($eventFields);
                            //
                            if  (count($eventFields) > 20)
                            {
                                $eventLine = array();

                                // Check numeric fields are not Zero length

                                //$eventFields[29] = str_replace(' ','', $eventFields[29]);
                                //$eventFields[30] = str_replace(' ','', $eventFields[30]);
                                //$eventFields[31] = str_replace(' ','', $eventFields[31]);
                                //
                                //if (strlen($eventFields[29]) == 0) $eventFields[29] = "0";
                                //if (strlen($eventFields[30]) == 0) $eventFields[30] = "0";
                                //if (strlen($eventFields[31]) == 0) $eventFields[31] = "0";

                                //

                                //if ($eventFields[9] == 'Annonce' or $eventFields[9] == 'Avis' ) // event Line
                                //{
                                    //$eventLine['fileName'] = $thisFile;
                                    //$eventLine['text'] = $line;

                                    $eventLine['isinCode'] = trim($eventFields[0]);
                                    echo "ISIN : " .$eventLine['isinCode'] . "<BR>";
                                    $eventLine['securitiesDescription'] = trim($eventFields[1]);
                                    $eventLine['category'] = trim($eventFields[2]);
                                    $eventLine['type'] = trim($eventFields[3]);
                                    $eventLine['securitiesAccountBranch'] = trim($eventFields[4]);
                                    $eventLine['securitiesAccountNumber'] = trim($eventFields[5]);
                                    $eventLine['securitiesAccountDesc'] = trim($eventFields[6]);
                                    $eventLine['bankReference'] = trim($eventFields[7]);
                                    $eventLine['statusCode'] = trim($eventFields[8]);
                                    $eventLine['event'] = trim($eventFields[9]);
                                    $eventLine['responseType'] = trim($eventFields[10]);
                                    $eventLine['clientDeadlineDate'] = trim($eventFields[11]);
                                    $eventLine['clientDeadlineTime'] = trim($eventFields[12]);
                                    $eventLine['exDate']  = trim($eventFields[13]);
                                    $eventLine['effectiveDate'] = trim($eventFields[14]);
                                    $eventLine['recordDate'] = trim($eventFields[15]);
                                    $eventLine['eligibleQuantity'] = trim($eventFields[16]);
                                    $eventLine['eligibleQuantityType'] = trim($eventFields[17]);
                                    $eventLine['updateDate'] = trim($eventFields[18]);
                                    $eventLine['updateTime'] = trim($eventFields[19]);
                                    $eventLine['messageReference'] = trim($eventFields[20]);
                                    $eventLine['pendingDeliveries'] = trim($eventFields[21]);
                                    $eventLine['pendingReceipt'] = trim($eventFields[22]);
                                    $eventLine['indicativeUninstructedQty'] = trim($eventFields[23]);
                                    $eventLine['minimumPrice'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $eventFields[24]));
                                    $eventLine['maximumPrice'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $eventFields[25]));
                                    $eventLine['minimumEligibleQty'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $eventFields[26]));
                                    $eventLine['maximumEligibleQty'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $eventFields[27]));
                                    $eventLine['acceptablePriceIncrement'] = numfmt_parse($Formatter_Decimal, str_replace(' ','', $eventFields[28]));
                                    $eventLine['countryCode'] = trim($eventFields[29]);
                                    $eventLine['localCode'] = trim($eventFields[30]);
                                    $eventLine['paymentDate'] = trim($eventFields[31]);

                                    $params[] = $eventLine;
                                //}

                            }

                        } // $line

                    } // If Count($lines)
                    else
                    {
                        if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' contained no lines. Empty or no permissions ?';
                    }

                    // Save events file entries.
                    $DoMoveFile = true;


                    if (count($params) > 0)
                    {
                        if (set_CorporateEvents_sqlsrv($params) === false)
                        {
                            $DoMoveFile = false;
                        }
                    }
                }

                if ($DoMoveFile)
                {
                    if (NEOCAPTURE_DEBUG_ECHO) echo '    moving file ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . PHP_EOL;

                    try
                    {
                        $nowDate = New DateTime();
                    }
                    catch (Exception $e)
                    {
                        // In the case of an exception, probable because the timezone is not set in php.ini, default to Paris.
                        date_default_timezone_set('Europe/Paris');
                        $nowDate = New DateTime();
                    }

                    $newFile = $nowDate->format('YmdHis') . '_' . $thisFile;

                    $copyResult = copy(($pendingDirectoryName . '/' . $thisFile), ($processedDirectoryName . '/' . $newFile));
                    if ($copyResult)
                    {
                        unlink(($pendingDirectoryName . '/' . $thisFile));
                    }
                    else
                    {
                        if (NEOCAPTURE_DEBUG_ECHO) echo '    Error, failed to copy ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . '/' . $newFile . PHP_EOL;
                    }

                }
                else
                {
                    if (NEOCAPTURE_DEBUG_ECHO) echo '    not moving file ' . $pendingDirectoryName . '/' . $thisFile . ' - Update did not work.' . PHP_EOL;
                }

            } // If preg_match()
            else
            {
                if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' did not match the given pattern (' . $filePattern . ')';
            }

        } // For Each $directoryFiles as $thisFile

    } // If $directoryFiles !== false
    else
    {
        if (NEOCAPTURE_DEBUG_ECHO) echo '    directory is empty.';
    }

    if (NEOCAPTURE_DEBUG_ECHO) echo '  Ending ' . $pendingDirectoryName . PHP_EOL;

}
catch (Exception $e)
{
    if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
    capturefailed('checkCorporateEvents, failed to process file : ' . $e->getMessage(), $thisFile);
}






?>

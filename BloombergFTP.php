<?php

$ftp_server = 'bfmrr.bloomberg.com';
$ftp_user_name = 'PRIMFTP';
$ftp_user_pass = 'pM5I8tE=';
$directory = '/mnt/bloombergFtpFiles/';
$processedDirectory = '/mnt/bloombergFtpFiles/processed/';

// Connexion
$conn_id = ftp_connect($ftp_server);

// Authenticate
$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

$contents = ftp_nlist($conn_id, ".");

function tst($file) {
    return preg_match('/BlockFTP/', $file) > 0;
}

$filtered = array_filter($contents,'tst');


foreach($filtered as $file)
{
    $local_file = $directory . $file;
    if (is_file($processedDirectory . $file) or is_file($directory . $file) )
    {
        echo "File already received - skip" . $file . PHP_EOL;
        continue;
    }

    if (ftp_get($conn_id, $local_file, $file, FTP_ASCII)) {
        echo "The file " .$file . " has been received  " . PHP_EOL;
    } else {
        echo "Unable to get the file" . $file . PHP_EOL;
    }

}

//print_r($filtered);

// Close connexion
ftp_close($conn_id);
?>

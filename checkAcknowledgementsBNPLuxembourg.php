<?php
header('Content-Type: text/html; charset=utf-8');
set_time_limit(0);

// Note the New DateTime() constructor requires that the timezone be set in the php.ini file.
// If the file is not being written, look at this.

require_once('localise/localise.php');
require_once(LOGIN_PASSWORD_FILE);

if (NEOCAPTURE_DEBUG_ECHO) echo 'checkAcknowledgementsLuxembourg, Start' . PHP_EOL;

//ini_set('display_errors', 1);

require_once(NEOCAPTURE_ROOT . '/data/data_tradefiles.php');
require_once(NEOCAPTURE_ROOT . '/data/data_validation.php');

// Get trade confirmations from pending directory

$pendingDirectoryName = LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT;
$processedDirectoryName = LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PROCESSED;
$LuxembourgFilePattern = LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PATTERN;
$acknowledgementLine=array();

$thisFile = '';

if (NEOCAPTURE_DEBUG_ECHO) echo '  Starting ' . $pendingDirectoryName . PHP_EOL;

$directoryFiles = scandir($pendingDirectoryName);
$filecount=0;

try
  {

  if ($directoryFiles!==false)
  foreach ($directoryFiles as $thisFile)
    {
    // is this file is a directory, especially . and ..

    if (is_dir($pendingDirectoryName . '/' . $thisFile))
      {
      continue;
      }
	
      
    // if (stripos($thisFile, LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PREFIX, 0) === 0)
    if (preg_match($LuxembourgFilePattern, $thisFile))
      {
      if (NEOCAPTURE_DEBUG_ECHO) echo '    Checking : ' . $thisFile . PHP_EOL;
      $filecount++;
      $results=array();

      // $filecontents = file_get_contents($pendingDirectoryName . '/' . $thisFile);
      $filecontents = utf8_encode( file_get_contents($pendingDirectoryName . '/' . $thisFile));

      $lines = explode("\n", $filecontents);

      if (NEOCAPTURE_DEBUG_ECHO) echo sprintf('    File has $d lines.', count($lines)) . PHP_EOL;

      if (count($lines) > 0)
        {

        foreach ($lines as $line)
          {
          if (strlen($line) > 55)
            {
            $fields = explode(";", $line);

            if (count($fields) >= 11)
              {
              $TransactionID = $fields[7];  // trim(substr($line, 39, 16));
              $Status = $fields[2];         // strtoupper(trim(substr($line, 55, 4)));
              $TradeStatus = trim($fields[10]);

              $acknowledgementLine = array();
              $acknowledgementLine['transactionID'] = intval($TransactionID);
              $acknowledgementLine['statusID'] = 0;
              $acknowledgementLine['tradeStatus'] = $TradeStatus;
              $acknowledgementLine['text'] = $line;

              switch ($Status) {
                case "AC":
                  $acknowledgementLine['statusID'] = 187; // Received BP2S
                  break;
                case "RJ":
                  $acknowledgementLine['statusID'] = 191; // Rejected BP2S
                  break;
              }

              $results[] = $acknowledgementLine;

              if (NEOCAPTURE_DEBUG_ECHO) echo '    ' . $TransactionID . ', statusID = ' . strval($acknowledgementLine['statusID']) . ' : ' . $line . PHP_EOL;

              }
            }
          } // For each

        } // Lines > 0
      else
        {
        capturemessage('File contained no lines');	
        if (NEOCAPTURE_DEBUG_ECHO) echo '    file ' . $pendingDirectoryName . '/' . $thisFile . ' contained no lines. Empty or no permissions ?';
        }

      // Save Acknowledgement file entries.

      set_tradeFilesAcknowledgementsBNPLuxembourg_sqlsrv($results);

      // Move Acknowledgement file to Processed directory.

      if (NEOCAPTURE_DEBUG_ECHO) echo '    moving file ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . PHP_EOL;

      try
        {
        $nowDate = New DateTime();
        }
      catch (Exception $e)
        {
        // In the case of an exception, probable because the timezone is not set in php.ini, default to Luxembourg.
        date_default_timezone_set('Europe/Paris');
        $nowDate = New DateTime();
        }

      $newFile = $thisFile . '_' . $nowDate->format('YmdHis');

      $copyResult = copy(($pendingDirectoryName . '/' . $thisFile), ($processedDirectoryName . '/' . $newFile));
      if ($copyResult)
        {
        capturemessage('success');
        unlink(($pendingDirectoryName . '/' . $thisFile));
        }
      else
        {
        capturemessage('Failed to copy');
        if (NEOCAPTURE_DEBUG_ECHO) echo '    Error, failed to copy ' . $pendingDirectoryName . '/' . $thisFile . ' to ' . $processedDirectoryName . '/' . $newFile . PHP_EOL;
        }

      }

    }

  }
catch (Exception $e)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '    Error. ' . $e->getMessage() . PHP_EOL;
  capturemessage( $e->getMessage());
  }
  
  if ($filecount==0){
  	capturemessage('No files found');
  }
  
  function capturemessage($message)
  {
  	$capture = array();
  	$capture['setID'] = 13;
  	$capture['dateandtime'] = convertToSQLDate(time());
  	$capture['result'] = $message;
  
  	$sql_DateEntered = get_DateNow_sqlsvr();
  	add_capture_sqlserver($capture, $sql_DateEntered);
  }

?>
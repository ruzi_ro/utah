<?php

// Script used to get AMF declaration files

require_once('localise/localise.php');

$sftp_server = 'hub.amf-france.org';
$port = '6321';
$sftp_user_name = 'ALTAROAM01';
$sftp_user_pass = 'shyaP9djj';
$directory = AMF_FILE_DIRECTORY  . '/feedback/';
$indexRDT = '1';

$pattern = '#^feedback_+[0-9]+\.txt$#';

$connection = ssh2_connect($sftp_server, $port);
ssh2_auth_password($connection, $sftp_user_name, $sftp_user_pass);

$sftp = ssh2_sftp($connection);

$handle = opendir("ssh2.sftp://$sftp/feedback");
while (false != ($entry = readdir($handle))){
    echo "Download next file ....." . $entry . PHP_EOL;
    $strData = utf8_encode(file_get_contents("ssh2.sftp://$sftp/feedback/$entry"));


    $local_file = $directory . $entry;
    if (is_file($local_file) )
    {
        echo "File already received - skip ......" . $entry . PHP_EOL;
    } else {
        file_put_contents($local_file, $strData);

        if (preg_match($pattern, $entry) > 0)
        {
            echo "Send the file" . $entry . " by email to MO" . PHP_EOL;

            $subject = "AMF RDT - " . $entry ;
            $message = $strData;
            $headers = "From: ITSUPPORTAM@primonial.fr";
            $mailto = "mo_pam@primonial.fr, rodolphe.ruzindana@primonial.fr, lotfi.ouederni@primonial.fr, martin.greggory@altarocca-am.fr";
            // SendMail
            mail($mailto, $subject, $message, $headers);
        }
    }

}

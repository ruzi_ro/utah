<?php
/**
 * Created by PhpStorm.
 * User: rruzinda
 * Date: 1/8/14
 * Time: 10:23 AM
 */
require_once('localise/localise.php');
require_once('functions/db_functions.php');
require_once(LOGIN_PASSWORD_FILE);
define('DELIMITER', ';');

$timestamp = date('YmdHis');

//ini_set('display_errors', 1);
$sqlsvr_conn = renaissance_connect();

//declare the SQL statement that will query the database
//$query = mssql_query($query_candidates); status 1 = Validated MO
$query = mssql_query('SELECT txn.TransactionParentID,txn.TransactionID  FROM fn_tblTransactionSingle_SelectKD(0,null,null) AS txn WHERE
txn.TransactionTradeStatusID = 1 and txn.TransactionIsFX = 1 and txn.TransactionDateEntered > "20140303"');
//and not exists
//( SELECT tradefile.TransactionParentID FROM tblTradeFileExecutionReporting AS tradeFile
//WHERE  tradefile.TransactionParentID = txn.TransactionParentID)');


$filename = EXECUTIONS_FILE_DIRECTORY . '/' . EXECUTIONS_FILE_NAME . '_' . $timestamp . '.csv';
$header = 'BOF;' . $timestamp . ';PAM_FR';
$linesNbr = mssql_num_rows($query);
$trailer = (string)$linesNbr . ';EOF';



if (!mssql_num_rows($query)) {
    echo 'No candidates found' . PHP_EOL;
} else {

    $fp = fopen($filename, 'w');
    fputs($fp,$header . "\r\n");

    while ($row = mssql_fetch_array($query)) {
        //echo $row['TransactionParentID'] . " - " . $row['TransactionID']   . PHP_EOL;
        $execsArray = get_Fxs_sqlsrv($row['TransactionParentID']);

        if (count($execsArray) > 0)
        {


            foreach ($execsArray as $val)
            {
                $reportTable = array();
                $line = implode (";", $val );

                //echo $row['TransactionParentID'] . " - " . $row['TransactionID']   . PHP_EOL;
                echo $line . PHP_EOL;
                $reportTable['AdministratorID'] = 0;
                $reportTable['TransactionParentID'] = $row['TransactionParentID'];
                $reportTable['TransactionID'] = $row['TransactionID'];
                $reportTable['TradeReportString'] = $line;
                //$report[] = $reportTable;
                //print_r($report);
                // Entry in tblTradeFileExecutionReporting table
                insertTradeFileFxLines($reportTable);

                fputs($fp, $line . "\r\n" );
                //fputs($fp, "\n");
                // Mark the entry in tblTradeFileExecutionReporting table as deleted
                // The entry has been inserted in the executions file
                set_fxDone_sqlsrv($reportTable);


            }



        }


    }
    fputs($fp,$trailer . "\r\n");
    fclose($fp);


}

mssql_free_result($query);





function get_Fxs_sqlsrv($recID){

    try
    {

        //ini_set('display_errors', 1);
        $sqlsvr_conn = renaissance_connect();


        // Create a new stored prodecure
        $stmt = mssql_init('zzz_createForexFile', $sqlsvr_conn);
        $knowledgeDate_Str = Null;
        mssql_bind($stmt, '@AuditID', &$recID, SQLINT4, false, false);
        mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate_Str , SQLVARCHAR, false, false, 20);


        $results_array = array();
        $count = 0;

        // Execute
        if ($stmt)
        {
            $proc_result = mssql_execute($stmt);

            if (($proc_result !== false) && ($proc_result !== true))
            {
                if (mssql_num_rows($proc_result) > 0)
                {

                    while ($row = mssql_fetch_assoc($proc_result))
                    {
                        $results_array[$count] = $row;
                        $count++;
                    }

                }
            }

            // Free statement
            mssql_free_statement($stmt);

        }
    } catch (Exception $e)
    {
    }

    return $results_array;

}


function insertTradeFileFxLines($results){


    try
    {

        $results_array = array();

        //print_r($results);

        $sqlsvr_conn = renaissance_connect();


        $AdministratorID = $results['AdministratorID'];
        $transactionID = $results['TransactionID'];
        $transactionParentID = $results['TransactionParentID'];
        $tradeReportString = $results['TradeReportString'];
        $knowledgeDate = Null;

        // Create a new stored prodecure
        $stmt = mssql_init('adp_tblTradeFileExecutionReporting_InsertCommand', $sqlsvr_conn);

        // Execute
        if ($stmt)
        {


            mssql_bind($stmt, '@AdministratorID', &$AdministratorID, SQLINT4, false, false);
            mssql_bind($stmt, '@TransactionID', &$transactionID, SQLINT4, false, false);
            mssql_bind($stmt, '@TransactionParentID', &$transactionParentID, SQLINT4, false, false);
            mssql_bind($stmt, '@TradeReportString', &$tradeReportString , SQLVARCHAR, false, false, 2000);
            mssql_bind($stmt, '@KnowledgeDate', &$$knowledgeDate ,SQLVARCHAR , false, false);



            mssql_execute($stmt);

            //if (mssql_num_rows($proc_result) > 0)
            //{
            //    $row = mssql_fetch_assoc($proc_result);
            //
            //    $results_array[$count++] = $row;
            //}

            // Free statement
            mssql_free_statement($stmt);
        }

     // For each.

    } catch (Exception $e)
    {
    }

    return $results_array;


}

function set_fxDone_sqlsrv($results){
    // $results is an array of associative arrays each comprising 'RN', 'filename' and TransactionParentID
    // do whatever magic is required to change the status of execs in Venice

    try
    {

        $results_array = array();


        $sqlsvr_conn = renaissance_connect();
        //ini_set('display_errors', 1);


            $UserID = 0;
            $Token  = 'Null';
            $transactionParentID = $results['TransactionParentID'];

            // Create a new stored prodecure
            $stmt = mssql_init('web_tblTradeFileExecutionReporting_Delete', $sqlsvr_conn);

            // Execute
            if ($stmt)
            {
                mssql_bind($stmt, '@UserID', &$UserID, SQLINT4, false, false);
                mssql_bind($stmt, '@Token', &$Token, SQLVARCHAR, false, false, 100);
                mssql_bind($stmt, '@TransactionParentID', &$transactionParentID, SQLINT4, false, false);

                mssql_execute($stmt);

                //if (mssql_num_rows($proc_result) > 0)
                //{
                //    $row = mssql_fetch_assoc($proc_result);
                //
                //    $results_array[$count++] = $row;
                //}

                // Free statement
                mssql_free_statement($stmt);
            }



    } catch (Exception $e)
    {
    }

    return $results_array;


}
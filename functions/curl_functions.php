<?php
require_once(NEOCAPTURE_ROOT . '/functions/logfile.php');

function get_furl($url)
{
	$furl = false;

	// First check response headers
	$headers = get_headers($url);

	// Test for 301 or 302
	if(preg_match('/^HTTP\/\d\.\d\s+(301|302)/',$headers[0]))
	{
		foreach($headers as $value)
		{
			if(substr(strtolower($value), 0, 9) == "location:")
			{
				$furl = trim(substr($value, 9, strlen($value)));
			}
		}
	}
	// Set final URL
	$furl = ($furl) ? $furl : $url;

	return $furl;
}

function scrape_web($url, $selector, $filter){
	$foundclip='';
	try{
		$mydom=str_get_html(curl_download($url));
		if (isset($filter)){
			// filter all matches and return the last one to match 'filter'
			foreach($mydom->find($selector) as $clip){
				$pos = strpos($clip, $filter);
				if ($pos === false) {

				} else {
					$foundclip=$clip;
				}
			}
		} else {
			//return the first match with no filter
			$foundclip=$mydom->find($selector,0);
		}
	}
	catch(Exception $e)  {
		$foundclip='Could not find html';
	}
	return $foundclip;
	//return "Test";
}


function extract_web($url_content, $selector, $filter){
	$foundclip='';
	try{
		$mydom=str_get_html($url_content);
		if (isset($filter)){
			// filter all matches and return all those that match 'filter'
			foreach($mydom->find($selector) as $clip){
				$pos = strpos($clip, $filter);
				if ($pos === false) {

				} else {
					$foundclip=$foundclip."\n".$clip;
				}
			}
		} else {
			//return the all matches with no filter
			foreach($mydom->find($selector) as $clip){
				$foundclip=$foundclip."\n".$clip;
			}
		}
	}
	catch(Exception $e)  {
		$foundclip='Could not find html';
	}
	$mydom->clear();
	return $foundclip;
	//return "Test";
}

function remove_links($url_content){
	try{
		$mydom= new simple_html_dom();
		$mydom=str_get_html($url_content);
		
		foreach($mydom->find('a') as $clip){
			//$clip->outertext='';
			$clip->href = null;
			$clip->onclick = null;
		}
		
	}
	catch(Exception $e)  {
		return 'ERROR remove_content';
	}
	$str=$mydom;
	return $str;
}

function remove_scripts($url_content){
	try{
		$mydom= new simple_html_dom(); 
		$mydom=str_get_html($url_content);

		foreach($mydom->find('SCRIPT') as $clip){
			$clip->outertext='';
		}

	}
	catch(Exception $e)  {
		return 'ERROR remove_content';
	}
	$str=$mydom;
	return $str;
}

function insert_currentdir($scrapeclip,$currentdir){
	$outstring='';
	$length=strlen($scrapeclip);
	$char = str_split($scrapeclip);
	for ($i=0; $i<$length; $i++){

		$last6='';
		$next1='';
		$next4='';

		//set last6
		if ($i>4){
			$last6=$char[$i-5].$char[$i-4].$char[$i-3].$char[$i-2].$char[$i-1].$char[$i];
		}

		//set next1
		if ($i<($length-1)){
			$next1=$char[$i+1];
		}

		//set next4
		if ($i<($length-4)){
			$next4=$char[$i+1].$char[$i+2].$char[$i+3].$char[$i+4];
		}

		$outstring=$outstring.$char[$i];

		if ($last6=='href="'){
			if ($next4!='http'){
				if (ctype_alpha($next1)){
					$outstring=$outstring.$currentdir;
				}
			}
		}
	}
	return $outstring;
}


function curl_download($Url){

	// is cURL installed yet?
	if (!function_exists('curl_init')){
		die('Sorry cURL is not installed!');
	}

	// OK cool - then let's create a new cURL resource handle
	$ch = curl_init();

	// Now set some options (most are optional)

	// Set URL to download
	curl_setopt($ch, CURLOPT_URL, $Url);

	// Set a referer
	curl_setopt($ch, CURLOPT_REFERER, "http://www.example.org/yay.htm");

	// User agent
	curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");

	// Include header in result? (0 = yes, 1 = no)
	curl_setopt($ch, CURLOPT_HEADER, 0);

	// Should cURL return or print out the data? (true = return, false = print)
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// Timeout in seconds
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);

	// Disable SSL checks
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

	// Download the given URL, and return output
	$output = curl_exec($ch);

	// Close the cURL resource, and free system resources
	curl_close($ch);

	return $output;
}



function curl_post($Url,$fields){

  $fields_string = '';

	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	rtrim($fields_string,'&');

	// is cURL installed yet?
	if (!function_exists('curl_init')){
		die('Sorry cURL is not installed!');
	}

	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $Url);
	curl_setopt($ch, CURLOPT_REFERER, "http://www.example.org/yay.htm");
	curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch,CURLOPT_POST,count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);

	// Download the given URL, and return output
	$output = curl_exec($ch);

	curl_close($ch);

	return $output;
}


?>

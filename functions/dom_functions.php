<?php

require_once(NEOCAPTURE_ROOT . '/functions/phpQuery-onefile.php');
require_once(NEOCAPTURE_ROOT . '/data/data_holdings.php');
require_once(NEOCAPTURE_ROOT . '/data/data_confirmations.php');

function phpq_extract($url_content, $selector)
  {
  try
    {
    $doc = phpQuery::newDocument($url_content);
    $extract = $doc->find($selector . ':first');
    }
  catch (Exception $e)
    {
    return 'ERROR remove_content';
    }
  return $extract;
  }

function dom_valuation_table($url_content, $id, $sqlsvr_id, $thisFundNavDate, $fundcode, $sql_DateEntered)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '  In `dom_valuation_table()`' . PHP_EOL;

  $rowcount = 0;

  /* Initialise 'names' array. */

//$names[] = array('Database field name', flag : Should not carry forward if true, flag : set as '' string, max string length);
// carry-over flag does not work with this code, it's only in here for consistency with other code.

  $names = array();
  $names[0] = array('neo_isin', 0, 0, 50);
  $names[1] = array('neo_sharetype', 0, 0, 50);
  $names[2] = array('neo_currency', 0, 0, 5);
  $names[3] = array('neo_nav', 0, 0, 50);
  $names[4] = array('neo_navchange', 0, 0, 50);
  $names[5] = array('neo_netassets', 0, 0, 50);
  $names[6] = array('neo_numbershares', 0, 0, 50);
  $names[7] = array('neo_subscriptions', 0, 0, 50);
  $names[8] = array('neo_redemptions', 0, 0, 50);

  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    $tbody = $doc->getElementsByTagName('table')->item(0);

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {
      foreach ($tbody->childNodes AS $row)
        {
        $rowcount++;
        $cellcount = 0;

        //ignore the first row (headings)

        if ($rowcount > 1)
          {

          $holding = array();

          foreach ($row->childNodes AS $cell)
            {
            if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
              {
              $cellValue = $cell->nodeValue;

              if ($cellcount < $names_length)
                {
                //$holding[$names[$cellcount]] = trim($cellValue);

                $dataVal = ($names[$cellcount][2] ? '' : trim($cellValue));
                if (strlen($dataVal) > 0) $dataVal = substr($dataVal, 0, $names[$cellcount][3]);
                $holding[$names[$cellcount][0]] = $dataVal;
                }
              $cellcount++;
              }
            }

          // only save rows with an ISIN number - ignore the totals row
          if (strlen($holding['neo_isin']) > 5)
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '    Saving : ISIN (' . $holding['neo_isin'] . '), NAV (' . $holding['neo_nav'] . '), Shares (' . $holding['neo_numbershares'] . '), Subscriptions (' . $holding['neo_subscriptions'] . '), Redemptions (' . $holding['neo_redemptions'] . ')' . PHP_EOL;

            if ($cellcount >= $names_length)
              {
              //$holding['captureID'] = $id;
              //add_holdings($holding);
              $holding['captureID'] = $sqlsvr_id;
              $holding['fundCode'] = $fundcode;
              $holding['NAVDate'] = $thisFundNavDate;
              add_valuation_sqlsrv($holding, $names, $thisFundNavDate, $sql_DateEntered);
              }
            else
              {
              if (NEOCAPTURE_DEBUG_ECHO) echo '*Error. Not enough table columns. Expecting ' . $names_length . ', found ' . $cellcount . PHP_EOL;

              return false;
              }
            }
          }

        } // foreach ($tbody->childNodes AS $row)
      }
    }
  catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

  }

function dom_exchangerate_table($url_content, $id, $sqlsvr_id, $thisFundNavDate, $fundcode, $sql_DateEntered)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '  In `dom_exchange_table()`' . PHP_EOL;

  $rowcount = 0;

  /* Initialise 'names' array. */

  $names = array();
  $names[0] = array('neo_currency', 0, 0, 5);
  $names[1] = array('neo_ratedate', 0, 0, 50);
  $names[2] = array('neo_rate', 0, 0, 50);
  $names[3] = array('neo_previousratedate', 0, 0, 50);
  $names[4] = array('neo_previousrate', 0, 0, 50);
  $names[5] = array('neo_percentchange', 0, 0, 50);


  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    $tbody = $doc->getElementsByTagName('table')->item(0);

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {
      foreach ($tbody->childNodes AS $row)
        {
        $rowcount++;
        $cellcount = 0;

        //ignore the first row (headings)

        if ($rowcount > 1)
          {
          $holding = array();

          foreach ($row->childNodes AS $cell)
            {
            if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
              {
              $cellValue = $cell->nodeValue;

              if ($cellcount < $names_length)
                {
                // $holding[$names[$cellcount]] = trim($cellValue);

                $dataVal = ($names[$cellcount][2] ? '' : trim($cellValue));
                if (strlen($dataVal) > 0) $dataVal = substr($dataVal, 0, $names[$cellcount][3]);
                $holding[$names[$cellcount][0]] = $dataVal;
                }
              $cellcount++;
              }
            }

          // only save rows with a Currency - ignore the totals row
          if (strlen($holding['neo_currency']) > 0)
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '    Saving : Currency (' . $holding['neo_currency'] . '), Rate Date (' . $holding['neo_ratedate'] . '), Rate (' . $holding['neo_rate'] . '), Prev date (' . $holding['neo_previousratedate'] . '), Prev rate (' . $holding['neo_previousrate'] . ')' . '), Change (' . $holding['neo_percentchange'] . ')' . PHP_EOL;

            if ($cellcount >= $names_length)
              {
              //$holding['captureID'] = $id;
              //add_holdings($holding);
              $holding['captureID'] = $sqlsvr_id;
              $holding['fundCode'] = $fundcode;
              $holding['NAVDate'] = $thisFundNavDate;
              add_exchangerate_sqlsrv($holding, $names, $thisFundNavDate, $sql_DateEntered);
              }
            else
              {
              if (NEOCAPTURE_DEBUG_ECHO) echo '*Error. Not enough table columns. Expecting ' . $names_length . ', found ' . $cellcount . PHP_EOL;

              return false;
              }
            }
          }

        } // foreach ($tbody->childNodes AS $row)
      }
    }
  catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

  }

function dom_feeslist_table($url_content, $id, $sqlsvr_id, $thisFundNavDate, $fundcode, $sql_DateEntered)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '  In `dom_feeslist_table()`' . PHP_EOL;

  $rowcount = 0;

  /* Initialise 'names' array. */

  $names = array();
  $names[0] = array('neo_class', 0, 0, 50);
  $names[1] = array('neo_feetype', 0, 0, 50);
  $names[2] = array('neo_fixedamount', 0, 0, 50);
  $names[3] = array('neo_minthreshold', 0, 0, 50);
  $names[4] = array('neo_maxthreshold', 0, 0, 50);
  $names[5] = array('neo_rate', 0, 0, 50);
  $names[6] = array('neo_currency', 0, 0, 5);

  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    $tbody = $doc->getElementsByTagName('table')->item(0);

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {
      foreach ($tbody->childNodes AS $row)
        {
        $rowcount++;
        $cellcount = 0;

        //ignore the first row (headings)

        if ($rowcount > 1)
          {
          $holding = array();

          foreach ($row->childNodes AS $cell)
            {
            if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
              {
              $cellValue = $cell->nodeValue;

              if ($cellcount < $names_length)
                {
                // $holding[$names[$cellcount]] = trim($cellValue);

                $dataVal = ($names[$cellcount][2] ? '' : trim($cellValue));
                if (strlen($dataVal) > 0) $dataVal = substr($dataVal, 0, $names[$cellcount][3]);
                $holding[$names[$cellcount][0]] = $dataVal;
                }
              $cellcount++;
              }
            }

          // only save rows with a feetype - ignore the totals row
          if (($cellcount > 2))
            {
            if ((strlen($holding['neo_feetype']) > 0))
              {
              if (NEOCAPTURE_DEBUG_ECHO) echo '    Saving : Class (' . $holding['neo_class'] . '), Type (' . $holding['neo_feetype'] . '), Min (' . $holding['neo_minthreshold'] . '), Max (' . $holding['neo_maxthreshold'] . '), Rate (' . $holding['neo_rate'] . '), Currency (' . $holding['neo_currency'] . ')' . PHP_EOL;

              if ($cellcount >= $names_length)
                {
                //$holding['captureID'] = $id;
                //add_holdings($holding);
                $holding['captureID'] = $sqlsvr_id;
                $holding['fundCode'] = $fundcode;
                $holding['NAVDate'] = $thisFundNavDate;
                add_feeslist_sqlsrv($holding, $names, $thisFundNavDate, $sql_DateEntered);
                }
              else
                {
                if (NEOCAPTURE_DEBUG_ECHO) echo '*Error. Not enough table columns. Expecting ' . $names_length . ', found ' . $cellcount . PHP_EOL;

                return false;
                }
              }
            }
          }

        } // foreach ($tbody->childNodes AS $row)
      }
    }
  catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

  }

function dom_feesamount_table($url_content, $id, $sqlsvr_id, $thisFundNavDate, $fundcode, $sql_DateEntered)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '  In `dom_feesamount_table()`' . PHP_EOL;

  $rowcount = 0;

  /* Initialise 'names' array. */

  $names = array();
  $names[0] = array('neo_class', 0, 0, 50);
  $names[1] = array('neo_feetype', 0, 0, 50);
  $names[2] = array('neo_feesamount', 0, 0, 50);
  $names[3] = array('neo_feescumulative', 0, 0, 50);

  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    $tbody = $doc->getElementsByTagName('table')->item(0);

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {
      foreach ($tbody->childNodes AS $row)
        {
        $rowcount++;
        $cellcount = 0;

        //ignore the first row (headings)

        if ($rowcount > 1)
          {
          $holding = array();

          foreach ($row->childNodes AS $cell)
            {
            if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
              {
              $cellValue = $cell->nodeValue;

              if ($cellcount < $names_length)
                {
                // $holding[$names[$cellcount]] = trim($cellValue);

                $dataVal = ($names[$cellcount][2] ? '' : trim($cellValue));
                if (strlen($dataVal) > 0) $dataVal = substr($dataVal, 0, $names[$cellcount][3]);
                $holding[$names[$cellcount][0]] = $dataVal;
                }
              $cellcount++;
              }
            }

          // only save rows with a neo_feetype - ignore the totals row
          if (($cellcount > 2))
            {
            if ((strlen($holding['neo_feetype']) > 0))
              {
              if (NEOCAPTURE_DEBUG_ECHO) echo '    Saving : Class (' . $holding['neo_class'] . '), Type (' . $holding['neo_feetype'] . '), Amount (' . $holding['neo_feesamount'] . '), Cumulative (' . $holding['neo_feescumulative'] . ')' . PHP_EOL;

              if ($cellcount >= $names_length)
                {
                //$holding['captureID'] = $id;
                //add_holdings($holding);
                $holding['captureID'] = $sqlsvr_id;
                $holding['fundCode'] = $fundcode;
                $holding['NAVDate'] = $thisFundNavDate;
                add_feesamount_sqlsrv($holding, $names, $thisFundNavDate, $sql_DateEntered);
                }
              else
                {
                if (NEOCAPTURE_DEBUG_ECHO) echo '*Error. Not enough table columns. Expecting ' . $names_length . ', found ' . $cellcount . PHP_EOL;

                return false;
                }
              }
            }
          }

        } // foreach ($tbody->childNodes AS $row)
      }
    }
  catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

  }

function dom_result_table($url_content, $id, $sqlsvr_id, $thisFundNavDate, $fundcode, $sql_DateEntered)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '  In `dom_result_table()`' . PHP_EOL;

  $rowcount = 0;

  /* Initialise 'names' array. */
//$names[] = array('Database field name', flag : Should not carry forward if true, flag : set as '' string, max string length);

  $names = array();
  $names[0] = array('neo_blank', 1, 0, 50);
  $names[1] = array('neo_instrumenttype', 0, 0, 50);
  $names[2] = array('neo_marketvalue', 0, 0, 50);
  $names[3] = array('neo_currency', 0, 0, 5);
  $names[4] = array('neo_percentage', 0, 0, 50);

  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);


    $finder = new DomXPath($doc);
    $classname = "tableBorderCotes";
    $tbody = $finder->query("//table[@class='tableBorderCotes']")->item(0);

    //$tbody = $doc->getElementsByTagName('table')->item(0);

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {
      foreach ($tbody->childNodes AS $row)
        {
        $rowcount++;
        $cellcount = 0;

        //ignore the first row (headings)

        if ($rowcount > 1)
          {
          $holding = array();

          foreach ($row->childNodes AS $cell)
            {
            if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
              {
              $cellValue = $cell->nodeValue;

              if ($cellcount < $names_length)
                {
                // $holding[$names[$cellcount]] = trim($cellValue);

                $dataVal = ($names[$cellcount][2] ? '' : trim($cellValue));
                if (strlen($dataVal) > 0) $dataVal = substr($dataVal, 0, $names[$cellcount][3]);
                $holding[$names[$cellcount][0]] = $dataVal;
                }
              $cellcount++;
              }
            }

          // only save rows with an ISIN number - ignore the totals row
          if (($cellcount > 2))
            {
            if ((strlen($holding['neo_instrumenttype']) > 0))
              {
              if (NEOCAPTURE_DEBUG_ECHO) echo '    Saving : Instrument Type (' . $holding['neo_instrumenttype'] . '), Market Value (' . $holding['neo_marketvalue'] . '), Currency (' . $holding['neo_currency'] . '), Percentage (' . $holding['neo_percentage'] . ')' . PHP_EOL;

              if ($cellcount >= $names_length)
                {
                //$holding['captureID'] = $id;
                //add_holdings($holding);

                $holding['captureID'] = $sqlsvr_id;
                $holding['fundCode'] = $fundcode;
                $holding['NAVDate'] = $thisFundNavDate;
                add_result_sqlsrv($holding, $names, $thisFundNavDate, $sql_DateEntered);
                }
              else
                {
                if (NEOCAPTURE_DEBUG_ECHO) echo '*Error. Not enough table columns. Expecting ' . $names_length . ', found ' . $cellcount . PHP_EOL;

                return false;
                }
              }
            }
          }

        } // foreach ($tbody->childNodes AS $row)
      }
    }
  catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

  }

function dom_topdiv_table($url_content, $id, $sqlsvr_id, $fundcode, $sql_DateEntered)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '  In `dom_topdiv_table()`' . PHP_EOL;

  $rowcount = 0;

  /* Initialise 'names' array. */

  $names = array();
  $names[0] = array('neo_isin', 0, 0, 50);
  $names[1] = array('neo_funddescription', 0, 0, 50);
  $names[2] = array('neo_currency', 0, 0, 5);
  $names[3] = array('neo_navdate', 0, 0, 50);
  $names[4] = array('neo_status', 0, 0, 50);

  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    $holding = array();

    $finder = new DomXPath($doc);
    $tbody = $finder->query("//table")->item(0);

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {
      foreach ($tbody->childNodes AS $row)
        {
        if ($row->nodeName == "tr")
          {
          $rowcount++;
          if (NEOCAPTURE_DEBUG_ECHO) echo '    ' . $rowcount . PHP_EOL;

          if ($rowcount == 1)
            {
            $cellcount = 0;
            foreach ($row->childNodes AS $cell)
              {
              if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
                {
                $cellValue = $cell->nodeValue;

                if ($cellcount == 1)
                  {
                  $holding['neo_isin'] = trim($cellValue);
                  }

                if ($cellcount == 3)
                  {
                  $holding['neo_funddescription'] = trim($cellValue);
                  }

                $cellcount++;
                }
              }

            }

          if ($rowcount == 2)
            {
            $cellcount = 0;
            foreach ($row->childNodes AS $cell)
              {
              if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
                {
                $cellValue = $cell->nodeValue;

                if ($cellcount == 1)
                  {
                  $holding['neo_currency'] = trim($cellValue);
                  }

                if ($cellcount == 3)
                  {
                  $holding['neo_navdate'] = trim($cellValue);
                  }

                $cellcount++;
                }
              }

            }

          if ($rowcount == 3)
            {
            $cellcount = 0;
            foreach ($row->childNodes AS $cell)
              {
              if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
                {
                $cellValue = $cell->nodeValue;

                if ($cellcount == 1)
                  {
                  $holding['neo_status'] = trim($cellValue);
                  }

                $cellcount++;
                }
              }

            }

          if ($rowcount == 3)
            {
            if ((strlen($holding['neo_isin']) > 0))
              {
              //if (NEOCAPTURE_DEBUG_ECHO) echo '    ' . var_dump($holding) . PHP_EOL;

              //$holding['captureID'] = $id;
              //add_holdings($holding);
              $holding['captureID'] = $sqlsvr_id;
              $holding['fundcode'] = $fundcode;
              add_fundStatus_sqlsrv($holding, $names, $holding['neo_navdate'], $sql_DateEntered);

              }
            }
          }
        } // foreach ($tbody->childNodes AS $row)
      }
    }
  catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

  return $holding;

  }


function dom_positions_table($url_content, $id, $sqlsvr_id, $sql_DateEntered)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '  In `dom_positions_table()`' . PHP_EOL;

  $holding = array();
  $rowcount = 0;

  /* Collect Existing holdings. (Account and Isin combined as a unique identifier).
   * The idea is to find out if any holdings that at one time existed today no longer exist, and should be zeroed.*/

  $sql_ExistingHoldings = get_ExistingHoldings_sqlsvr($sqlsvr_id, $sql_DateEntered);
  $ExistingHoldings_length = count($sql_ExistingHoldings);

  /* Initialise 'names' array. */

  $names = array();
  $names[0] = 'neo_head';
  $names[1] = 'neo_a';
  $names[2] = 'neo_r';
  $names[3] = 'neo_ac';
  $names[4] = 'neo_isin';
  $names[5] = 'neo_security';
  $names[6] = 'neo_position';
  $names[7] = 'neo_quantity';
  $names[8] = 'neo_unit';
  $names[9] = 'neo_value';
  $names[10] = 'neo_currency';
  $names[11] = 'neo_depositary';
  $names[12] = 'neo_depositaryname';
  $names[13] = 'neo_bank';
  $names[14] = 'neo_securitiesaccount';
  $names[15] = 'neo_securitiesaccountdescription';

  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    $tbody = $doc->getElementsByTagName('tbody')->item(0);

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {
      foreach ($tbody->childNodes AS $row)
        {
        $rowcount++;
        $cellcount = 0;

        foreach ($row->childNodes AS $cell)
          {
          if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
            {
            $cellValue = $cell->nodeValue;

            if ($cellcount < $names_length)
              {
              $holding[$names[$cellcount]] = trim($cellValue);
              }
            $cellcount++;
            }
          }

        // Last row appear to be empty, stop entering a blank row.
        if (count($holding) > 2)
          {
          if (NEOCAPTURE_DEBUG_ECHO) echo '    Saving : ISIN (' . $holding['neo_isin'] . '), Qty (' . $holding['neo_quantity'] . '), Position (' . $holding['neo_position'] . ')' . PHP_EOL;

          if ($cellcount >= $names_length)
            {
            $holding['captureID'] = $id;
            add_holdings($holding);
            $holding['captureID'] = $sqlsvr_id;
            add_holdings_sqlsrv($holding, $sql_DateEntered);
            }
          else
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '*Error. Not enough table columns. Expecting ' . $names_length . ', found ' . $cellcount . PHP_EOL;

            return false;
            }

          if ($ExistingHoldings_length > 0)
            {
            /* Eliminate holdings that do exist from the  $sql_ExistingHoldings array.
* The idea is to find out if any holdings that at one time existed today no longer exist, and should be zeroed.*/

            $holdingVal = $holding['neo_securitiesaccount'] . '|' . $holding['neo_isin'] . '|' . $holding['neo_position'] . '|' . $holding['neo_security'];
            $keyVal = array_search($holdingVal, $sql_ExistingHoldings);

            if ($keyVal !== false)
              {
              unset($sql_ExistingHoldings[$keyVal]);
              }
            }

          } //  (count($holding) > 2)

        } // foreach ($tbody->childNodes AS $row)
      }
    }
  catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo '  Error : ' . $e->getMessage() . PHP_EOL;
    }

  /* Now Zero orphan positions. */

  $ExistingHoldings_length = count($sql_ExistingHoldings);

  if ($ExistingHoldings_length > 0)
    {
    foreach ($sql_ExistingHoldings as $orphanHolding)
      {
      // $holding['neo_securitiesaccount'] . '|' . $holding['neo_isin'] . '|' . $holding['neo_position'] . '|' . $holding['neo_security'] ;

      $expVal = explode('|', $orphanHolding, 4);
      $securitiesAccount = $expVal[0];
      $isinCode = $expVal[1];

      $holding['captureID'] = $sqlsvr_id;
      $holding['neo_head'] = '';
      $holding['neo_a'] = '';
      $holding['neo_r'] = '';
      $holding['neo_ac'] = '';
      $holding['neo_isin'] = $isinCode;
      $holding['neo_security'] = $expVal[3];
      $holding['neo_position'] = $expVal[2];
      $holding['neo_quantity'] = '0';
      $holding['neo_unit'] = 'UNIT';
      $holding['neo_value'] = '0';
      $holding['neo_currency'] = 'EUR';
      $holding['neo_depositary'] = '';
      $holding['neo_depositaryname'] = '';
      $holding['neo_bank'] = '';
      $holding['neo_securitiesaccount'] = $securitiesAccount;
      $holding['neo_securitiesaccountdescription'] = '';

      if (NEOCAPTURE_DEBUG_ECHO) echo '    Clearing : ISIN (' . $holding['neo_isin'] . '), Qty (' . $holding['neo_quantity'] . '), Position (' . $holding['neo_position'] . ')' . PHP_EOL;

      add_holdings_sqlsrv($holding, $sql_DateEntered);
      }
    }

  return true;
  }
  
  function dom_all_lines($url_content)
  {
  	
  
  	try
  	{
  		$doc = new DOMDocument();
  		@$doc->loadHTML($url_content);
  		
  		$finder = new DomXPath($doc);
  		$classname="buttonLines";
  		$nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
  	    
  		if ($nodes->length > 0) {
  			foreach( $nodes as $node) {
  				$content= $node->textContent;
  				if (NEOCAPTURE_DEBUG_ECHO) echo "BUTTONLINES : ". $content . PHP_EOL;
  				if (strpos($content,'All lines') !== false) {
    				return true;
				} else {
					return false;
				}
  			}
  		} else {
  			if (NEOCAPTURE_DEBUG_ECHO) echo "Could not find buttonLines Class" . PHP_EOL;
  		}
  	}
  	catch (Exception $e)
    {
    	return $e->getMessage();
    } 	
  
  	return false;
  }

function phpq_table($url_content, $id, $sqlsvr_id, $sql_DateEntered)
  {

  $hcount = 0;
  $headarray = array();
  $holding = array();
  $match = '';

  /* Collect Existing holdings. (Account and Isin combined as a unique identifier).
     The idea is to find out if any holdings that at one time existed today no longer exist, and should be zeroed.*/

  $sql_ExistingHoldings = get_ExistingHoldings_sqlsvr($sqlsvr_id, $sql_DateEntered);
  $ExistingHoldings_length = count($sql_ExistingHoldings);

  /* Initialise 'names' array. */

  $names = array();
  $names[0] = 'neo_head';
  $names[1] = 'neo_a';
  $names[2] = 'neo_r';
  $names[3] = 'neo_ac';
  $names[4] = 'neo_isin';
  $names[5] = 'neo_security';
  $names[6] = 'neo_position';
  $names[7] = 'neo_quantity';
  $names[8] = 'neo_unit';
  $names[9] = 'neo_value';
  $names[10] = 'neo_currency';
  $names[11] = 'neo_depositary';
  $names[12] = 'neo_depositaryname';
  $names[13] = 'neo_bank';
  $names[14] = 'neo_securitiesaccount';
  $names[15] = 'neo_securitiesaccountdescription';

  $names_length = count($names);

  try
    {

    $doc = phpQuery::newDocument($url_content);

    foreach ($doc->find('thead:first') as $cell)
      {
      $cell = pq($cell);
      $headarray[$hcount] = $cell->html();
      }

    $odd = true;
    if (strlen($match) > 0)
      {
      $findstring = 'tbody:first tr:contains(' . $match . ')';
      }
    else
      {
      $findstring = 'tbody:first tr';
      }
    foreach ($doc->find($findstring) as $row)
      {
      $row = pq($row);

      $cellcount = 0;

      foreach ($row->find('th,td') as $cell)
        {
        $cell = pq($cell);

        if ($cellcount < $names_length)
          {
          $holding[$names[$cellcount]] = trim($cell->html());
          }

        $cellcount++;
        }

      if (NEOCAPTURE_DEBUG_ECHO) echo '    Saving : ISIN (' . $holding['neo_isin'] . '), Qty (' . $holding['neo_quantity'] . '), Position (' . $holding['neo_position'] . ')' . PHP_EOL;

      $holding['captureID'] = $id;
      add_holdings($holding);
      $holding['captureID'] = $sqlsvr_id;
      add_holdings_sqlsrv($holding, $sql_DateEntered);

      if ($ExistingHoldings_length > 0)
        {
        /* Eliminate holdings that do exist from the  $sql_ExistingHoldings array.
           The idea is to find out if any holdings that at one time existed today no longer exist, and should be zeroed.*/

        $holdingVal = $holding['neo_securitiesaccount'] . '|' . $holding['neo_isin'] . '|' . $holding['neo_position'] . '|' . $holding['neo_security'];
        $keyVal = array_search($holdingVal, $sql_ExistingHoldings);

        if ($keyVal !== false)
          {
          unset($sql_ExistingHoldings[$keyVal]);
          }
        }

      }
    }
  catch (Exception $e)
    {
    return $e->getMessage();
    }

  /* Now Zero orphan positions. */

  $ExistingHoldings_length = count($sql_ExistingHoldings);

  if ($ExistingHoldings_length > 0)
    {
    foreach ($sql_ExistingHoldings as $orphanHolding)
      {
      // $holding['neo_securitiesaccount'] . '|' . $holding['neo_isin'] . '|' . $holding['neo_position'] . '|' . $holding['neo_security'] ;

      $expVal = explode('|', $orphanHolding, 4);
      $securitiesAccount = $expVal[0];
      $isinCode = $expVal[1];

      $holding['captureID'] = $sqlsvr_id;
      $holding['neo_head'] = '';
      $holding['neo_a'] = '';
      $holding['neo_r'] = '';
      $holding['neo_ac'] = '';
      $holding['neo_isin'] = $isinCode;
      $holding['neo_security'] = $expVal[3];
      $holding['neo_position'] = $expVal[2];
      $holding['neo_quantity'] = '0';
      $holding['neo_unit'] = 'UNIT';
      $holding['neo_value'] = '0';
      $holding['neo_currency'] = 'EUR';
      $holding['neo_depositary'] = '';
      $holding['neo_depositaryname'] = '';
      $holding['neo_bank'] = '';
      $holding['neo_securitiesaccount'] = $securitiesAccount;
      $holding['neo_securitiesaccountdescription'] = '';

      if (NEOCAPTURE_DEBUG_ECHO) echo '    Clearing : ISIN (' . $holding['neo_isin'] . '), Qty (' . $holding['neo_quantity'] . '), Position (' . $holding['neo_position'] . ')' . PHP_EOL;

      add_holdings_sqlsrv($holding, $sql_DateEntered);
      }
    }

  return true;
  }

function dom_confirmations_table($url_content, $id, $sqlsvr_id, $sql_DateEntered)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '  In `dom_confirmations_table()`' . PHP_EOL;

  $confirmation = array();
  $rowcount = 0;

  $names = array();
  $names[] = 'neo_head';
  $names[] = 'neo_bank';
  $names[] = 'neo_st';
  $names[] = 'neo_status';
  $names[] = 'neo_cat';
  $names[] = 'neo_t';
  $names[] = 'neo_settlement';
  $names[] = 'neo_clientreference';
  $names[] = 'neo_quantity';
  $names[] = 'neo_unit';
  $names[] = 'neo_isin';
  $names[] = 'neo_securitydescription';
  $names[] = 'neo_settlementamount';
  $names[] = 'neo_settlementcurrency';
  $names[] = 'neo_price';
  $names[] = 'neo_pricecurrency';
  $names[] = 'neo_blank1';
  $names[] = 'neo_trade';
  $names[] = 'neo_securitiesaccount';
  $names[] = 'neo_accountdescription';
  $names[] = 'neo_settagentbic';
  $names[] = 'neo_settagentdescr';
  $names[] = 'neo_ctrpbic';
  $names[] = 'neo_tradecounterpartdescr';
  $names[] = 'neo_bankreference';

  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    //$thead = $doc->getElementsByTagName('thead')->item(0);
    $tbody = $doc->getElementsByTagName('tbody')->item(0);

    // extract headings

    //extract body of table
    $odd = true;

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {
      foreach ($tbody->childNodes AS $row)
        {
        $rowcount++;

        //echo $row->nodeName . " = " . $row->nodeValue . "<br />";

        $cellcount = 0;

        foreach ($row->childNodes AS $cell)
          {
          if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
            {
            $cellValue = $cell->nodeValue;

            //echo $cell->nodeName . " = " . $cell->nodeValue . "<br />";

            if ($cellcount < $names_length)
              {
              $confirmation[$names[$cellcount]] = trim($cellValue);
              }
            $cellcount++;
            }
          }

//      if ($rowcount>155) // Debug purposes only, somewhere to put a breakpoint !
//        {
//          $rowcount = $rowcount;
//        }

        // Last row appear to be empty, stop entering a blank row.
        if (count($confirmation) > 2)
          {
          if ($cellcount >= $names_length)
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '    Saving : BR (' . $confirmation['neo_bankreference'] . '), ISIN (' . $confirmation['neo_isin'] . '), Qty (' . $confirmation['neo_quantity'] . '), Trade (' . $confirmation['neo_trade'] . '), Sett (' . $confirmation['neo_settlement'] . ')' . PHP_EOL;

            $confirmation['captureID'] = $id;
            add_confirmations($confirmation);
            $confirmation['captureID'] = $sqlsvr_id;
            add_confirmations_sqlsrv($confirmation, $sql_DateEntered);

            }
          else
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '*Error. Not enough table columns. Expecting ' . $names_length . ', found ' . $cellcount . PHP_EOL;

            return false;
            }
          }

        $confirmation = array();
        }
      }
    }
  catch (Exception $e)
    {
    return $e->getMessage();
    }
  return true; //
  }

function dom_ucits_orders_table($url_content, $id, $sqlsvr_id, $sql_DateEntered)
  {
  if (NEOCAPTURE_DEBUG_ECHO) echo '  In `dom_ucits_orders_table()`' . "<BR>";

  $ucits_orders = array();
  $rowcount = 0;
  $columnCount = 0;

  /*
  <th width="1%"> </th>
  <th> Bank code </th>
  <th> Status </th>
  <th> Type </th>
  <th> Client Reference </th>
  <th> Quantity </th>
  <th> ISIN </th>
  <th> Security description </th>
  <th> Amount (pay. ccy) </th>
  <th> </th>
  <th> Securities account </th>
  <th> Securities account description </th>
  <th> Last modification date </th>
  */

  $names = array();
  $names[] = 'neo_blank';
  $names[] = 'neo_bank';
  $names[] = 'neo_st';
  $names[] = 'neo_t';
  $names[] = 'neo_clientreference';
  $names[] = 'neo_quantity';
  $names[] = 'neo_isin';
  $names[] = 'neo_securitydescription';
  $names[] = 'neo_settlementamount';
  $names[] = 'neo_settlementcurrency';
  $names[] = 'neo_securitiesaccount';
  $names[] = 'neo_accountdescription';
  $names[] = 'neo_trade';
  $names[] = 'neo_bankreference';

  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    //$thead = $doc->getElementsByTagName('thead')->item(0);
    $tbody = $doc->getElementsByTagName('tbody')->item(0);

    // extract headings

    //extract body of table
    $odd = true;

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {
      foreach ($tbody->childNodes AS $row)
        {
        $columnCount = 0;

        // Default values, not all the required columns exist on this (Lux UCITS) page.

        $ucits_orders['neo_bank'] = '';
        $ucits_orders['neo_st'] = '';
        $ucits_orders['neo_status'] = '';
        $ucits_orders['neo_cat'] = '';
        $ucits_orders['neo_t'] = '';
        $ucits_orders['neo_settlement'] = '';
        $ucits_orders['neo_clientreference'] = '';
        $ucits_orders['neo_bankreference'] = '';
        $ucits_orders['neo_quantity'] = '';
        $ucits_orders['neo_unit'] = '';
        $ucits_orders['neo_isin'] = '';
        $ucits_orders['neo_securitydescription'] = '';
        $ucits_orders['neo_settlementamount'] = '';
        $ucits_orders['neo_settlementcurrency'] = '';
        $ucits_orders['neo_price'] = '';
        $ucits_orders['neo_pricecurrency'] = '';
        $ucits_orders['neo_trade'] = '';
        $ucits_orders['neo_securitiesaccount'] = '';
        $ucits_orders['neo_accountdescription'] = '';
        $ucits_orders['neo_settagentbic'] = '';
        $ucits_orders['neo_settagentdescr'] = '';
        $ucits_orders['neo_ctrpbic'] = '';
        $ucits_orders['neo_tradecounterpartdescr'] = '';

        $rowcount++;

        //echo $row->nodeName . " = " . $row->nodeValue . "<br />";

        $cellcount = 0;

        foreach ($row->childNodes AS $cell)
          {
          if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
            {
            $cellValue = $cell->nodeValue;

            //echo $cell->nodeName . " = " . $cell->nodeValue . "<br />";

            if ($cellcount < $names_length)
              {
              $ucits_orders[$names[$cellcount]] = trim($cellValue);
              $columnCount += 1;
              }
            $cellcount++;
            }
          }

        // Last row appear to be empty, stop entering a blank row.
        if ($columnCount > 2)
          {
          if ($cellcount >= $names_length)
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '    Saving : CR (' . $ucits_orders['neo_client_reference'] . '), ISIN (' . $ucits_orders['neo_isin'] . ')' . "<br>";

            $ucits_orders['neo_head'] = 'LUX_UCITS'; /* mark this as a Luxembourg Ucits confirm, (thus has lower priority) */
            $ucits_orders['captureID'] = $sqlsvr_id;
            add_ucits_orders_sqlsrv($ucits_orders, $sql_DateEntered);

            }
          else
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '*Error. Not enough table columns. Expecting ' . $names_length . ', found ' . $cellcount . "<br>";

            return false;
            }
          }

        $ucits_orders = array();
        }
      }
    }
  catch (Exception $e)
    {
    return $e->getMessage();
    }
  return true; //

  }


function dom_remove_elements($url_content)
  {

  try
    {
    $doc = new DOMDocument();
    $doc->loadHTML($url_content);

    // get all the script tags
    $script_tags = $doc->getElementsByTagName('script');
    $length = $script_tags->length;

    // for each tag, remove it from the DOM
    for ($i = 0; $i < $length; $i++)
      {
      $script_tags->item(0)->parentNode->removeChild($script_tags->item(0));
      }
    }
  catch (Exception $e)
    {
    return 'ERROR remove_content';
    }
  return $doc->saveHTML();
  }

function dom_remove_named_elements($doc, $elementname)
  {

  try
    {
    // get all the script tags
    $elements = $doc->getElementsByTagName($elementname);
    $length = $elements->length;

    // for each tag, remove it from the DOM
    for ($i = 0; $i < $length; $i++)
      {
      $elements->item(0)->parentNode->removeChild($elements->item(0));
      }
    }
  catch (Exception $e)
    {
    }
  return $doc;
  }

function dom_remove_named_element_attributes($doc, $elementname, $attributes)
  {
  // $attributes : Array or comma delimited list of attributes to delete.

  if (!is_array($attributes))
    {
    $attributes = explode(',', $attributes);
    }

  try
    {
    // get all the script tags
    $elements = $doc->getElementsByTagName($elementname);
    $length = $elements->length;

    // for each tag, remove it from the DOM
    for ($i = 0; $i < $length; $i++)
      {
      $thiselement = $elements->item($i);

      if (is_array($attributes))
        {
        foreach ($attributes as $thisatt)
          {
          $thiselement->removeAttribute($thisatt);
          }
        }
      else
        {
        $thiselement->removeAttribute($attributes);
        }

      }
    }
  catch (Exception $e)
    {
    }
  return $doc;
  }

function dom_remove_elements_attributes_links($url_content)
  {

  try
    {
    //
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    dom_remove_named_elements($doc, 'script');
    dom_remove_named_elements($doc, 'img');
    dom_remove_named_elements($doc, 'input');

    dom_remove_named_element_attributes($doc, 'a', array('href', 'onclick', 'id'));
    dom_remove_named_element_attributes($doc, 'th', array('onclick', 'align', 'class', 'id'));
    dom_remove_named_element_attributes($doc, 'tr', array('onclick', 'class', 'id'));
    dom_remove_named_element_attributes($doc, 'td', 'id');
    dom_remove_named_element_attributes($doc, 'table', 'id');

    // get all the script tags
    $elements = $doc->getElementsByTagName('a');
    $length = $elements->length;

    $i = $length - 1;
    while ($i > -1)
      {
      $element = $elements->item($i);
      $ignore = false;

      $debug = $element->textContent;

      $newelement = $doc->createTextNode($element->textContent);
      $element->parentNode->replaceChild($newelement, $element);
      $i--;
      }

    }
  catch (Exception $e)
    {
    return 'ERROR remove_content';
    }

  return $doc->saveHTML();
  }

function phpq_remove_elements_attributes_links($url_content)
  {

  try
    {
    // phpq_remove_elements()

    $doc = phpQuery::newDocument($url_content);

    $doc->find('script')->remove();
    $doc->find('img')->remove();
    $doc->find('input')->remove();

    // phpq_remove_attributes()
    $object_set = $doc->find('a');
    $object_set->removeAttr('href');
    $object_set->removeAttr('onclick');
    $object_set->removeAttr('id');

    $object_set = $doc->find('th');
    $object_set->removeAttr('onclick');
    $object_set->removeAttr('align');
    $object_set->removeAttr('class');
    $object_set->removeAttr('id');

    $object_set = $doc->find('tr');
    $object_set->removeAttr('onclick');
    $object_set->removeAttr('class');
    $object_set->removeAttr('id');

    $object_set = $doc->find('td');
    $object_set->removeAttr('id');

    $object_set = $doc->find('table');
    $object_set->removeAttr('id');

    //$doc->find('a')->removeAttr('href');
    //$doc->find('a')->removeAttr('onclick');
    //$doc->find('th')->removeAttr('onclick');
    //$doc->find('th')->removeAttr('align');
    //$doc->find('th')->removeAttr('class');
    //$doc->find('th')->removeAttr('id');
    //$doc->find('tr')->removeAttr('onclick');
    //$doc->find('tr')->removeAttr('class');
    //$doc->find('tr')->removeAttr('id');

    // phpq_remove_links()

    $object_set = $doc->find('a');
    foreach ($object_set as $link)
      {
      // make $link a phpQuery object, otherwise its just a DOMElement object
      $link = pq($link);
      // fetch the innerHTML of the link, and replace the link with its innerHTML
      $link->replaceWith($link->html());
      }

    }
  catch (Exception $e)
    {
    return 'ERROR remove_content';
    }
  return (string)$doc;

  }

function phpq_remove_elements($url_content)
  {
  try
    {
    $doc = phpQuery::newDocument($url_content);
    $doc->find('script')->remove();
    $doc->find('img')->remove();
    $doc->find('input')->remove();
    }
  catch (Exception $e)
    {
    return 'ERROR remove_content';
    }
  return $doc;
  }

function phpq_remove_attributes($url_content)
  {
  try
    {
    $doc = phpQuery::newDocument($url_content);

    $doc->find('a')->removeAttr('href');
    $doc->find('a')->removeAttr('onclick');
    $doc->find('th')->removeAttr('onclick');
    $doc->find('th')->removeAttr('align');
    $doc->find('th')->removeAttr('class');
    $doc->find('tr')->removeAttr('onclick');
    $doc->find('tr')->removeAttr('class');

    //$doc->find('td')->removeAttr('id');

    }
  catch (Exception $e)
    {
    return 'ERROR remove_content';
    }
  return $doc;
  }

function phpq_remove_links($url_content)
  {
  try
    {
    $doc = phpQuery::newDocument($url_content);

    foreach ($doc->find('a') as $link)
      {
      // make $link a phpQuery object, otherwise its just a DOMElement object
      $link = pq($link);
      // fetch the innerHTML of the link, and replace the link with its innerHTML
      $link->replaceWith($link->html());
      }

    }
  catch (Exception $e)
    {
    return 'ERROR remove_links';
    }
  return $doc;
  }

function phpq_remove_id($url_content)
  {
  try
    {
    $doc = phpQuery::newDocument($url_content);

    foreach ($doc->find('tr') as $row)
      {
      // make $link a phpQuery object, otherwise its just a DOMElement object
      $row = pq($row);
      foreach ($row->find('td') as $cell)
        {
        $cell = pq($cell);
        $cell->removeAttr('id');

        }

      }

    }
  catch (Exception $e)
    {
    return 'ERROR remove_links';
    }
  return $doc;
  }

function phqp_exists($url_content, $selector)
  {
  $doc = phpQuery::newDocument($url_content)->find($selector);
  $found = false;
  if (strlen($doc) > 0)
    {
    $found = true;
    }
  return $found;
  }

function phqp_getValue($url_content, $selector)
  {
  $doc = phpQuery::newDocument($url_content)->find($selector)->val();
  return $doc;
  }

?>

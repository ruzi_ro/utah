<?php

function getLoginScreen()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;

	//get the login in screen (this also initiates a cookie so needs to be done before the post)
	$url_content = bnp_getLoginScreen($ckfile);
	$pos = strpos($url_content, "getLoginScreen");
	if ($pos === false)
	{
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "login screen";
		}
		else
		{
			return "failed";
		}
	}
	else
	{
		return "failed";
	}
}

function postLoginScreen()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;

	$post['accessCode'] = NEOLINK_BNP_USERNAME;
	$post['accessPass'] = NEOLINK_BNP_PASSWORD;
	$post['appId'] = '-1';
	$post['appURL'] = '';
	$post['authLevel'] = '1';
	$post['locale'] = 'en';

	$post_params_1 = "";
	foreach ($post as $key => $value)
	{
		$post_params_1 .= $key . '=' . $value . '&';
	}
	$post_params_1 .= "org.apache.struts.taglib.html.TOKEN=" . $token;

	$url_content = bnp_postLoginScreen($ckfile, $post_params_1);
	$pos = strpos($url_content, "postLoginScreen");
	if ($pos === false)
	{
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "login screen";
		}
		if (1)
		{
			return "portal page";
		}
		if (strpos($url_content, "https://securities-client.bnpparibas.com/web/home.do?lang=en")>0)
		{
			return "home post";
		}
		 
		if (strpos($url_content, "https://securities-client.bnpparibas.com/web/portal?lang=en") === false)
		{
			return "failed";
		} else
		{
			/*return "menu page";*/
			return "loginContinuation";
		}
	} else
	{

	}
}

function loginScreenContinue()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;

	$post_params_1 = 'lang=en';

	$url_content = bnp_postGeneric($ckfile, 'https://securities-client.bnpparibas.com/web/portal', $post_params_1);

	$pos = strpos($url_content, "postGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "login screen";
		}
		if (phqp_exists($url_content, '#QuickAccess-1016'))
		{
			return "portal page";
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		} else
		{
			return "menu page";
		}
	} else
	{

	}
}

function homePost()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;

	$post_params_1 = 'lang=en';

	$url_content = bnp_postGeneric($ckfile, 'https://securities-client.bnpparibas.com/web/home.do', $post_params_1);

	$pos = strpos($url_content, "postGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "login screen";
		}
		if (strpos($url_content, 'qAccessMenu')>0)
		{
			return "portal page";
		} else
		{
			return "menu page";
		}
	} else
	{

	}
}

function postRocheBrune()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;

	$post_params_1 = 'lang=en&subscriptionCode=A3442';

	$url_content = bnp_postGeneric($ckfile, 'https://securities-client.bnpparibas.com/web/home.do', $post_params_1);

	$pos = strpos($url_content, "postGeneric");
	if ($pos === false)
	{
			
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "login screen";
		}
		if (strpos($url_content, 'A3442')>0)
		{
			return "rochebrune";
			
			/*
			$doc = new DOMDocument();
			@$doc->loadHTML($url_content);
			$finder = new DomXPath($doc);
			$infodiv = $finder->query('//*[@id="ServerInfo-1013"]');
			foreach ($infodiv as $info){
				$infotext=$info->textContent;
			}
			
			if (strpos($infotext,'A3442')>0){
				return "rochebrune";
			}
			*/
			
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		} else
		{
			return "menu page";
		}
	} else
	{
		return "failed";
	}
}

function postTimeoutLoginScreen()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;

	$post['accessCode'] = NEOLINK_BNP_USERNAME;
	$post['accessPass'] = NEOLINK_BNP_PASSWORD;
	$post['appId'] = '38';
	$post['appURL'] = 'https://securities-link.bnpparibas.com/web/ConnectActionEx.do?bdujpo]4E]3GMpbeGjmufsMjtu/ep]4Gqbhfobnf]4EDBS%60QBHF%60MJTUF%60TFDVSJUJFT]37nfov]4Eusvf]37bqqmz]4Eusvf&bcu]4EB3192&lang=en';
	$post['authLevel'] = '1';
	$post['locale'] = 'en';

	$post_params_1 = "";
	foreach ($post as $key => $value)
	{
		$post_params_1 .= $key . '=' . $value . '&';
	}
	$post_params_1 .= "org.apache.struts.taglib.html.TOKEN=" . $token;


	$url_content = bnp_postLoginScreen($ckfile, $post_params_1);
	$pos = strpos($url_content, "postLoginScreen");
	if ($pos === false)
	{
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "login screen";
		}
		if (strpos($url_content, "QuickAccess") === false)
		{
			return "portal page";
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		}
		else
		{
			return "menu page";
		}
	}
	else
	{

	}
}


function getAdministrationByFund()
{

	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;

	//$geturl = "https://securities-client.bnpparibas.com/web/portal/neolink/fa/consultation/byfund.psml";
	date_default_timezone_set("UTC");
	$time=time()*1000;
	$geturl ="https://securities-client.bnpparibas.com/web/doRedirect.do?menuId=1&menuItemId=95";
	//$geturl="https://securities-client.bnpparibas.com/web/doRedirect.do?menuId=1&menuItemId=121";
        echo "EEEEEEEEEEEEEEEE" . PHP_EOL;
	$url_content = bnp_getGeneric($ckfile, $geturl);
        echo $url_content . PHP_EOL;
	$pos = strpos($url_content, "getGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_ADF_PAGE_LISTE_NAV'))
		{
			return "view by fund";
		}
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "timeout login screen";
		}
		if (phqp_exists($url_content, '#quickAccessZone'))
		{
			return "portal page";
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		}
		else
		{
			return "waiting page";
		}
	}
	else
	{
		return "failed";
	}
}

function getAdministrationByFundFollow()
{

	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;

	//https://securities-link.bnpparibas.com/web/FWS_OnGetStringToMenu.do?_dc=1384852684487
	
	$timenow=time()*1000;
	
	//$geturl="https://securities-link.bnpparibas.com/web/FWS_OnGetStringToMenu.do?_dc=".$timenow;
	
	$geturl = "https://securities-link.bnpparibas.com/web/ConnectActionEx.do?bdujpo]4E]3Gnfov.fwfou/ep]4GOBWJHBUPS%60WBSOBNF]4EJTfttjpoDpotubout/OBWJHBUJPO%60CFBO]37JUFNDMJDLFE%60WBSOBNF]4E32]37NFOVDMJDLFE%60WBSOBNF]4EBEG.gbnpNfov]37sboe]4E2291796838778&bcu]4EB3192&lang=en";

	$url_content = bnp_getGeneric($ckfile, $geturl);
	$pos = strpos($url_content, "getGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_ADF_PAGE_LISTE_NAV'))
		{
			return "view by fund";
		}
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "timeout login screen";
		}
		if (phqp_exists($url_content, '#quickAccessZone'))
		{
			return "portal page";
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		}
		else
		{
			return "menu page";
		}
	}
	else
	{
		return "failed";
	}
}

function getDashboard($index, $fundcode)
{

	global $ckfile, $url_content;

	$geturl = "https://securities-link.bnpparibas.com/web/navigation.do?action=ADF_PAGE_DETAIL_FUND_CONSULT&arrayListName=ADF_NAV_VALIDATION_BEAN&index=";
	$geturl .= $index;

	$url_content = bnp_getGeneric($ckfile, $geturl);
	$pos = strpos($url_content, "getGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#valoDiv'))
		{
			return "dashboard";
		}
		else
		{
			return "failed";
		}
	}
	else
	{
		return "failed";
	}
}

function postInventoriesTab($navdate, $fundcode)
{
	global $ckfile, $url_content, $token;
	$posturl = "https://securities-link.bnpparibas.com/web/tabSheetChange.do?tabsheet_id=TAB_FUND&active_tabsheet=ADF_PAGE_LISTE_INVENTORY_TABSHEET_LOAD&active_form=0&page_name=ADF_PAGE_DETAIL_FUND&next_action=ADF_PAGE_LISTE_INVENTORY_TABSHEET_LOAD";

	$postparams = "&pagename=ADF_PAGE_DETAIL_FUND&navDateString=";
	$postparams .= urlencode($navdate);
	$postparams .= "&potfIden=67521&fundCode=";
	$postparams .= $fundcode;
	$postparams .= "&navDate=";
	$postparams .= urlencode($navdate);
	$postparams .= "&";

	$url_content = bnp_postGeneric($ckfile, $posturl, $postparams);
	$pos = strpos($url_content, "postGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_ADF_PAGE_LISTE_INVENTORY_TABSHEET'))
		{
			return "inventory tab sheet";
		}
		else
		{
			return "failed";
		}

	}
	else
	{
		return "failed";
	}
}

function postInventoriesTabAll($fundcode)
{
	global $ckfile, $url_content, $token;
	$posturl = "https://securities-link.bnpparibas.com/web/FilterBuilder2.do?C_MODE=LAST";
	$postparams = "&aazones=FundTabsheet%2Cerrorzone&&pagename=ADF_PAGE_LISTE_INVENTORY_TABSHEET&detailAction=&detailBean=ADF_PAGE_LISTE_INVENTORY_TABSHEET_BEAN&begin=0&libelleFilter=&codeFilter=360049&rupture1=&rupture2=&fundCodes_selectedValues=%2F~%2F";
	$postparams .= $fundcode;
	$postparams .= "&page=ADF_PAGE_LISTE_INVENTORY_TABSHEET&htxtMode=all&SHOW_ALL=&fldParam_ADF_PAGE_LISTE_INVENTORY_TABSHEET=&indexSort_ADF_PAGE_LISTE_INVENTORY_TABSHEET=&sort_ADF_PAGE_LISTE_INVENTORY_TABSHEET=&TABLE_ID=ADF_PAGE_LISTE_INVENTORY_TABSHEET&";

	$url_content = bnp_postGeneric($ckfile, $posturl, $postparams);
	$pos = strpos($url_content, "postGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_ADF_PAGE_LISTE_INVENTORY_TABSHEET'))
		{
			return "inventory tab sheet all";
		}
		else
		{
			return "failed";
		}

	}
	else
	{
		return "failed";
	}
}

function postAvailabilitiesTab($navdate, $fundcode)
{
	global $ckfile, $url_content, $token;

	$posturl = "https://securities-link.bnpparibas.com/web/tabSheetChange.do?tabsheet_id=TAB_FUND&active_tabsheet=ADF_PAGE_LISTE_AVAILABILITY_TABSHEET_LOAD&active_form=0&page_name=ADF_PAGE_DETAIL_FUND&next_action=ADF_PAGE_LISTE_AVAILABILITY_TABSHEET_LOAD";
	//$posturl = "https://securities-link.bnpparibas.com/web/tabSheetChange.do?tabsheet_id=TAB_FUND&active_tabsheet=ADF_PAGE_LISTE_INVENTORY_TABSHEET_LOAD&active_form=0&page_name=ADF_PAGE_DETAIL_FUND&next_action=ADF_PAGE_LISTE_INVENTORY_TABSHEET_LOAD";

	$postparams = "&pagename=ADF_PAGE_DETAIL_FUND&navDateString=";
	$postparams .= urlencode($navdate);
	$postparams .= "&potfIden=79221&fundCode=";
	$postparams .= $fundcode;
	$postparams .= "&navDate=";
	$postparams .= urlencode($navdate);
	$postparams .= "&";

	$url_content = bnp_postGeneric($ckfile, $posturl, $postparams);
	$pos = strpos($url_content, "postGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_ADF_PAGE_LISTE_AVAILABILITY_TABSHEET'))
		{
			// check to see if 'all lines' button is present.  If so, press it
			if (dom_all_lines($url_content)===true){
				//if (NEOCAPTURE_DEBUG_ECHO) echo "Found ALL LINES so pressing the button" . PHP_EOL;

				$posturl="https://securities-link.bnpparibas.com/web/FilterBuilder2.do?C_MODE=LAST";
				 
				$postparams="&aazones=FundTabsheet%2Cerrorzone&&aaxmlrequest=true&pagename=ADF_PAGE_LISTE_AVAILABILITY_TABSHEET&detailAction=&detailBean=ADF_PAGE_LISTE_AVAILABILITY_TABSHEET_BEAN&begin=0&page=ADF_PAGE_LISTE_AVAILABILITY_TABSHEET&htxtMode=all&SHOW_ALL=&fldParam_ADF_PAGE_LISTE_AVAILABILITY_TABSHEET=&indexSort_ADF_PAGE_LISTE_AVAILABILITY_TABSHEET=&sort_ADF_PAGE_LISTE_AVAILABILITY_TABSHEET=&TABLE_ID=ADF_PAGE_LISTE_AVAILABILITY_TABSHEET&";
				 
				$url_content = bnp_postGeneric($ckfile, $posturl, $postparams);

				return "retry";

			}
			else
			{
				// did not find "all lines"
				// if (NEOCAPTURE_DEBUG_ECHO) echo 'Did not find ALL LINES' . PHP_EOL;
				return "availabilities tab sheet";
			}
		}
		else
		{
			// failed on first attempt
			return "failed";
		}
	}
	else
	{
		return "failed";
	}
}

function postMovementTab($navdate, $fundcode)
{
	global $ckfile, $url_content, $token;
	$posturl = "https://securities-link.bnpparibas.com/web/tabSheetChange.do?tabsheet_id=TAB_FUND&active_tabsheet=ADF_PAGE_LISTE_MOVEMENT_TABSHEET_LOAD&active_form=0&page_name=ADF_PAGE_DETAIL_FUND&next_action=ADF_PAGE_LISTE_MOVEMENT_TABSHEET_LOAD";

	$postparams = "&aazones=errorzone&&pagename=ADF_PAGE_DETAIL_FUND&navDateString=";
	$postparams .= urlencode($navdate);
	$postparams .= "&potfIden=67521&fundCode=";
	$postparams .= $fundcode;
	$postparams .= "&navDate=";
	$postparams .= urlencode($navdate);
	$postparams .= "&";

	$url_content = bnp_postGeneric($ckfile, $posturl, $postparams);
	$pos = strpos($url_content, "postGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_ADF_PAGE_LISTE_MOVEMENT_TABSHEET'))
		{
			return "movement tab sheet";
		}
		else
		{
			return "failed";
		}

	}
	else
	{
		return "failed";
	}
}

function postOrderTab($navdate, $fundcode)
{
	global $ckfile, $url_content, $token;
	$posturl = "https://securities-link.bnpparibas.com/web/tabSheetChange.do?tabsheet_id=TAB_FUND&active_tabsheet=ADF_PAGE_LISTE_ORDER_TABSHEET_LOAD&active_form=0&page_name=ADF_PAGE_DETAIL_FUND&next_action=ADF_PAGE_LISTE_ORDER_TABSHEET_LOAD";

	$postparams = "&aazones=errorzone&&pagename=ADF_PAGE_DETAIL_FUND&navDateString=";
	$postparams .= urlencode($navdate);
	$postparams .= "&potfIden=67521&fundCode=";
	$postparams .= $fundcode;
	$postparams .= "&navDate=";
	$postparams .= urlencode($navdate);
	$postparams .= "&";

	$url_content = bnp_postGeneric($ckfile, $posturl, $postparams);
	$pos = strpos($url_content, "postGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_ADF_PAGE_LISTE_ORDER_TABSHEET'))
		{
			return "order tab sheet";
		}
		else
		{
			return "failed";
		}

	}
	else
	{
		return "failed";
	}
}

function getCustodyHoldings()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;

	$url_content = bnp_getCustodyHoldings($ckfile);
	$pos = strpos($url_content, "getCustodyHoldings");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_PTF_PAGE_LISTE_POSITIONS'))
		{
			return "paged custody holdings page";
		}
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			//org.apache.struts.taglib.html.TOKEN
			return "timeout login screen";
		}
		if (phqp_exists($url_content, '#quickAccessZone'))
		{
			return "portal page";
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		}
		else
		{
			return "menu page";
		}
	}
	else
	{
		return "failed";
	}
}

function postCustodyHoldingsAll()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;

	$url_content = bnp_postCustodyHoldingsAll($ckfile, $post_params_2);
	$pos = strpos($url_content, "postCustodyHoldingsAll");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_PTF_PAGE_LISTE_POSITIONS'))
		{
			return "success";
		}
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "login screen";
		}
		if (phqp_exists($url_content, '#quickAccessZone'))
		{
			return "portal page";
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		}
		else
		{
			return "menu page";
		}
	}
	else
	{
		return "failed";
	}
}

function getConfirmations()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;

	$url_content = bnp_getConfirmations($ckfile);
	$pos = strpos($url_content, "getConfirmations");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_CAR_PAGE_LISTE_SECURITIES'))
		{
			return "paged confirmations page";
		}
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "timeout login screen";
		}
		if (phqp_exists($url_content, '#quickAccessZone'))
		{
			return "portal page";
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		}
		else
		{
			return "menu page";
		}
	}
	else
	{
		return "failed";
	}
}

function postConfirmationsAll()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;

	$url_content = bnp_postConfirmationsAll($ckfile, $post_params_2);
	$pos = strpos($url_content, "postConfirmationsAll");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_CAR_PAGE_LISTE_SECURITIES'))
		{
			return "success";
		}
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "login screen";
		}
		if (phqp_exists($url_content, '#quickAccessZone'))
		{
			return "portal page";
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		}
		else
		{
			return "menu page";
		}
	}
	else
	{
		return "failed";
	}
}

function postAddCriteriaBank()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;
	global $params_add_criteria_bank;
	global $post_add_criteria_bank;

	$url_content = bnp_postGeneric($ckfile, $post_add_criteria_bank, $params_add_criteria_bank);
	$pos = strpos($url_content, "postGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_CAR_PAGE_LISTE_SECURITIES'))
		{
			return "bank criteria added";
		}
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "login screen";
		}
		if (phqp_exists($url_content, '#quickAccessZone'))
		{
			return "portal page";
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		}
		else
		{
			return "menu page";
		}
	}
	else
	{
		return "failed";
	}
}

function postAddCriteriaTrade()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;
	global $params_add_criteria_trade;
	global $post_add_criteria_trade;

	$url_content = bnp_postGeneric($ckfile, $post_add_criteria_trade, $params_add_criteria_trade);
	$pos = strpos($url_content, "postGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_CAR_PAGE_LISTE_SECURITIES'))
		{
			return "trade criteria added";
		}
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "login screen";
		}
		if (phqp_exists($url_content, '#quickAccessZone'))
		{
			return "portal page";
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		}
		else
		{
			return "menu page";
		}
	}
	else
	{
		return "failed";
	}
}

function postApplyFilter()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;
	global $params_apply_filter;
	global $post_apply_filter;

	$url_content = bnp_postGeneric($ckfile, $post_apply_filter, $params_apply_filter);
	$pos = strpos($url_content, "postGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_CAR_PAGE_LISTE_SECURITIES'))
		{
			return "filtered results";
		}
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "login screen";
		}
		if (phqp_exists($url_content, '#quickAccessZone'))
		{
			return "portal page";
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		}
		else
		{
			return "menu page";
		}
	}
	else
	{
		return "failed";
	}
}

function postAllFilter()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;
	global $params_filter_builder;
	global $post_filter_builder;

	$url_content = bnp_postGeneric($ckfile, $post_filter_builder, $params_filter_builder);
	$pos = strpos($url_content, "postGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_CAR_PAGE_LISTE_SECURITIES'))
		{
			return "success";
		}
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "login screen";
		}
		if (phqp_exists($url_content, '#quickAccessZone'))
		{
			return "portal page";
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		}
		else
		{
			return "menu page";
		}
	}
	else
	{
		return "failed";
	}
}

function getPortalPage()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;

	$url_content = bnp_getPortalPage($ckfile);
	$pos = strpos($url_content, "getPortalPage");
	if ($pos === false)
	{
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "login screen";
		}
		if (phqp_exists($url_content, '#quickAccessZone'))
		{
			return "portal page";
		} else
		{
			return "menu page";
		}
	} else
	{
		return "failed";
	}
}

function getUcitsOrders()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token, $get_ucits_orders;

	$url_content = bnp_getGeneric($ckfile,$get_ucits_orders);
	$pos = strpos($url_content, "getGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_FDS_PAGE_LISTE_ORDER'))
		{
			return "paged orders page";
		}
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "timeout login screen";
		}
		if (phqp_exists($url_content, '#quickAccessZone'))
		{
			return "portal page";
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		}
		else
		{
			return "menu page";
		}
	}
	else
	{
		return "failed";
	}
}

function postAddCriteriaDate()
{
	global $ckfile, $post, $post_params_1, $post_params_2, $url_content, $token;
	global $params_add_criteria_date;
	global $post_add_criteria_date;

	$url_content = bnp_postGeneric($ckfile, $post_add_criteria_date, $params_add_criteria_date);
	$pos = strpos($url_content, "postGeneric");
	if ($pos === false)
	{
		if (phqp_exists($url_content, '#ContentTable_FDS_PAGE_LISTE_ORDER'))
		{
			return "date criteria added";
		}
		if (phqp_exists($url_content, 'input[name*="accessPass"]'))
		{
			//extract the hidden token field
			$token = phqp_getValue($url_content, 'input[name*="org.apache.struts.taglib.html.TOKEN"]');
			return "login screen";
		}
		if (phqp_exists($url_content, '#quickAccessZone'))
		{
			return "portal page";
		}
		if (strpos($url_content, "PENNINGTON") === false)
		{
			return "failed";
		}
		else
		{
			return "menu page";
		}
	}
	else
	{
		return "failed";
	}
}


?>

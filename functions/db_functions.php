<?php

require_once(LOGIN_PASSWORD_FILE);

function db_connect()
  {
  $result = new mysqli(MYSQL_NEO_HOSTNAME, MYSQL_NEO_USER, MYSQL_NEO_PASSWORD, MYSQL_NEO_DATABASE);
  if (!$result)
    {
    throw new Exception('Could not connect to database server');
    }
  else
    {
    return $result;
    }
  }

function sqlserver_connect($pHostname, $pInstance, $pPort, $pUser, $pPassword, $pDatabase)
  {
  // Useful debug option !!!!!
//   ini_set('display_errors', 1);

  // Allow an optional first parameter specifying the database to switch to.

  $database_name = $pDatabase;

  // Build the server string <Server>:<port>[\<Instance>]

  $server_string = $pHostname . ':' . $pPort;

  if (strlen($pInstance) > 0)
    {
    $server_string = $server_string . '\\' . $pInstance;
    }

  // Connect

  $conn = mssql_pconnect($server_string, $pUser, $pPassword);

  // Change database ?

  if ($conn)
    {
    if (strlen($database_name) > 0)
      {
      mssql_select_db($database_name, $conn);
      }
    elseif (strlen($pDatabase) > 0)
      {
      mssql_select_db($pDatabase, $conn);
      }
    }

  // return.

  return $conn;
  }

function renaissance_connect()
  {
  // Allow an optional first parameter specifying the database to switch to.

  $database_name = '';

  if (func_num_args() > 0)
    {
    $args = func_get_args();
    $database_name = $args[0];
    }
  elseif ((strlen(SQLSERVER_RENAISSANCE_DATABASE) > 0) && (SQLSERVER_RENAISSANCE_DATABASE <> 'SQLSERVER_RENAISSANCE_DATABASE'))
    {
    $database_name = SQLSERVER_RENAISSANCE_DATABASE;
    }

  $instanceName = '';
  if ((strlen(SQLSERVER_RENAISSANCE_INSTANCE) > 0) && (SQLSERVER_RENAISSANCE_INSTANCE <> 'SQLSERVER_RENAISSANCE_INSTANCE'))
    {
    $instanceName = SQLSERVER_RENAISSANCE_INSTANCE;
    }

  return sqlserver_connect(SQLSERVER_RENAISSANCE_HOSTNAME, $instanceName, SQLSERVER_RENAISSANCE_PORT, SQLSERVER_RENAISSANCE_USER, SQLSERVER_RENAISSANCE_PASSWORD, $database_name);

  }

function renaissance_RB_connect()
{
    // Allow an optional first parameter specifying the database to switch to.

    $database_name = '';

    if (func_num_args() > 0)
    {
        $args = func_get_args();
        $database_name = $args[0];
    }
    elseif ((strlen(SQLSERVER_RENAISSANCE_DATABASE) > 0) && (SQLSERVER_RENAISSANCE_DATABASE <> 'SQLSERVER_RENAISSANCE_DATABASE'))
    {
        $database_name = SQLSERVER_RENAISSANCE_DATABASE;
    }

    $instanceName = '';
    if ((strlen(SQLSERVER_RENAISSANCE_INSTANCE) > 0) && (SQLSERVER_RENAISSANCE_INSTANCE <> 'SQLSERVER_RENAISSANCE_INSTANCE'))
    {
        $instanceName = SQLSERVER_RENAISSANCE_INSTANCE;
    }

    //return sqlserver_connect(SQLSERVER_RENAISSANCE_RB_HOSTNAME, $instanceName, SQLSERVER_RENAISSANCE_PORT, SQLSERVER_RENAISSANCE_USER, SQLSERVER_RENAISSANCE_PASSWORD, $database_name);

    return mssql_connect(SQLSERVER_RENAISSANCE_RB_HOSTNAME, SQLSERVER_RENAISSANCE_USER ,SQLSERVER_RENAISSANCE_PASSWORD );
}

function renaissance_AR_connect()
{
    // Allow an optional first parameter specifying the database to switch to.

    $database_name = '';

    if (func_num_args() > 0)
    {
        $args = func_get_args();
        $database_name = $args[0];
    }
    elseif ((strlen(SQLSERVER_RENAISSANCE_DATABASE) > 0) && (SQLSERVER_RENAISSANCE_DATABASE <> 'SQLSERVER_RENAISSANCE_DATABASE'))
    {
        $database_name = SQLSERVER_RENAISSANCE_DATABASE;
    }

    $instanceName = '';
    if ((strlen(SQLSERVER_RENAISSANCE_INSTANCE) > 0) && (SQLSERVER_RENAISSANCE_INSTANCE <> 'SQLSERVER_RENAISSANCE_INSTANCE'))
    {
        $instanceName = SQLSERVER_RENAISSANCE_INSTANCE;
    }

    //return sqlserver_connect(SQLSERVER_RENAISSANCE_RB_HOSTNAME, $instanceName, SQLSERVER_RENAISSANCE_PORT, SQLSERVER_RENAISSANCE_USER, SQLSERVER_RENAISSANCE_PASSWORD, $database_name);

    return mssql_connect(SQLSERVER_RENAISSANCE_AR_HOSTNAME, SQLSERVER_RENAISSANCE_USER ,SQLSERVER_RENAISSANCE_PASSWORD );
}

function sqlserver_neocapture_connect()
  {

  // Useful debug option !!!!!
//   ini_set('display_errors', 1);

  // Allow an optional first parameter specifying the database to switch to.

  $database_name = '';

  if (func_num_args() > 0)
    {
    $args = func_get_args();
    $database_name = $args[0];
    }
  elseif ((strlen(SQLSERVER_NEO_DATABASE) > 0) && (SQLSERVER_NEO_DATABASE <> 'SQLSERVER_NEO_DATABASE'))
    {
    $database_name = SQLSERVER_NEO_DATABASE;
    }

  $instanceName = '';
  if ((strlen(SQLSERVER_NEO_INSTANCE) > 0) && (SQLSERVER_NEO_INSTANCE <> 'SQLSERVER_NEO_INSTANCE'))
    {
    $instanceName = SQLSERVER_NEO_INSTANCE;
    }

  return sqlserver_connect(SQLSERVER_NEO_HOSTNAME, $instanceName, SQLSERVER_NEO_PORT, SQLSERVER_NEO_USER, SQLSERVER_NEO_PASSWORD, $database_name);

  }

function get_DateNow_sqlsvr()
  {
  $sql_DateEntered = substr(date('Y-m-d H:i:s.u'), 0, 23); //  ODBC canonical (with milliseconds) yyyy-mm-dd hh:mi:ss.mmm(24h)  

  // ini_set('display_errors', 1);

  try
    {
    $link = sqlserver_neocapture_connect();

    if ($link)
      {
        $queryString = "SELECT dbo.fn_getDateString() AS DateValue";
        $result = mssql_query($queryString, $link);

      if ($result === false)
        {
        $debug_val = mssql_get_last_message();
        $debug_val = $debug_val;
        }
      else
        {
        $row = mssql_fetch_array($result);

        if ($row[0])
          {
          $sql_DateEntered = $row[0];
          }

        mssql_free_result($result);
        }
      }

    } catch (Exception $e)
    {
    }

  return $sql_DateEntered;

  }


?>
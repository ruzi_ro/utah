<?php

require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');

function get_capture($ID)
  {

  $conn = db_connect();

  $query = "SELECT *
	        FROM capture
	        WHERE ID='" . $conn->real_escape_string($ID) . "'";

  if (!($result = $conn->query($query)))
    {
    throw new Exception('Could not find capture');
    }

  if ($result->num_rows == 0)
    {
    throw new Exception('Could not find capture');
    }

  $results_array = array();
  // build an array of the relevant information record
  for ($count = 0; $row = $result->fetch_assoc(); $count++)
    {
    $results_array = $row;
    }
  return $results_array;
  }


function add_capture_sqlserver($params, $sql_DateEntered)
  {
  try
    {

    $sqlsvr_conn = sqlserver_neocapture_connect();

    $CaptureID = 0;

// Create a new stored prodecure
    if ($sqlsvr_conn)
      {
      $stmt = mssql_init('dbo.adp_capture_InsertCommand', $sqlsvr_conn);

// Bind the field names
      mssql_bind($stmt, '@SetID', &$params['setID'], SQLINT4, false, false);
      mssql_bind($stmt, '@Result', &$params['result'], SQLVARCHAR, false, false, 255);
      mssql_bind($stmt, '@filename', &$params['filename'], SQLVARCHAR, false, false, 255);
      mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);

// Execute
      if ($stmt) $proc_result = mssql_execute($stmt);

      // Free statement
      mssql_free_statement($stmt);

      $row = mssql_fetch_assoc($proc_result);
      $CaptureID = $row['ID'];
      }  // if ($sqlsvr_conn)

    } catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }

function add_capture($params)
  {
    return 0;
/*
  $conn = db_connect();

// prepare insert query
  $name_array = array_keys($params);
  $value_array = array_values($params);
  $count = 0;


  $query = "INSERT INTO capture (";
  foreach ($name_array as $name)
    {
    if ($count >= 0)
      {
      if ($count > 0) $query = $query . " ,";
      $query = $query . $conn->real_escape_string($name);
      }
    $count++;
    }
  $query = $query . ") VALUES(";
  $count = 0;
  foreach ($value_array as $value)
    {
    if ($count >= 0)
      {
      if ($count > 0) $query = $query . " ,";
      $query = $query . "'" . $conn->real_escape_string($value) . "'";
      }
    $count++;
    }
  $query = $query . ")";

  // insert the new information record
  if (!$conn->query($query))
    {
    throw new Exception('capture record could not be saved');
    }

  return $conn->insert_id;

*/
  }

function delete_capture($ID)
  {

  $conn = db_connect();

  // prepare delete query

  $query = "DELETE FROM capture ";
  $query = $query . "WHERE ID='" . $ID . "'";

  // delete the information record
  if (!$conn->query($query))
    {
    throw new Exception("Unable to delete capture record");
    }

  return true;
  }

?>
<?php

function convertToSQLDate($phpdate) {
	try {
		$mysqldate = date( 'Y-m-d H:i:s', $phpdate );
		return $mysqldate;
	}
	catch (Exception $e){
		return false;
	}
}

function convertToPHPDate($mysqldate) {
	try {
		$phpdate = strtotime( $mysqldate );
	}
	catch (Exception $e){
		return false;
	}
}

?>
<?php

function dom_availabilities_table_org($url_content, $fundcode, $id, $sqlsvr_id, $thisFundNavDate, $sql_DateEntered)
  {

  $names = array();
  
//$names[] = array('Database field name', flag : Should not carry forward if true, flag : set as '' string, max string length);
// carry-over flag does not work with this code, it's only in here for consistency with other code.

  $names[] = array('neo_expand', 1, 0, 50);
  $names[] = array('neo_check', 1, 0, 50);
  $names[] = array('neo_description', 0, 0, 255);
  $names[] = array('neo_percentassets', 0, 0, 50);
  $names[] = array('neo_percentavailabilities', 0, 0, 50);
  $names[] = array('neo_currency', 0, 0, 5);
  $names[] = array('neo_balance', 0, 0, 50);
  $names[] = array('neo_currentbalance', 0, 0, 50);
  $names[] = array('neo_previousbalance', 0, 0, 50);
  $names[] = array('neo_previouscurrentbalance', 0, 0, 50);
  $names[] = array('neo_balancevariation', 0, 0, 50);
  $names[] = array('neo_percentvariation', 0, 0, 50);

  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    $tbody = $doc->getElementsByTagName('table')->item(0);

    //extract body of table
    $count = 0;

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {
      foreach ($tbody->childNodes AS $row)
        {
        //skip first row of headings
        if ($count > 0)
          {
      
          $cellcount = 0;
          $inventory = array();
          $info = $row->childNodes->length;
          $inventory['fundcode'] = '';

          foreach ($row->childNodes AS $cell)
            {
            if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
              {

              if ($cellcount < $names_length)
                {
                $dataVal = ($names[$cellcount][2] ? '' : trim($cell->nodeValue));
                if (strlen($dataVal) > 0) $dataVal = substr($dataVal, 0, $names[$cellcount][3]);
                $inventory[$names[$cellcount][0]] = $dataVal;
                }

              $cellcount++;
              }
            }

          if ($cellcount >= $names_length)
            {
            if ((strlen($inventory['neo_description']) <= 0) || ((strlen($inventory['neo_check']) >= 5) && (strtolower(substr($inventory['neo_check'] , 0, 5 )) == 'total'))) // Total line (probably)
              {
              $inventory['neo_description'] = 'Total';
              $inventory['neo_currency'] = 'Total'; // MAX 5 chars for CCY
              }
            elseif (strlen($inventory['neo_currency']) <= 0) // Summary line (probably)
              {
              $inventory['neo_currency'] = 'Summa'; // MAX 5 chars for CCY
              }

            if (strlen($inventory['fundcode']) <= 0)
              {
              $inventory['fundcode'] = $fundcode;
              }

            $inventory['NAVDate'] = $thisFundNavDate;

            if (NEOCAPTURE_DEBUG_ECHO) echo '          '.$inventory['neo_description'] . PHP_EOL;
            //if (NEOCAPTURE_DEBUG_ECHO) var_dump($inventory) . PHP_EOL;
              
            //$inventory['captureID'] = $id;
            //add_inventory($inventory);
            $inventory['captureID'] = $sqlsvr_id;
            add_availability_sqlsvr($inventory, $names, $sql_DateEntered);

            }
          else
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '*Error. Not enough table columns. Expecting ' . $names_length . ', found ' . $cellcount . PHP_EOL;

            return false;
            }
          }
        $count++;
        }
      }

    } catch (Exception $e)
    {
    return $e->getMessage();
    }
  return true; 
  }

function dom_availabilities_table($url_content, $fundcode, $id, $sqlsvr_id, $thisFundNavDate, $sql_DateEntered)
  {

  $names = array();
  $AvailabilityCounters = array();
  $AvailabilityCounter = 0;
  $KeyName = '';

  $AllExistingAvailabilities = get_existing_availabilities($fundcode, $thisFundNavDate);
  $ExistingAvailabilityNames = array();
  foreach ($AllExistingAvailabilities AS $row)
    {
    // Key is converted to UCase, as it is later, because SQL Server is not case sensitive (In our configuration).

    $KeyName = strtoupper($row['Description'] . "_" . $row['Currency']);

    if (strlen($row['Currency']) <= 4)
      {
      $ExistingAvailabilityNames[$KeyName]=$row;
      }
    }

//$names[] = array('Database field name', flag : Should not carry forward if true, flag : set as '' string, max string length);
// carry-over flag does not work with this code, it's only in here for consistency with other code.

  $names[] = array('neo_expand', 1, 0, 50);
  $names[] = array('neo_check', 1, 0, 50);
  $names[] = array('neo_description', 0, 0, 255);
  $names[] = array('neo_percentassets', 0, 0, 50);
  $names[] = array('neo_percentavailabilities', 0, 0, 50);
  $names[] = array('neo_currency', 0, 0, 5);
  $names[] = array('neo_balance', 0, 0, 50);
  $names[] = array('neo_currentbalance', 0, 0, 50);
  $names[] = array('neo_previousbalance', 0, 0, 50);
  $names[] = array('neo_previouscurrentbalance', 0, 0, 50);
  $names[] = array('neo_balancevariation', 0, 0, 50);
  $names[] = array('neo_percentvariation', 0, 0, 50);

  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    $tbody = $doc->getElementsByTagName('table')->item(0);

    //extract body of table
    $count = 0;

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {
      foreach ($tbody->childNodes AS $row)
        {
        //skip first row of headings
        if ($count > 0)
          {

          $cellcount = 0;
          $inventory = array();
          $info = $row->childNodes->length;
          $inventory['fundcode'] = '';

          foreach ($row->childNodes AS $cell)
            {
            if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
              {

              if ($cellcount < $names_length)
                {
                $dataVal = ($names[$cellcount][2] ? '' : trim($cell->nodeValue));
                if (strlen($dataVal) > 0) $dataVal = substr($dataVal, 0, $names[$cellcount][3]);
                $inventory[$names[$cellcount][0]] = $dataVal;
                }

              $cellcount++;
              }
            }

          if ($cellcount >= $names_length)
            {
            $KeyName = strtoupper($inventory['neo_description'] . "_" . $inventory['neo_currency']);
            if (array_key_exists ( $KeyName , $AvailabilityCounters ))
              {
              $AvailabilityCounter = $AvailabilityCounters[$KeyName];
              }
            else
              {
              $AvailabilityCounters[$KeyName] = 0;
              $AvailabilityCounter = 0;
              }

            if ((strlen($inventory['neo_description']) <= 0) || ((strlen($inventory['neo_check']) >= 5) && (strtolower(substr($inventory['neo_check'] , 0, 5 )) == 'total'))) // Total line (probably)
              {
              $inventory['neo_description'] = 'Total';
              $inventory['neo_currency'] = 'Total'; // MAX 5 chars for CCY
              }
            elseif (strlen($inventory['neo_currency']) <= 0) // Summary line (probably)
              {
              $inventory['neo_currency'] = 'Summa'; // MAX 5 chars for CCY
              }

            if (strlen($inventory['fundcode']) <= 0)
              {
              $inventory['fundcode'] = $fundcode;
              }

            $inventory['NAVDate'] = $thisFundNavDate;

            if ($AvailabilityCounter > 0)
              {
              $inventory['neo_description'] = $inventory['neo_description'] . $AvailabilityCounter;
              }

            if (NEOCAPTURE_DEBUG_ECHO) echo '          '.$inventory['neo_description'] . PHP_EOL;
            //if (NEOCAPTURE_DEBUG_ECHO) var_dump($inventory) . PHP_EOL;

            //$inventory['captureID'] = $id;
            //add_inventory($inventory);
            $inventory['captureID'] = $sqlsvr_id;
            add_availability_sqlsvr($inventory, $names, $sql_DateEntered);

            // Does this Entry Exist ?
            // Key is converted to UCase, because SQL Server is not case sensitive (In our configuration).

            $ExistingAvaKeyName = strtoupper($inventory['neo_description'] . "_" . $inventory['neo_currency']);
            if (array_key_exists ( $ExistingAvaKeyName , $ExistingAvailabilityNames ))
              {
              unset($ExistingAvailabilityNames[$ExistingAvaKeyName]);
              }

            $AvailabilityCounter += 1;
            $AvailabilityCounters[$KeyName] = $AvailabilityCounter;

            }
          else
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '*Error. Not enough table columns. Expecting ' . $names_length . ', found ' . $cellcount . PHP_EOL;

            return false;
            }
          }
        $count++;
        }

      // now remove Availabilities which do not seem to exist any more.
      foreach ($ExistingAvailabilityNames AS $row)
        {
        $inventory = array();

        foreach ($names as $name)
          {
          if ($name[1] == 0)
            {
            $inventory[$name[0]] = '0';
            }
          }

        $inventory['fundcode'] = $row['FundCode'];
        $inventory['neo_description'] = $row['Description'];
        $inventory['neo_currency'] = $row['Currency'];
        $inventory['NAVDate'] = $thisFundNavDate;
        $inventory['captureID'] = $sqlsvr_id;

        if ($row['Balance_Value'] != 0)
          {
          add_availability_sqlsvr($inventory, $names, $sql_DateEntered);
          }

        }
      }

    } catch (Exception $e)
    {
    return $e->getMessage();
    }
  return true;
  }

function add_availability($params)
  {

  // add to mysql database (no longer required)	
  
  }

function add_availability_sqlsvr($params, $names, $sql_DateEntered)
  {
  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_rec_Availabilitites_InsertCommand', $sqlsvr_conn);

// Bind the field names

    $Identity = 0;
    mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    mssql_bind($stmt, '@NAVDate', &$params['NAVDate'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@FundCode', &$params['fundcode'], SQLVARCHAR, false, false, 50);
    mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);

    foreach ($names as $name)
      {
      if ($name[1] == 0)
        {
         mssql_bind($stmt, '@' . $name[0], &$params[$name[0]], SQLVARCHAR, false, false, $name[3]);
        }
      }

    // Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result === false)
        {
        $CaptureID = false;
        }
      else if ($proc_result !== true)
        {

        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];

        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    } catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }

function get_existing_availabilities($fundcode, $thisFundNavDate)
  {
  $results_array = array();

  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();

    // Create a new stored prodecure
    $stmt = mssql_init('get_Availabilitites', $sqlsvr_conn);

    // Execute
    if ($stmt)
      {
      if (substr(phpversion(), 0, 4) == '5.3.')
        {
        mssql_bind($stmt, '@NAVDate', &$thisFundNavDate, SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@FundCode', &$fundcode, SQLVARCHAR, false, false, 50);
        } else
        {
        mssql_bind($stmt, '@NAVDate', $thisFundNavDate, SQLVARCHAR, false, false, 50);
        mssql_bind($stmt, '@FundCode', $fundcode, SQLVARCHAR, false, false, 50);
        }

      $proc_result = mssql_execute($stmt);

      if ($proc_result === false)
        {
        }
      else if ($proc_result !== true)
        {
        if (mssql_num_rows($proc_result) > 0)
          {
          while ($row = mssql_fetch_assoc($proc_result))
            {
            $results_array[] = $row;
            }
          }
        }

      // Free statement
      mssql_free_statement($stmt);
      }

    }
  catch (Exception $e)
    {
    }

  return $results_array;

  }

?>

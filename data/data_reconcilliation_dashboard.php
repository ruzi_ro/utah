<?php
/**
 *
 * User: nicholas
 * Date: 18/01/2013
 * Time: 11:51
 *
 */

function add_valuation_sqlsrv($params, $names, $thisFundNavDate, $sql_DateEntered)
  {
  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_rec_Valuation_InsertCommand', $sqlsvr_conn);

// Bind the field names

      /*
       * */


    $Identity = 0;
    $temp = mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    $temp = mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    $temp = mssql_bind($stmt, '@NAVDate', &$params['NAVDate'], SQLVARCHAR, false, false, 50);
    $temp = mssql_bind($stmt, '@FundCode', &$params['fundCode'], SQLVARCHAR, false, false, 50);
    $temp = mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);

    foreach ($names as $name)
      {
      $temp = mssql_bind($stmt, '@' . $name[0], &$params[$name[0]], SQLVARCHAR, false, false, $name[3]);
      }

    // Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result === false)
        {
        $CaptureID = false;
        }
      else if ($proc_result !== true)
        {

        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];

        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    } catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }


function add_exchangerate_sqlsrv($params, $names, $thisFundNavDate, $sql_DateEntered)
  {
  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_rec_FX_InsertCommand', $sqlsvr_conn);

// Bind the field names

    /*
     * PROCEDURE [dbo].[adp_rec_FX_InsertCommand]
          (
          @IDENTITY int OUTPUT,
          @captureID int,
          @NAVDate [nvarchar](50),
          @FundCode [nvarchar](50),
          @neo_currency [nvarchar](5),
          @neo_ratedate [nvarchar](50),
          @neo_rate [nvarchar](50),
          @neo_previousratedate [nvarchar](50),
          @neo_previousrate [nvarchar](50),
          @neo_percentchange [nvarchar](50),
          @neo_DateEntered nvarchar(50)
          )

     * */

    $Identity = 0;
    $temp = mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    $temp = mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    $temp = mssql_bind($stmt, '@NAVDate', &$params['NAVDate'], SQLVARCHAR, false, false, 50);
    $temp = mssql_bind($stmt, '@FundCode', &$params['fundCode'], SQLVARCHAR, false, false, 50);
    $temp = mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);


    foreach ($names as $name)
      {
      mssql_bind($stmt, '@' . $name[0], &$params[$name[0]], SQLVARCHAR, false, false, $name[3]);
      }

    // Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result === false)
        {
        $CaptureID = false;
        }
      else if ($proc_result !== true)
        {

        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];

        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    } catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }


function add_feeslist_sqlsrv($params, $names, $thisFundNavDate, $sql_DateEntered)
  {
  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_rec_Fees_InsertCommand', $sqlsvr_conn);

// Bind the field names

    /*
     * PROCEDURE [dbo].[adp_rec_Fees_InsertCommand]
          (
          @IDENTITY int OUTPUT,
          @captureID int,
          @NAVDate [nvarchar](50),
          @FundCode [nvarchar](50),
          @neo_class [nvarchar](50),
          @neo_feetype [nvarchar](50),
          @neo_fixedamount [nvarchar](50),
          @neo_minthreshold [nvarchar](50),
          @neo_maxthreshold [nvarchar](50),
          @neo_rate [nvarchar](50),
          @neo_currency [nvarchar](5),
          @neo_DateEntered nvarchar(50)
          )

     * */

    $Identity = 0;
    $temp = mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    $temp = mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    $temp = mssql_bind($stmt, '@NAVDate', &$params['NAVDate'], SQLVARCHAR, false, false, 50);
    $temp = mssql_bind($stmt, '@FundCode', &$params['fundCode'], SQLVARCHAR, false, false, 50);
    $temp = mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);


    foreach ($names as $name)
      {
      mssql_bind($stmt, '@' . $name[0], &$params[$name[0]], SQLVARCHAR, false, false, $name[3]);
      }

    // Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result === false)
        {
        $CaptureID = false;
        }
      else if ($proc_result !== true)
        {

        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];

        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    } catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }


function add_feesamount_sqlsrv($params, $names, $thisFundNavDate, $sql_DateEntered)
  {
  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_rec_FeesAmount_InsertCommand', $sqlsvr_conn);

// Bind the field names

    /*
     * PROCEDURE [dbo].[adp_rec_Fees_InsertCommand]
          (
          @IDENTITY int OUTPUT,
          @captureID int,
          @NAVDate [nvarchar](50),
          @FundCode [nvarchar](50),
          @neo_class [nvarchar](50),
          @neo_feetype [nvarchar](50),
          @neo_feesamount [nvarchar](50),
          @neo_feescumulative[nvarchar](50),
          @neo_DateEntered nvarchar(50)
          )

     * */

    $Identity = 0;
    $temp = mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    $temp = mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    $temp = mssql_bind($stmt, '@NAVDate', &$params['NAVDate'], SQLVARCHAR, false, false, 50);
    $temp = mssql_bind($stmt, '@FundCode', &$params['fundCode'], SQLVARCHAR, false, false, 50);
    $temp = mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);


    foreach ($names as $name)
      {
      mssql_bind($stmt, '@' . $name[0], &$params[$name[0]], SQLVARCHAR, false, false, $name[3]);
      }

    // Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result === false)
        {
        $CaptureID = false;
        }
      else if ($proc_result !== true)
        {

        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];

        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    } catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }


function add_result_sqlsrv($params, $names, $thisFundNavDate, $sql_DateEntered)
  {
  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_rec_ValuationByInstrumentType_InsertCommand', $sqlsvr_conn);

// Bind the field names

    /*
     * PROCEDURE [dbo].[adp_rec_ValuationByInstrumentType_InsertCommand]
          (
          @IDENTITY int OUTPUT,
          @captureID int,
          @NAVDate [nvarchar](50),
          @FundCode [nvarchar](50),
          @neo_instrumenttype [nvarchar](50),
          @neo_marketvalue [nvarchar](50),
          @neo_currency [nvarchar](5),
          @neo_percentage[nvarchar](50),
          @neo_DateEntered nvarchar(50)
          )

     * */

    $Identity = 0;
    $temp = mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    $temp = mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    $temp = mssql_bind($stmt, '@NAVDate', &$params['NAVDate'], SQLVARCHAR, false, false, 50);
    $temp = mssql_bind($stmt, '@FundCode', &$params['fundCode'], SQLVARCHAR, false, false, 50);
    $temp = mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);


    foreach ($names as $name)
      {
      if ($name[1] == 0)
        {
        mssql_bind($stmt, '@' . $name[0], &$params[$name[0]], SQLVARCHAR, false, false, $name[3]);
        }
      }

    // Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result === false)
        {
        $CaptureID = false;
        }
      else if ($proc_result !== true)
        {

        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];

        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    } catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }


function add_fundStatus_sqlsrv($params, $names, $thisFundNavDate, $sql_DateEntered)
  {
  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_rec_FundStatus_InsertCommand', $sqlsvr_conn);

// Bind the field names

    /*
     * PROCEDURE [dbo].[adp_rec_ValuationByInstrumentType_InsertCommand]
          (
          @IDENTITY int OUTPUT,
          @captureID int,
          @FundCode [nvarchar](50),
          @neo_isin [nvarchar](50),
          @neo_funddescription [nvarchar](50),
          @neo_currency [nvarchar](5),
          @neo_navdate [nvarchar](50),
          @neo_status [nvarchar](50),
          @neo_DateEntered nvarchar(50)
          )

     * */

    $Identity = 0;
    $temp = mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    $temp = mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    $temp = mssql_bind($stmt, '@FundCode', &$params['fundcode'], SQLVARCHAR, false, false, 50);
    $temp = mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);


    foreach ($names as $name)
      {
      if ($name[1] == 0)
        {
        mssql_bind($stmt, '@' . $name[0], &$params[$name[0]], SQLVARCHAR, false, false, $name[3]);
        }
      }

    // Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result === false)
        {
        $CaptureID = false;
        }
      else if ($proc_result !== true)
        {

        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];

        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    } catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }


?>


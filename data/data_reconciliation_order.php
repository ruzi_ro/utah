<?php

function dom_order_table($url_content, $fundcode, $id, $sqlsvr_id, $funddate, $sql_DateEntered)
  {

  $names = array();
  $names[] = array('neo_header', 1, 0, 50);
  $names[] = array('neo_clickall', 1, 0, 50);
  $names[] = array('neo_type', 0, 0, 50); // Carry forward, if necessary
  $names[] = array('neo_isin', 1, 0, 50);
  $names[] = array('neo_shareclass', 1, 0, 50);
  $names[] = array('neo_partcurrency', 1, 0, 50);
  $names[] = array('neo_numberofshares', 1, 0, 50);
  $names[] = array('neo_amount', 1, 0, 50);
  $names[] = array('neo_navdate', 1, 0, 50);
  $names[] = array('neo_nav', 1, 0, 50);
  $names[] = array('neo_fees', 1, 0, 50);
  $names[] = array('neo_retrocession', 1, 0, 50);
  $names[] = array('neo_classcode', 1, 0, 50);
  $names[] = array('neo_accountingdate', 1, 0, 50);
  $names[] = array('neo_linecount', 1, 0, 50); // Must be at the end
  $names[] = array('fundcode', 1, 0, 50); // Must be at the end

  $names_length = count($names);

  try
    {
    $doc = new DOMDocument();
    @$doc->loadHTML($url_content);

    //$thead = $doc->getElementsByTagName('thead')->item(0);
    $tbody = $doc->getElementsByTagName('table')->item(0);

    //extract body of table

    $count = 0;
    $db_counter = 0;
    $order = array();

    if (($tbody != null) && $tbody->hasChildNodes()) // ($tbody->hasChildNodes())
      {
      foreach ($tbody->childNodes AS $row)
        {
        //skip first row of headings

        if ($count > 0)
          {
          $order['neo_nav'] = ''; // This ensures that a blank line if not saved because of the carry forward information.
          $cellcount = 0;

          foreach ($row->childNodes AS $cell)
            {
            $debuginfo1 = $cell->nodeName;

            if (($cell->nodeName == "td") OR ($cell->nodeName == "th"))
              {

              if ($cellcount < $names_length)
                {
                $dataVal = ($names[$cellcount][2] ? '' : trim($cell->nodeValue));
                if (strlen($dataVal) > 0) $dataVal = substr($dataVal, 0, $names[$cellcount][3]);

                // Set value if no-carry-forward, or there is a value given.

                if (($names[$cellcount][1]) || (strlen($dataVal) > 0))
                  {
                  $order[$names[$cellcount][0]] = $dataVal;
                  }
                }
              $cellcount++;

              }

            }

          $order['neo_linecount'] = (string)$db_counter;
          $order['fundcode'] = $fundcode;

          if (($cellcount > 1) && (strlen($order['neo_nav']) > 0)) // Exclude Summary lines, they don't have nav values.
            {
            if ($cellcount >= ($names_length-2)) /* minus 2 to allow for linecount and fundcode  */
              {
              //if data in the table then write to database

              $db_counter++;
              $order['captureID'] = $id;
              //add_order($order);
              $order['captureID'] = $sqlsvr_id;
              add_order_sqlsvr($order, $names, $sql_DateEntered);


              }
            else
              {
              if (NEOCAPTURE_DEBUG_ECHO) echo '*Error. Not enough table columns. Expecting ' . $names_length . ', found ' . $cellcount . PHP_EOL;

              return false;
              }
            }
          }

        $count++;
        }
      }
    } catch (Exception $e)
    {
    return $e->getMessage();
    }
  return true; //$table . "</tbody></table>";
  }

function add_order($params)
  {

  $conn = db_connect();

  // prepare insert query
  $name_array = array_keys($params);
  $value_array = array_values($params);
  $count = 0;

  $query = "INSERT INTO rec_order (";
  foreach ($name_array as $name)
    {
    if ($count >= 0)
      {
      if ($count > 0) $query = $query . " ,";
      $query = $query . $conn->real_escape_string($name);
      }
    $count++;
    }
  $query = $query . ") VALUES(";
  $count = 0;
  foreach ($value_array as $value)
    {
    if ($count >= 0)
      {
      if ($count > 0) $query = $query . " ,";
      $query = $query . "'" . $conn->real_escape_string($value) . "'";
      }
    $count++;
    }
  $query = $query . ")";

  // insert the new information record
  if (!$conn->query($query))
    {
    throw new Exception('order record could not be saved');
    //throw new Exception($query);
    }

  return $conn->insert_id;
  }

function add_order_sqlsvr($params, $names, $sql_DateEntered)
  {
  try
    {
    $sqlsvr_conn = sqlserver_neocapture_connect();
    //ini_set('display_errors', 1);

// Create a new stored prodecure
    $stmt = mssql_init('adp_rec_order_InsertCommand', $sqlsvr_conn);

// Bind the field names

    $Identity = 0;
    mssql_bind($stmt, '@IDENTITY', &$Identity, SQLINT4, true, false);
    mssql_bind($stmt, '@captureID', &$params['captureID'], SQLINT4, false, false);
    mssql_bind($stmt, '@neo_DateEntered', &$sql_DateEntered, SQLVARCHAR, false, false, 50);

    foreach ($names as $name)
      {
      mssql_bind($stmt, '@' . $name[0], &$params[$name[0]], SQLVARCHAR, false, false, $name[3]);
      }

    // Execute
    if ($stmt)
      {
      $proc_result = mssql_execute($stmt);

      if ($proc_result === false)
        {
        $CaptureID = false;
        }
      else if ($proc_result !== true)
        {

        $row = mssql_fetch_assoc($proc_result);
        $CaptureID = $row['ID'];

        mssql_free_statement($proc_result);
        }
      else
        { // ($proc_result===true)
        $CaptureID = true;
        }

      // Free statement
      mssql_free_statement($stmt);
      }
    else
      {
      $CaptureID = (-1);
      }
    } catch (Exception $e)
    {
    $CaptureID = -1;
    }

  return $CaptureID;
  }

?>

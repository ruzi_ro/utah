<?php

require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');


function set_CorporateEvents_sqlsrv($params)
{
    $sqlsvr_conn = sqlserver_neocapture_connect();

    //$myServer = "srbuatapp31";
    //$myUser   = "renaissance";
    //$myPass   = "venice";
    //$myDB = "neocapture";
    //ini_set('display_errors', 1);
    //$sqlsvr_conn = mssql_connect($myServer, $myUser ,$myPass);
    //or die("Couldn't connect to SQL Server on $myServer");
    //mssql_select_db($myDB, $sqlsvr_conn);
    //or die("Couldn't open database $myDB");

    if ($sqlsvr_conn)
    {

        echo "CNX : OK  <BR>". (string)$sqlsvr_conn. PHP_EOL;
    }
    else
    {
        echo "CNX : NOK  <BR>". (string)$sqlsvr_conn. PHP_EOL;
    }



    foreach ($params as $line)
    {
        try
        {

            // Create a new stored prodecure
            $stmt = mssql_init('adp_corporateEvents_InsertCommand', $sqlsvr_conn);

            // Bind the field names

            //$Identity = 0;



            $isinCode = $line['isinCode'];
            $securitiesDescription = $line['securitiesDescription'];
            $category = $line['category'];
            $type = $line['type'];
            $securitiesAccountBranch = $line['securitiesAccountBranch'];
            $securitiesAccountNumber = $line['securitiesAccountNumber'];
            $securitiesAccountDesc = $line['securitiesAccountDesc'];
            $bankReference = $line['bankReference'];
            $statusCode = $line['statusCode'];
            $event = $line['event'];
            $responseType = $line['responseType'];
            $clientDeadlineDate = $line['clientDeadlineDate'];
            $exDate = $line['exDate'] ;
            $effectiveDate = $line['effectiveDate'] ;
            $recordDate = $line['recordDate'];
            $eligibleQuantity = $line['eligibleQuantity'];
            $eligibleQuantityType = $line['eligibleQuantityType'];
            $updateDate = $line['updateDate'];
            $updateTime = $line['updateTime'];
            $messageReference = $line['messageReference'];
            $pendingDeliveries = $line['pendingDeliveries'];
            $pendingReceipt = $line['pendingReceipt'];
            $indicativeUninstructedQty = $line['indicativeUninstructedQty'];
            $minimumPrice = $line['minimumPrice'];
            $maximumPrice = $line['maximumPrice'];
            $minimumEligibleQty = $line['minimumEligibleQty'];
            $maximumEligibleQty = $line['maximumEligibleQty'];
            $acceptablePriceIncrement = $line['acceptablePriceIncrement'];
            $countryCode = $line['countryCode'];
            $localCode = $line['localCode'];
            $paymentDate = $line['paymentDate'];
            $dateEntered = date('Y-m-d');

            mssql_bind($stmt, '@isinCode', &$isinCode, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@securitiesDescription', &$securitiesDescription, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@category', &$category,  SQLVARCHAR, false, false);
            mssql_bind($stmt, '@type', &$type, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@securitiesAccountBranch', &$securitiesAccountBranch, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@securitiesAccountNumber', &$securitiesAccountNumber, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@securitiesAccountDesc', &$securitiesAccountDesc, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@bankReference', &$bankReference, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@statusCode', &$statusCode, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@event', &$event, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@responseType', &$responseType, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@clientDeadlineDate', &$clientDeadlineDate, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@clientDeadlineTime', &$clientDeadlineTime, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@exDate', &$exDate, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@effectiveDate', &$effectiveDate, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@recordDate', &$recordDate, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@eligibleQuantity', &$eligibleQuantity , SQLVARCHAR, false, false);
            mssql_bind($stmt, '@eligibleQuantityType', &$eligibleQuantityType, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@updateDate', &$updateDate , SQLVARCHAR, false, false);
            mssql_bind($stmt, '@updateTime ', &$updateTime , SQLVARCHAR, false, false);
            mssql_bind($stmt, '@messageReference'  , &$messageReference , SQLVARCHAR, false, false);
            mssql_bind($stmt, '@pendingDeliveries' , &$pendingDeliveries, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@pendingReceipt', &$pendingReceipt, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@indicativeUninstructedQty', &$indicativeUninstructedQty, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@minimumPrice', &$minimumPrice, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@maximumPrice', &$maximumPrice, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@minimumEligibleQty', &$minimumEligibleQty, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@maximumEligibleQty', &$maximumEligibleQty, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@acceptablePriceIncrement', &$acceptablePriceIncrement, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@countryCode', &$countryCode, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@localCode', &$localCode, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@paymentDate', &$paymentDate , SQLVARCHAR, false, false);
            mssql_bind($stmt, '@neo_DateEntered', &$dateEntered, SQLVARCHAR, false, false);


            //echo '  RRU : PARAM securitiesAccountBranch ' . $securitiesAccountBranch . "<BR>". PHP_EOL;

            // Execute
            if ($stmt)
            {
                $proc_result = mssql_execute($stmt);

                if ($proc_result===false)
                {
                    $CaptureID = false;
                }
                else if ($proc_result!==true)
                {
                    $row = mssql_fetch_assoc($proc_result);
                    $CaptureID = $row['ID'];
                }
                else
                { // ($proc_result===true)
                    $CaptureID = true;
                }

                // Free statement
                mssql_free_statement($stmt);
            }
            else
            {
                $CaptureID = (-1);
            }
        }
        catch (Exception $e)
        {
            $CaptureID = -1;
        }


    }
        return $CaptureID;


}


?>
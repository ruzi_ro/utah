<?php

require_once(NEOCAPTURE_ROOT . '/functions/db_functions.php');

function get_morningstar_list_sqlsrv()
  {

  try
    {
    $results_array = array();

    $sqlsvr_conn = renaissance_connect();

    $userID = 0;
    $securityToken = '';
    $knowledgeDate = '1900-01-01';

    // Create a new stored procedure

    $stmt = mssql_init('web_GetInstrumentIDsForPricing', $sqlsvr_conn);

    // Execute

    if ($stmt)
      {
      if (substr(phpversion(), 0, 4) == '5.3.')
        {

        mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
        mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
        mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate, SQLVARCHAR, false, false);

        } else
        {
        // Pass by reference deprecated in PHP 5.4

        mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
        mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
        mssql_bind($stmt, '@KnowledgeDate_Str', $knowledgeDate, SQLVARCHAR, false, false);
        }

      $proc_result = mssql_execute($stmt);

      if (mssql_num_rows($proc_result) > 0)
        {
        while ($row = mssql_fetch_assoc($proc_result))
          {
          $results_array[] = $row;
          }
        }

      // Free statement
      mssql_free_statement($stmt);
      }

    } catch (Exception $e)
    {
    }

  return $results_array;


  }

function get_morningstar_index_list_sqlsrv()
  {

  try
    {
    $results_array = array();

    $sqlsvr_conn = renaissance_connect();

    $userID = 0;
    $securityToken = '';
    $knowledgeDate = '1900-01-01';

    // Create a new stored procedure

    $stmt = mssql_init('web_GetIndexIDsForPricing_Morningstar', $sqlsvr_conn);

    // Execute

    if ($stmt)
      {
      if (substr(phpversion(), 0, 4) == '5.3.')
        {

        mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
        mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
        mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate, SQLVARCHAR, false, false);

        } else
        {
        // Pass by reference deprecated in PHP 5.4

        mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
        mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
        mssql_bind($stmt, '@KnowledgeDate_Str', $knowledgeDate, SQLVARCHAR, false, false);
        }

      $proc_result = mssql_execute($stmt);

      if (mssql_num_rows($proc_result) > 0)
        {
        while ($row = mssql_fetch_assoc($proc_result))
          {
          $results_array[] = $row;
          }
        }

      // Free statement
      mssql_free_statement($stmt);
      }


    } catch (Exception $e)
    {

    }


  return $results_array;


  }

function getMorningstarTest()
  {

  $results_array = array();
  $element = array();

  $codestring = "GB0033737767
LU0135706553
LU0231482349
GB00B2PF4L24
LU0522792919
LU0658025209";

  $currencystring = "GBP
EUR
USD
EUR
USD
EUR";


  $codes = explode("\n", $codestring);
  //$codes = preg_split ('/$\R?^/m', $codestring);
  $currencies = explode("\n", $currencystring);
  //$currencies = preg_split ('/$\R?^/m', $currencystring);
  //$results_array[]=$codes;
  //$results_array[]=$currencies;

  // end dummy code

  for ($i = 0; $i < count($codes); $i++)
    {
    $element['ISIN'] = $codes[$i];
    $element['Currency'] = $currencies[$i];
    $results_array[] = $element;
    }

  return $results_array;
  }


function updatePricesAutoUpdate()
  {
  /* Update the Venice 'AutoUpdate' table for Prices.
   * Venice update message should subsequently be pulished by the 'Mitchigan' process.
   */
  try
    {
    $sqlsvr_conn = renaissance_connect();

    mssql_free_result(mssql_query("EXECUTE [web_setAutoUpdate] @UpdateField='HAS_PRICE'", $sqlsvr_conn));

    } catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo 'problem in updatePricesAutoUpdate(). ' . $e->getMessage() . PHP_EOL;
    }
  }


function add_morningstar_prices_sqlsrv($msCodes, $results, $sql_DateEntered)
  {
  $results_array = array();
  $iCounter = 0;
  $somePricesSaved = false;

  try
    {
    $sqlsvr_conn = renaissance_connect();

    foreach ($results as $line)
      {
      $userID = 0;
      $securityToken = '';
      $knowledgeDate = '1900-01-01';

      $ISIN = substr(strtoupper($line['ISIN']), 0, 25);
      $MorningstarID = substr($line['MorningstarID'], 0, 25);
      $Currency = substr($line['Currency'], 0, 25);
      $LegalName = substr($line['LegalName'], 0, 100);
      $InitialPurchaseUnit = substr($line['InitialPurchaseUnit'], 0, 25);
      $InitialPurchaseBase = substr($line['InitialPurchaseBase'], 0, 25);
      $InitialPurchase = substr($line['InitialPurchase'], 0, 25);
      $SubsequentInvestmentUnit = substr($line['SubsequentInvestmentUnit'], 0, 25);
      $SubsequentInvestment = substr($line['SubsequentInvestment'], 0, 25);
      $LegalStructure = substr($line['LegalStructure'], 0, 25);
      $DomicileCountryName = substr($line['DomicileCountryName'], 0, 50);
      $UCITS = substr($line['UCITS'], 0, 25);
      $ClosePriceDate = substr($line['ClosePriceDate'], 0, 10); // Date only, yyyy-mm-dd
      $ClosePrice = substr($line['ClosePrice'], 0, 25);
      $PriceCurrencyID = substr($line['PriceCurrencyId'], 0, 25);

      /* e.g.
      *
      FR0010259937
      F0GBR06FKX
      CU$$$$$EUR
      Amundi Volatilité Actions Euro
      2
      1.00000
      1.00000
      2
      1.00000
      FCP
      France
      false
      2013-04-08T00:00:00+01:00
      75751.090000
      */

      /*
      @UserID int,
      @Token varchar(100),
      @InstrumentID             [int] = 0,
      @ISIN                     [nvarchar](25) = NULL,
      @Morningstar              [nvarchar](25) = NULL,
      @Currency                 [nvarchar](25) = NULL,
      @LegalName                [nvarchar](100) = NULL,
      @InitialPurchaseUnit      [nvarchar](25) = NULL,
      @InitialPurchaseBase      [nvarchar](25) = NULL,
      @InitialPurchase          [nvarchar](25) = NULL,
      @SubsequentInvestmentUnit [nvarchar](25) = NULL,
      @SubsequentInvestment     [nvarchar](25) = NULL,
      @LegalStructure           [nvarchar](25) = NULL,
      @DomicileCountryName      [nvarchar](50) = NULL,
      @UCITS                    [nvarchar](25) = NULL,
      @ClosePriceDate           [nvarchar](25) = NULL,
      @ClosePrice               [nvarchar](25) = NULL,
      @KnowledgeDate_Str [varchar](25) = '1900-01-01'
      */

      // Find InstrumentID from $msCodes
      // The logic at present will match a price if the Morningstar codes match or if the ISIN AND Currency match.
      // Instruments will match one time only.
      // Prices will not save unless a matching instrument is found.

      $found = false;
      $instrumentID = 0;

      // Match by Morningstar ID first

      if (strlen($MorningstarID) > 0)
        {
        for ($iCounter = 0; $iCounter < count($msCodes); $iCounter++)
          {
          $instrumentLine = $msCodes[$iCounter];

          if (($instrumentLine['ID'] > 0) && ($instrumentLine['Morningstar'] == $MorningstarID))
            {
            $instrumentID = $instrumentLine['ID'];

            // Clear this $instrumentLine so that it does not match again.

            $instrumentLine['ID'] = 0;
            $instrumentLine['Morningstar'] = '';
            $instrumentLine['ISIN'] = '';
            $instrumentLine['Currency'] = '';

            $msCodes[$iCounter] = $instrumentLine;

            $found = true;
            break;
            }
          } // for
        }

      if ($found == false)
        {
        for ($iCounter = 0; $iCounter < count($msCodes); $iCounter++)
          {
          $instrumentLine = $msCodes[$iCounter];

          if (($instrumentLine['ID'] > 0) && (($instrumentLine['ISIN'] == $ISIN) && ($instrumentLine['Currency'] == substr($Currency, -strlen($instrumentLine['Currency'])))))
            {
            $instrumentID = $instrumentLine['ID'];

            // Clear this $instrumentLine so that it does not match again.

            $instrumentLine['ID'] = 0;
            $instrumentLine['Morningstar'] = '';
            $instrumentLine['ISIN'] = '';
            $instrumentLine['Currency'] = '';

            $msCodes[$iCounter] = $instrumentLine;

            $found = true;
            break;
            }
          }
        }


      if ($found)
        {
        // Create a new stored procedure

        $stmt = mssql_init('web_SaveInstrumentPrice', $sqlsvr_conn);

        // Execute

        if ($stmt)
          {
          if (substr(phpversion(), 0, 4) == '5.3.')
            {

            mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
            mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate, SQLVARCHAR, false, false);

            mssql_bind($stmt, '@InstrumentID', &$instrumentID, SQLINT4, false, false);
            mssql_bind($stmt, '@ISIN', &$ISIN, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@Morningstar', &$MorningstarID, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@Currency', &$Currency, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@LegalName', &$LegalName, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@InitialPurchaseUnit', &$InitialPurchaseUnit, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@InitialPurchaseBase', &$InitialPurchaseBase, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@InitialPurchase', &$InitialPurchase, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@SubsequentInvestmentUnit', &$SubsequentInvestmentUnit, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@SubsequentInvestment', &$SubsequentInvestment, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@LegalStructure', &$LegalStructure, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@DomicileCountryName', &$DomicileCountryName, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@UCITS', &$UCITS, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@ClosePriceDate', &$ClosePriceDate, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@ClosePrice', &$ClosePrice, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@PriceCurrencyID', &$PriceCurrencyID, SQLVARCHAR, false, false);

            } else
            {
            // Pass by reference deprecated in PHP 5.4

            mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
            mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@KnowledgeDate_Str', $knowledgeDate, SQLVARCHAR, false, false);

            mssql_bind($stmt, '@InstrumentID', $instrumentID, SQLINT4, false, false);
            mssql_bind($stmt, '@ISIN', $ISIN, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@Morningstar', $MorningstarID, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@Currency', $Currency, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@LegalName', $LegalName, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@InitialPurchaseUnit', $InitialPurchaseUnit, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@InitialPurchaseBase', $InitialPurchaseBase, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@InitialPurchase', $InitialPurchase, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@SubsequentInvestmentUnit', $SubsequentInvestmentUnit, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@SubsequentInvestment', $SubsequentInvestment, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@LegalStructure', $LegalStructure, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@DomicileCountryName', $DomicileCountryName, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@UCITS', $UCITS, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@ClosePriceDate', $ClosePriceDate, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@ClosePrice', $ClosePrice, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@PriceCurrencyID', $PriceCurrencyID, SQLVARCHAR, false, false);
            }

          $proc_result = mssql_execute($stmt);

          if (mssql_num_rows($proc_result) > 0)
            {
            $row = mssql_fetch_assoc($proc_result);
            $results_array[] = $instrumentID . ', ' . $ClosePriceDate . ', ' . $row;

            if (is_numeric($row['StatusString']))
              {
              $somePricesSaved = true;
              }

            if (NEOCAPTURE_DEBUG_ECHO) echo '  ' . $instrumentID . ', ' . $ClosePriceDate . ', ' . $row . PHP_EOL;
            } else
            {
            if (NEOCAPTURE_DEBUG_ECHO) echo '  ' . $instrumentID . ', ' . $ClosePriceDate . ', failed - (mssql_num_rows($proc_result) == 0)' . PHP_EOL;
            }

          // Free statement
          mssql_free_statement($stmt);

          } // if ($stmt)

        } // $found.

      }

    } catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo 'problem in add_morningstar_prices_sqlsrv(). ' . $e->getMessage() . PHP_EOL;
    }

  if ($somePricesSaved)
    {
    updatePricesAutoUpdate();
    }

  return $results_array;
  }

function add_morningstar_index_returns_sqlsrv($msCodes, $results, $sql_DateEntered)
  {
  $results_array = array();
  $iCounter = 0;
  $somePricesSaved = false;

  try
    {
    $sqlsvr_conn = renaissance_connect();

    foreach ($results as $line)
      {
      $userID = 0;
      $securityToken = '';
      $knowledgeDate = '1900-01-01';

      $instrumentID = intval(substr(strtoupper($line['ID']), 0, 25));
      $MorningstarID = substr($line['Morningstar'], 0, 25);
      $ReturnDate = substr($line['date'], 0, 10); // Date only, yyyy-mm-dd
      $PriceValue = '';
      $ReturnValue = substr($line['return'], 0, 25);


      // Create a new stored procedure

      $stmt = mssql_init('web_SaveIndexPrice', $sqlsvr_conn);

      // Execute

      if ($stmt)
        {
        if (substr(phpversion(), 0, 4) == '5.3.')
          {

          mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
          mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate, SQLVARCHAR, false, false);

          mssql_bind($stmt, '@InstrumentID', &$instrumentID, SQLINT4, false, false);
          mssql_bind($stmt, '@ReturnDate', &$ReturnDate, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@PriceValue', &$PriceValue, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@ReturnValue', &$ReturnValue, SQLVARCHAR, false, false);

          } else
          {
          // Pass by reference deprecated in PHP 5.4

          mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
          mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@KnowledgeDate_Str', $knowledgeDate, SQLVARCHAR, false, false);

          mssql_bind($stmt, '@InstrumentID', $instrumentID, SQLINT4, false, false);
          mssql_bind($stmt, '@ReturnDate', $ReturnDate, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@PriceValue', $PriceValue, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@ReturnValue', $ReturnValue, SQLVARCHAR, false, false);
          }

        $proc_result = mssql_execute($stmt);

        if (mssql_num_rows($proc_result) > 0)
          {
          $row = mssql_fetch_assoc($proc_result);
          $results_array[] = $instrumentID . ', ' . $ReturnDate . ', ' . $row['StatusString'];

          if (is_numeric($row['StatusString']))
            {
            $somePricesSaved = true;
            }

          if (NEOCAPTURE_DEBUG_ECHO) echo '  ' . $instrumentID . ', ' . $ReturnDate . ', ' . $row . PHP_EOL;
          } else
          {
          if (NEOCAPTURE_DEBUG_ECHO) echo '  ' . $instrumentID . ', ' . $ReturnDate . ', failed - (mssql_num_rows($proc_result) == 0)' . PHP_EOL;
          }

        // Free statement
        mssql_free_statement($stmt);

        } // if ($stmt)


      }

    } catch (Exception $e)
    {
    if (NEOCAPTURE_DEBUG_ECHO) echo 'problem in add_morningstar_index_returns_sqlsrv(). ' . $e->getMessage() . PHP_EOL;
    }

  if ($somePricesSaved)
    {
    //updatePricesAutoUpdate(); // Not Updating the prices table from here (yet!)
    }

  return $results_array;
  }

?>